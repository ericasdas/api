/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.base.vo.FileVO;
import cn.shoptnt.service.base.service.FileManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;


/**
 * 存储方案控制器
 *
 * @author fk
 * @version v5.2.3
 * @since v5.2.3
 * 2021年12月01日16:56:28
 */
@RestController
@RequestMapping("/admin/uploaders")
@Tag(name = "上传图片api")
public class FileManagerController {

    @Autowired
    private FileManager fileManager;


    @Operation(summary = "文件上传")
    @Parameter(name = "scene", description = "业务场景", required = true,   in = ParameterIn.QUERY)
    @PostMapping
    public FileVO list(MultipartFile file, String scene) {
        return fileManager.upload(file, scene);
    }

    @Operation(summary = "文件删除")
    @Parameter(name = "file_path", description = "文件路径", required = true,   in = ParameterIn.QUERY)
    @DeleteMapping
    public String delete(@Parameter(hidden = true) String filePath) {
        this.fileManager.deleteFile(filePath);
        return null;
    }


}
