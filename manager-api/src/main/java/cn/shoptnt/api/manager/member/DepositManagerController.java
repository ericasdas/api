/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.client.trade.DepositeLogClient;
import cn.shoptnt.client.trade.RechargeClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.model.trade.deposite.DepositeLogDO;
import cn.shoptnt.model.trade.deposite.RechargeDO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 预存款相关API
 * @author: liuyulei
 * @create: 2020-01-02 16:28
 * @version:1.0
 * @since:7.1.4
 **/
@RestController
@RequestMapping("/admin/members/deposit")
@Tag(name = "预存款相关API")
@Validated
public class DepositManagerController {


    @Autowired
    private DepositeLogClient depositeLogClient;

    @Autowired
    private RechargeClient rechargeClient;

    @Operation(summary = "获取会员预存款明细")
    @GetMapping(value = "/log")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "member_name", description = "会员名称", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
    })
    public WebPage listLog(@Validated DepositeParamDTO paramDTO) {
        return this.depositeLogClient.list(paramDTO);
    }


    @Operation(summary = "获取会员充值记录")
    @GetMapping(value = "/recharge")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "member_name", description = "会员名称", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "sn", description = "充值编号", in = ParameterIn.QUERY),
    })
    public WebPage listRecharge(@Validated DepositeParamDTO paramDTO) {
        return this.rechargeClient.list(paramDTO);
    }


}
