/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.ValidatorPlatformDO;
import cn.shoptnt.model.system.vo.UploaderVO;
import cn.shoptnt.model.system.vo.ValidatorPlatformVO;
import cn.shoptnt.service.system.ValidatorPlatformManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 滑块验证平台相关API
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
@Tag(name = "滑块验证平台相关API")
@RestController
@RequestMapping("/admin/systems/validator")
@Validated
public class ValidatorPlatformManagerController {

    @Autowired
    private ValidatorPlatformManager validatorPlatformManager;

    @Operation(summary = "查询验证平台列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.validatorPlatformManager.list(pageNo, pageSize);
    }

    @Operation(summary = "修改验证平台信息")
    @PutMapping(value = "/{pluginId}")
    @Parameters({
            @Parameter(name = "plugin_id", description = "验证平台插件ID", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "slider_verify_platform", description = "验证平台对象", required = true)
    })
    public ValidatorPlatformVO edit(@PathVariable String pluginId, @RequestBody @Parameter(hidden = true) ValidatorPlatformVO validatorPlatformVO) {
        validatorPlatformVO.setPluginId(pluginId);
        return this.validatorPlatformManager.edit(validatorPlatformVO);
    }

    @Operation(summary = "获取验证平台的配置")
    @GetMapping("/{pluginId}")
    @Parameter(name = "plugin_id", description = "验证平台插件ID", required = true,   in = ParameterIn.PATH)
    public ValidatorPlatformVO getUploadSetting(@PathVariable String pluginId) {
        return this.validatorPlatformManager.getConfig(pluginId);
    }

    @Operation(summary = "开启某个验证平台")
    @PutMapping("/{pluginId}/open")
    @Parameter(name = "plugin_id", description = "验证平台插件ID", required = true,   in = ParameterIn.PATH)
    public String open(@PathVariable String pluginId) {
        this.validatorPlatformManager.open(pluginId);
        return null;
    }
}
