/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.trade;

import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.trade.order.vo.OrderSettingVO;
import cn.shoptnt.framework.util.JsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 订单设置相关API
 * @author Snow create in 2018/7/13
 * @version v2.0
 * @since v7.0.0
 */
@Tag(name = "订单设置相关API")
@RestController
@RequestMapping("/admin/trade/orders")
@Validated
public class OrderSettingManagerController {


    @Autowired
    private SettingClient settingClient;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;

    @GetMapping("/setting")
    @Operation(summary = "获取订单任务设置信息")
    public OrderSettingVO getOrderSetting(){

        String json = this.settingClient.get(SettingGroup.TRADE);

        return JsonUtil.jsonToObject(json,OrderSettingVO.class);
    }

    @PostMapping("/setting")
    @Operation(summary = "保存订单任务设置信息")
    @Log(client = LogClient.admin,detail = "修改订单设置参数",level = LogLevel.important)
    public OrderSettingVO  save(@Valid OrderSettingVO setting){

        //调用二次验证
        this.adminTwoStepAuthentication.sensitive();

        this.settingClient.save(SettingGroup.TRADE,setting);
        return setting;
    }

}
