/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.MemberZpzzDO;
import cn.shoptnt.model.member.dto.ZpzzQueryParam;
import cn.shoptnt.service.member.MemberZpzzManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 会员增票资质API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Tag(name = "会员增票资质API")
@RestController
@RequestMapping("/admin/members/zpzz")
@Validated
public class MemberZpzzManagerController {

    @Autowired
    private MemberZpzzManager memberZpzzManager;

    @Operation(summary = "查询会员增票资质信息列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Valid ZpzzQueryParam zpzzQueryParam, @Parameter(hidden = true) Long pageNo,
                        @Parameter(hidden = true) Long pageSize) {
        return this.memberZpzzManager.list(pageNo, pageSize, zpzzQueryParam);
    }

    @Operation(summary = "查询会员增票资质详细")
    @Parameters({
            @Parameter(name = "id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{id}")
    public MemberZpzzDO get(@PathVariable Long id) {
        return this.memberZpzzManager.get(id);
    }

    @Operation(summary = "平台审核会员增票资质申请")
    @Parameters({
            @Parameter(name = "id", description = "主键ID", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "status", description = "审核状态", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "remark", description = "审核备注",   in = ParameterIn.QUERY)
    })
    @PostMapping(value = "audit/{id}/{status}")
    public String audit(@PathVariable Long id, @PathVariable String status, String remark) {
        this.memberZpzzManager.audit(id, status, remark);
        return "";
    }
}
