/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dto.HistoryQueryParam;
import cn.shoptnt.model.member.vo.ReceiptHistoryVO;
import cn.shoptnt.service.member.ReceiptHistoryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;

/**
 * 会员开票历史记录API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Tag(name = "会员开票历史记录API")
@RestController
@RequestMapping("/admin/members/receipts")
@Validated
public class ReceiptManagerController {

    @Autowired
    private ReceiptHistoryManager receiptHistoryManager;

    @Operation(summary = "查询会员开票历史记录信息列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页数",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数",  in = ParameterIn.QUERY),
    })
    @GetMapping()
    public WebPage list(@Valid HistoryQueryParam params, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        WebPage page = this.receiptHistoryManager.list(pageNo, pageSize, params);
        return page;
    }


    @Operation(summary = "查询会员开票历史记录详细")
    @Parameters({
            @Parameter(name = "history_id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{history_id}")
    public ReceiptHistoryVO get(@PathVariable("history_id") Long historyId) {
        return this.receiptHistoryManager.get(historyId);
    }


}
