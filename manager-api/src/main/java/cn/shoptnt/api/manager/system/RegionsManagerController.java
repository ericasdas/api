/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.Regions;
import cn.shoptnt.model.system.vo.RegionsVO;
import cn.shoptnt.service.system.RegionsManager;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.util.BeanUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 地区控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-18 11:45:03
 */
@RestController
@RequestMapping("/admin/systems/regions")
@Tag(name = "地区相关API")
public class RegionsManagerController {

    @Autowired
    private RegionsManager regionsManager;

    @Operation(summary = "添加地区")
    @PostMapping
    public Regions add(@Valid RegionsVO regionsVO) {
        return this.regionsManager.add(regionsVO);

    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改地区")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public Regions edit(@Valid RegionsVO regionsVO, @PathVariable Long id) {
        Regions regions = regionsManager.getModel(id);
        if (regions == null) {
            throw new ResourceNotFoundException("当前地区不存在");
        }
        BeanUtil.copyProperties(regionsVO, regions);
        this.regionsManager.edit(regions, id);
        return regions;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除地区")
    @Parameters({
            @Parameter(name = "id", description = "要删除的地区主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.regionsManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个地区")
    @Parameters({
            @Parameter(name = "id", description = "要查询的地区主键", required = true,  in = ParameterIn.PATH)
    })
    public Regions get(@PathVariable Long id) {

        Regions regions = this.regionsManager.getModel(id);

        return regions;
    }


    @GetMapping(value = "/{id}/children")
    @Operation(summary = "获取某地区的子地区")
    @Parameters({
            @Parameter(name = "id", description = "地区id", required = true,  in = ParameterIn.PATH)
    })
    public List<Regions> getChildrenById(@PathVariable Long id) {

        return regionsManager.getRegionsChildren(id);
    }

}
