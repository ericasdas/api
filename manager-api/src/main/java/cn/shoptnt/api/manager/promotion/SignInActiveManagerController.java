package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.sign.dto.SignInActivityOperation;
import cn.shoptnt.model.promotion.sign.dto.SignInActivitySearch;
import cn.shoptnt.model.promotion.sign.vos.SignInActivityInfo;
import cn.shoptnt.service.promotion.sign.SignInActivityManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * @author zh
 * @version 1.0
 * @title SignInActiveManagerController
 * @description 签到活动相关api
 * @program: api
 * 2024/3/12 14:57
 */
@RestController
@RequestMapping("/admin/promotion/sign/active")
@Tag(name = "签到活动相关Api")
@Validated
public class SignInActiveManagerController {


    @Autowired
    private SignInActivityManager signInActivityManager;

    @Operation(summary = "分页查询签到记录")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo,
                        @Parameter(hidden = true) Long pageSize,
                        SignInActivitySearch search) {
        return this.signInActivityManager.list(pageNo, pageSize, search);
    }


    @PutMapping(value = "/{id}/close")
    @Operation(summary = "关闭活动")
    @Parameters({
            @Parameter(name = "id", description = "签到活动id", required = true,  in = ParameterIn.PATH),
    })
    public void close(@PathVariable Long id) {
        this.signInActivityManager.close(id);
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "获取活动详细信息")
    @Parameters({
            @Parameter(name = "id", description = "签到活动id", required = true,  in = ParameterIn.PATH),
    })
    public SignInActivityInfo getInfo(@PathVariable Long id) {
        return this.signInActivityManager.getInfo(id);
    }


    @PostMapping
    @Operation(summary = "添加活动")
    public SignInActivityInfo add(@Valid @RequestBody SignInActivityOperation sign) {
        return signInActivityManager.add(sign);
    }


    @PutMapping("/{id}")
    @Operation(summary = "修改活动")
    @Parameters({
            @Parameter(name = "id", description = "签到活动id", required = true,  in = ParameterIn.PATH),
    })
    public SignInActivityInfo edit(@PathVariable Long id, @Valid @RequestBody SignInActivityOperation sign) {
        return signInActivityManager.edit(id, sign);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除活动")
    @Parameters({
            @Parameter(name = "id", description = "签到活动id", required = true, in = ParameterIn.PATH),
    })
    public void delete(@PathVariable Long id) {
        this.signInActivityManager.delete(id);
    }

}
