/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dto.MemberEditDTO;
import cn.shoptnt.model.member.dto.MemberQueryParam;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.StringUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * 会员控制器
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 11:33:56
 */
@RestController
@RequestMapping("/admin/members")
@Validated
@Tag(name = "会员后台管理API")
public class MemberManagerController {

    @Autowired
    private MemberManager memberManager;


    @Operation(summary = "查询会员列表")
    @GetMapping
    public WebPage list(@Valid MemberQueryParam memberQueryParam, @Parameter(hidden = true) Long pageNo,
                        @Parameter(hidden = true) Long pageSize) {
        memberQueryParam.setPageNo(pageNo);
        memberQueryParam.setPageSize(pageSize);
        return this.memberManager.list(memberQueryParam);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改会员")
    @Parameters({
            @Parameter(name = "password", description = "会员密码",   in = ParameterIn.QUERY),
            @Parameter(name = "mobile", description = "手机号码",   in = ParameterIn.QUERY),
            @Parameter(name = "remark", description = "会员备注",   in = ParameterIn.QUERY)
    })
    public Member edit(@Valid MemberEditDTO memberEditDTO, @PathVariable Long id, String password, String mobile, String remark) {
        Member member = memberManager.getModel(id);
        if (member == null) {
            throw new ResourceNotFoundException("当前会员不存在");
        }
        //如果密码不为空的话 修改密码
        if (!StringUtil.isEmpty(password)) {
            //退出会员信息
            memberManager.memberLoginout(id);
            //组织会员的新密码
            member.setPassword(StringUtil.md5(password + member.getUname().toLowerCase()));
        }
        member.setRemark(remark);
        member.setUname(member.getUname());
        member.setMobile(mobile);
        member.setTel(memberEditDTO.getTel());
        //复制属性
        BeanUtil.copyProperties(memberEditDTO, member);
        if (memberEditDTO.getRegion() != null) {
            BeanUtil.copyProperties(memberEditDTO.getRegion(), member);
        }
        this.memberManager.edit(member, id);
        return member;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除会员")
    @Parameters({
            @Parameter(name = "id", description = "要删除的会员主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.memberManager.disable(id);
        return "";
    }


    @PostMapping(value = "/{id}")
    @Operation(summary = "恢复会员")
    @Parameters({
            @Parameter(name = "id", description = "要恢复的会员主键", required = true,  in = ParameterIn.PATH)
    })
    public Member recovery(@PathVariable Long id) {
        Member member = memberManager.getModel(id);
        if (member == null) {
            throw new ResourceNotFoundException("当前会员不存在");
        }
        if (member.getDisabled().equals(-1)) {
            member.setDisabled(0);
            this.memberManager.edit(member, id);

            //发送会员解禁消息
            this.memberManager.recoverySendMsg(id);
        }
        return member;
    }

    @PostMapping(value = "/{id}/status")
    @Operation(summary = "将休眠会员更改为正常状态")
    @Parameters({
            @Parameter(name = "id", description = "要恢复的会员主键", required = true,  in = ParameterIn.PATH)
    })
    public Member editStatus(@PathVariable Long id) {
        Member member = memberManager.getModel(id);
        if (member == null) {
            throw new ResourceNotFoundException("当前会员不存在");
        }
        member.setStatus(1);
        this.memberManager.edit(member, id);
        return member;
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个会员")
    @Parameters({
            @Parameter(name = "id", description = "要查询的会员主键", required = true,  in = ParameterIn.PATH)
    })
    public Member get(@PathVariable Long id) {
        return this.memberManager.getModel(id);
    }


    @PostMapping
    @Operation(summary = "平台添加会员")
    @Parameters({
            @Parameter(name = "password", description = "会员密码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uname", description = "会员用户名", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.QUERY)
    })
    public Member addMember(@Valid MemberEditDTO memberEditDTO, @NotEmpty(message = "会员密码不能为空") String password, @Length(min = 2, max = 20, message = "用户名长度必须在2到20位之间") String uname, String mobile) {
        Member member = new Member();
        member.setUname(uname);
        member.setPassword(password);
        member.setNickname(memberEditDTO.getNickname());
        member.setMobile(mobile);
        member.setTel(memberEditDTO.getTel());
        BeanUtil.copyProperties(memberEditDTO, member);
        BeanUtil.copyProperties(memberEditDTO.getRegion(), member);
        //会员注册
        memberManager.register(member);
        return member;

    }


    @GetMapping(value = "/{member_ids}/list")
    @Operation(summary = "查询多个会员的基本信息")
    @Parameters({
            @Parameter(name = "member_ids", description = "要查询的会员的主键", required = true,  in = ParameterIn.PATH)})
    public List<Member> getGoodsDetail(@PathVariable("member_ids") Long[] memberIds) {
        return this.memberManager.getMemberByIds(memberIds);

    }

    @GetMapping(value = "/{id}/lock/status")
    @Operation(summary = "获取会员锁定状态")
    @Parameters({
            @Parameter(name = "id", description = "会员主键", required = true,  in = ParameterIn.PATH)
    })
    public Boolean getLockStatus(@PathVariable Long id) {
        return memberManager.getLockStatus(id);
    }

    @PostMapping(value = "/{id}/unlock")
    @Operation(summary = "解除会员锁定状态")
    @Parameters({
            @Parameter(name = "id", description = "会员主键", required = true,  in = ParameterIn.PATH)
    })
    public void unLock(@PathVariable Long id) {
        memberManager.unLock(id);
    }


}
