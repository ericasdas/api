/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.UploaderDO;
import cn.shoptnt.model.system.dos.WayBillDO;
import cn.shoptnt.model.system.vo.ValidatorPlatformVO;
import cn.shoptnt.model.system.vo.WayBillVO;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import cn.shoptnt.service.system.WaybillManager;

/**
 * 电子面单控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-06-08 16:26:05
 */
@RestController
@RequestMapping("/admin/systems/waybills")
@Tag(name = "电子面单相关API")
public class WaybillManagerController {

    @Autowired
    private WaybillManager waybillManager;


    @Operation(summary = "查询电子面单列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.waybillManager.list(pageNo, pageSize);
    }

    @Operation(summary = "修改电子面单")
    @PutMapping(value = "/{bean}")
    @Parameters({
            @Parameter(name = "bean", description = "电子面单bean id", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "wayBillVO", description = "电子面单对象", required = true)
    })
    public WayBillVO edit(@PathVariable String bean, @RequestBody @Parameter(hidden = true) WayBillVO wayBillVO) {
        wayBillVO.setBean(bean);
        return this.waybillManager.edit(wayBillVO);
    }


    @PutMapping(value = "/{bean}/open")
    @Operation(summary = "开启电子面单")
    @Parameters({
            @Parameter(name = "bean", description = "bean", required = true,   in = ParameterIn.PATH)
    })
    public String open(@PathVariable String bean) {
        this.waybillManager.open(bean);
        return null;
    }

    @Operation(summary = "获取电子面单配置")
    @GetMapping("/{bean}")
    @Parameter(name = "bean", description = "电子面单bean id", required = true,   in = ParameterIn.PATH)
    public WayBillVO getWaybillSetting(@PathVariable String bean) {
        return waybillManager.getWaybillConfig(bean);
    }

}
