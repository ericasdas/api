/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dos.BrandDO;
import cn.shoptnt.service.goods.BrandManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 品牌控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-16 16:32:46
 */
@RestController
@RequestMapping("/admin/goods/brands")
@Tag(name = "品牌相关API")
public class BrandManagerController {
    @Autowired
    private BrandManager brandManager;

    @Operation(summary = "查询品牌列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "品牌名称",   in = ParameterIn.QUERY)})
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo,
                        @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize,
                        String name) {

        return this.brandManager.list(pageNo, pageSize,name);
    }

    @Operation(summary = "添加品牌")
    @PostMapping
    public BrandDO add(@Valid BrandDO brand) {

        this.brandManager.add(brand);

        return brand;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改品牌")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)})
    public BrandDO edit(@Valid BrandDO brand, @PathVariable Long id) {

        this.brandManager.edit(brand, id);

        return brand;
    }

    @DeleteMapping(value = "/{ids}")
    @Operation(summary = "删除品牌")
    @Parameters({
            @Parameter(name = "ids", description = "要删除的品牌主键集合", required = true,  in = ParameterIn.PATH)})
    public String delete(@PathVariable Long[] ids) {

        this.brandManager.delete(ids);

        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个品牌")
    @Parameters({
            @Parameter(name = "id", description = "要查询的品牌主键", required = true,  in = ParameterIn.PATH)})
    public BrandDO get(@PathVariable Long id) {

        BrandDO brand = this.brandManager.getModel(id);

        return brand;
    }

    @GetMapping(value = "/all")
    @Operation(summary = "查询所有品牌")
    public List<BrandDO> listAll() {

        List<BrandDO> brands = this.brandManager.getAllBrands();

        return brands;
    }

}
