/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.model.member.dto.ConnectSettingDTO;
import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import cn.shoptnt.model.member.vo.ConnectSettingParametersVO;
import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.framework.util.JsonUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author zjp
 * @version v7.0
 * @Description 信任登录后台测试用例
 * @ClassName ConnectControllerTest
 * @since v7.0 下午8:40 2018/6/26
 */
@Transactional(value = "memberTransactionManager", rollbackFor = Exception.class)
public class MemberConnectControllerTest extends BaseTest {
    @Autowired
    @Qualifier("systemDaoSupport")
    private DaoSupport systemDaoSupport;

    @Autowired
    private SettingManager settingManager;

    /**
     * 编辑授权登录参数测试
     *
     * @throws Exception
     */
    @Test
    public void editConnectSettingTest() throws Exception {
        ConnectSettingDTO connectSettingDTO = new ConnectSettingDTO();
        connectSettingDTO.setName("qq");
        connectSettingDTO.setType(ConnectTypeEnum.QQ.value());
        List<ConnectSettingParametersVO> list = JsonUtil.jsonToList("[\n" +
                "      {\n" +
                "        \"name\": \"网页端参数 （PC，WAP，微信网页端）\",\n" +
                "        \"configList\": [\n" +
                "          {\n" +
                "            \"key\": \"qq_pc_app_id\",\n" +
                "            \"name\": \"AppId\",\n" +
                "            \"value\": \"101374797\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"key\": \"qq_pc_app_key\",\n" +
                "            \"name\": \"AppKey\",\n" +
                "            \"value\": \"dd69523f13737bc04209863eb8f48f50\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"原生-APP参数(安卓)\",\n" +
                "        \"configList\": [\n" +
                "          {\n" +
                "            \"key\": \"qq_native_android_app_id\",\n" +
                "            \"name\": \"AppId\",\n" +
                "            \"value\": null\n" +
                "          },\n" +
                "          {\n" +
                "            \"key\": \"qq_native_android_app_key\",\n" +
                "            \"name\": \"AppKey\",\n" +
                "            \"value\": null\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"原生-APP参数(IOS)\",\n" +
                "        \"configList\": [\n" +
                "          {\n" +
                "            \"key\": \"qq_native_ios_app_id\",\n" +
                "            \"name\": \"AppId\",\n" +
                "            \"value\": null\n" +
                "          },\n" +
                "          {\n" +
                "            \"key\": \"qq_native_ios_app_key\",\n" +
                "            \"name\": \"AppKey\",\n" +
                "            \"value\": null\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"RN-APP参数(安卓)\",\n" +
                "        \"configList\": [\n" +
                "          {\n" +
                "            \"key\": \"qq_rn_android_app_id\",\n" +
                "            \"name\": \"AppId\",\n" +
                "            \"value\": null\n" +
                "          },\n" +
                "          {\n" +
                "            \"key\": \"qq_rn_android_app_key\",\n" +
                "            \"name\": \"AppKey\",\n" +
                "            \"value\": null\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"RN-APP参数(IOS)\",\n" +
                "        \"configList\": [\n" +
                "          {\n" +
                "            \"key\": \"qq_rn_ios_app_id\",\n" +
                "            \"name\": \"AppId\",\n" +
                "            \"value\": null\n" +
                "          },\n" +
                "          {\n" +
                "            \"key\": \"qq_rn_ios_app_key\",\n" +
                "            \"name\": \"AppKey\",\n" +
                "            \"value\": null\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]", ConnectSettingParametersVO.class);
        connectSettingDTO.setClientList(list);

        mockMvc.perform(put("/admin/members/connect/" + ConnectTypeEnum.QQ.name())
                .header("Authorization", superAdmin)
                .content(JsonUtil.objectToJson(connectSettingDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));


    }


}
