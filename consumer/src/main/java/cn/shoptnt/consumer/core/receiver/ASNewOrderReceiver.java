/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.trade.aftersale.ASNewOrderDispatcher;
import cn.shoptnt.model.base.message.AsOrderStatusChangeMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商家创建换货或补发商品售后服务新订单接收者
 * 针对的是用户申请换货和补发商品的售后服务，商家审核通过后要生成新订单
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-23
 */
@Component
public class ASNewOrderReceiver {

    @Autowired
    private ASNewOrderDispatcher dispatcher;

    /**
     * 创建订单
     *
     * @param orderStatusChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.AS_SELLER_CREATE_ORDER + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.AS_SELLER_CREATE_ORDER, type = ExchangeTypes.FANOUT)
    ))
    public void createOrder(AsOrderStatusChangeMsg orderStatusChangeMsg) {
        dispatcher.dispatch(orderStatusChangeMsg);
    }

}
