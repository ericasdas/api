/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.system.SendEmailDispatcher;
import cn.shoptnt.model.base.vo.EmailVO;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送邮件
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:58
 */
@Component
public class SendEmailReceiver {

    @Autowired
    private SendEmailDispatcher dispatcher;

    /**
     * 发送邮件
     *
     * @param emailVO
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.EMAIL_SEND_MESSAGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.EMAIL_SEND_MESSAGE, type = ExchangeTypes.FANOUT)
    ))
    public void sendEmail(EmailVO emailVO) {
        dispatcher.dispatch(emailVO);
    }
}
