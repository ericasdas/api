/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.job.handler;

import cn.shoptnt.job.dispatcher.EveryHourDispatcher;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 每小时执行
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-06 上午4:24
 */
@Component
public class EveryHourExecuteJobHandler {

    @Autowired
    private EveryHourDispatcher dispatcher;

    @XxlJob("everyHourExecuteJobHandler")
    public ReturnT<String> execute(String param) {
        Boolean dispatch = dispatcher.dispatch();
        if (!dispatch) {
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}
