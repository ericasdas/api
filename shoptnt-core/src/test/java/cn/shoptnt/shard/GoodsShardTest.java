/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.shard;

import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.goods.dos.TagGoodsDO;
import cn.shoptnt.model.goods.dos.TagsDO;
import cn.shoptnt.model.goods.enums.TagType;
import cn.shoptnt.model.goods.vo.TagGoodsNum;
import cn.shoptnt.service.goods.TagsManager;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import java.util.List;

import static cn.shoptnt.framework.util.StringUtil.random;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/6/15
 */
@Rollback(false)
public class GoodsShardTest extends BaseTest {
    long sellerId = 1;
    @Autowired
    DaoSupport daoSupport;

    @Autowired
    TagsManager tagsManager;


    @Test
    public void queryTagGoodsTest() {
        TagGoodsNum goodsNum = tagsManager.queryGoodsNumByShopId(sellerId);
        
        List list =tagsManager.queryTagGoods(sellerId, 100, TagType.NEW.mark());
        
    }

    @Test
    public void goodsTagTest() {

        Long hotTagId = 1L;
        Long newTagId = 1L;
        TagType[] tags = TagType.values();
        for (TagType tag : tags) {
            TagsDO tagDO = new TagsDO(tag.tagName(), sellerId, tag.mark());

            this.daoSupport.insert(tagDO);

            Long tagId = daoSupport.getLastId("");
            if (tag.mark().equals(TagType.HOT.mark())) {
                hotTagId = tagId;
            }

            if (tag.mark().equals(TagType.NEW.mark())) {
                newTagId = tagId;
            }
        }

        
        

        for (int i = 1; i <= 10; i++) {
            GoodsDO goods = new GoodsDO();
            goods.setSellerId(Long.valueOf( random()));
            goods.setGoodsName("test" + i);
            goods.setQuantity(1);
            goods.setEnableQuantity(1);
            goods.setDisabled(1);
            goods.setMarketEnable(1);

            daoSupport.insert(goods);
            Long goodsId = daoSupport.getLastId("");

            TagGoodsDO hotTagGoods = new TagGoodsDO();
            hotTagGoods.setGoodsId(goodsId);
            hotTagGoods.setTagId(hotTagId);

            TagGoodsDO newTagGoods = new TagGoodsDO();
            newTagGoods.setGoodsId(goodsId);
            newTagGoods.setTagId(newTagId);

            this.daoSupport.insert( hotTagGoods);
            this.daoSupport.insert( newTagGoods);
        }

        this.queryTagGoodsTest();

    }

    @Test
    public void goodsSkuTest() {
        for (int i = 1; i <= 100; i++) {
            GoodsDO goods = new GoodsDO();
            goods.setSellerId(Long.valueOf(random()));
            goods.setGoodsName("test" + i);
            goods.setQuantity(1);
            goods.setEnableQuantity(1);

            daoSupport.insert(goods);

            GoodsSkuDO goodsSku = new GoodsSkuDO();
            BeanUtils.copyProperties(goods, goodsSku);
            goodsSku.setEnableQuantity(goodsSku.getQuantity());
            goodsSku.setHashCode(-1);
            this.daoSupport.insert("es_goods_sku", goodsSku);
        }
    }
}
