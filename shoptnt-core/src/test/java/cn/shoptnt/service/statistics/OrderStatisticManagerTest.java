/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 订单相关统计业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/04
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class OrderStatisticManagerTest {

    @Autowired
    private OrderStatisticManager orderStatisticManager;

    @Test
    public void getIncreaseMember() {

        String orderStatus = OrderStatusEnum.COMPLETE.value();

        
        
        
    }

    @Test
    public void getOrderPage() {

        String orderStatus = OrderStatusEnum.COMPLETE.value();

        
        
        
    }

    @Test
    public void getOrderNum() {

        String orderStatus = OrderStatusEnum.COMPLETE.value();

        
        
        
    }

    @Test
    public void getSalesMoney() {

        
        
        
    }

    @Test
    public void getAfterSalesMoney() {

        
        
        
    }

    @Test
    public void getSalesMoneyTotal() {

        
        
        
    }

    @Test
    public void getOrderRegionMember() {

        
        
        
    }

    @Test
    public void getOrderRegionNum() {

        
        
        
    }

    @Test
    public void getOrderRegionMoney() {

        
        
        
    }

    @Test
    public void getOrderRegionForm() {

        
        
        
    }

    @Test
    public void getUnitPrice() {

        Integer[] prices = {0, 10, 20};

        
        
        
    }

    @Test
    public void getUnitNum() {

        
    }

    @Test
    public void getUnitTime() {

        
        
        
    }

    @Test
    public void getReturnMoney() {

        
        
        
    }

    private SearchCriteria getSearchCriteria1(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2019);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria2(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(555l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria3(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

}
