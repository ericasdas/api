/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.mapper.statistics.ShopProfileStatisticsMapper;
import cn.shoptnt.model.statistics.vo.ShopProfileVO;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 商家中心，店铺概况业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/05
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class ShopProfileStatisticManagerTest {

    @Autowired
    private ShopProfileStatisticsManager shopProfileStatisticsManager;

    @Autowired
    ShopProfileStatisticsMapper shopProfileStatisticsMapper;

    @Test
    public void data() {

        ShopProfileVO data = shopProfileStatisticsManager.data();

        
    }

    @Test
    public void chart() {

        SimpleChart chart = shopProfileStatisticsManager.chart();

        
    }


}
