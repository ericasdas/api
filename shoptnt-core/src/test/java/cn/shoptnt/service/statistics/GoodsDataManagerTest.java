/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.statistics.dto.GoodsData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 商品收集业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class GoodsDataManagerTest {

    @Autowired
    private GoodsDataManager goodsDataManager;

    @Test
    public void deleteGoods() {

        Long[] goodIds = {123l, 456l, 3l, 4l};

        goodsDataManager.deleteGoods(goodIds);
    }

    @Test
    public void updateCollection() {

        GoodsData goodsData = new GoodsData();
        goodsData.setGoodsId(29043976057270273l);
        goodsData.setFavoriteNum(5);

        goodsDataManager.updateCollection(goodsData);
    }

    @Test
    public void get() {

        GoodsData goodsData = goodsDataManager.get(29043976057270273l);

        
    }

    @Test
    public void underAllGoods() {

        goodsDataManager.underAllGoods(18l);
    }

}
