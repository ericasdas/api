/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system;

import com.baomidou.mybatisplus.extension.api.R;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.model.system.dos.Regions;
import cn.shoptnt.model.system.vo.RegionsVO;
import cn.shoptnt.service.statistics.CollectFrontStatisticsManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 商家中心，商品收藏统计业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class RegionsManagerTest {

    @Autowired
    private RegionsManager regionsManager;


    @Autowired
    private Cache cache;

    @Test
    public void edit() {

        Regions regions = new Regions();

        regions.setId(2799L);
        regions.setLocalName("三环以内111");
        regions.setParentId(72L);
//        regions.setRegionPath(",1,72,2799,");
//        regions.setRegionGrade(3);
        regions.setCod(1);

        regionsManager.edit(regions, 2799L);
    }

    @Test
    public void add() {

        RegionsVO regionsVO = new RegionsVO();
        regionsVO.setCod(1);
        regionsVO.setLocalName("333");
        regionsVO.setParentId(2799L);
        regionsVO.setZipcode("xxx");

        regionsManager.add(regionsVO);
    }


    @Test
    public void getRegionsChildren() {

        
        
        
        
    }

    @Test
    public void clearRegionsCache() {
        Integer depth = 1;
        Object obj = this.cache.get(CachePrefix.REGIONLIDEPTH.getPrefix() + depth);
        List<Regions> regions = (List<Regions>) obj;
        

        depth = 2;
        obj = this.cache.get(CachePrefix.REGIONLIDEPTH.getPrefix() + depth);
        regions = (List<Regions>) obj;
        

        depth = 3;
        obj = this.cache.get(CachePrefix.REGIONLIDEPTH.getPrefix() + depth);
        regions = (List<Regions>) obj;
        

        depth = 4;
        obj = this.cache.get(CachePrefix.REGIONLIDEPTH.getPrefix() + depth);
        regions = (List<Regions>) obj;
        
    }

}
