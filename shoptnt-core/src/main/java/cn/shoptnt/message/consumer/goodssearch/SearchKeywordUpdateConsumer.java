/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.goodssearch;

import cn.shoptnt.message.event.SearchKeywordEvent;
import cn.shoptnt.client.goods.SearchKeywordClient;
import cn.shoptnt.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author liuyulei
 * @version 1.0
 * @Description:  搜索关键词记录历史
 * @date 2019/5/27 12:05
 * @since v7.0
 */
@Service
public class SearchKeywordUpdateConsumer  implements SearchKeywordEvent {

    @Autowired
    private SearchKeywordClient searchKeywordClient;

    @Override
    public void updateOrAdd(String keyword) {

        if(!StringUtil.isEmpty(keyword)){
            //判断关键词是否存在
            boolean isExit = this.searchKeywordClient.isExist(keyword);
            if(!isExit){
                //新增关键词
                this.searchKeywordClient.add(keyword);
            }else{
                //更新关键词
                this.searchKeywordClient.update(keyword);
            }
        }

    }
}
