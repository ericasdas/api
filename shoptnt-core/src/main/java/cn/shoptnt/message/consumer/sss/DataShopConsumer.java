/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.sss;

import cn.shoptnt.message.event.ShopCollectionEvent;
import cn.shoptnt.message.event.ShopStatusChangeEvent;
import cn.shoptnt.model.base.message.ShopStatusChangeMsg;
import cn.shoptnt.client.member.ShopClient;
import cn.shoptnt.client.statistics.GoodsDataClient;
import cn.shoptnt.client.statistics.ShopDataClient;
import cn.shoptnt.model.shop.enums.ShopStatusEnum;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.model.statistics.dto.ShopData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 店铺数据收集
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-23 下午5:50
 */
@Component
public class DataShopConsumer implements ShopCollectionEvent, ShopStatusChangeEvent {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private ShopClient shopClient;

    @Autowired
    private ShopDataClient shopDataClient;

    @Autowired
    private GoodsDataClient goodsDataClient;

    /**
     * 店铺变更
     *
     * @param shopId
     */
    @Override
    public void shopChange(Long shopId) {

        try {
            ShopVO shop = shopClient.getShop(shopId);
            ShopData shopData = new ShopData(shop);
            this.shopDataClient.add(shopData);
        } catch (Exception e) {
            logger.error("店铺消息收集失败", e);
        }
    }

    /**
     * 状态变更
     *
     * @param shopStatusChangeMsg 店铺信息
     */
    @Override
    public void changeStatus(ShopStatusChangeMsg shopStatusChangeMsg) {
        if (shopStatusChangeMsg.getStatusEnum().equals(ShopStatusEnum.CLOSED)) {
            goodsDataClient.underAllGoods(shopStatusChangeMsg.getSellerId());
        }
    }
}
