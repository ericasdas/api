/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.trade;

import cn.shoptnt.client.trade.OrderOperateClient;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.OrderOutStatus;
import cn.shoptnt.model.trade.order.enums.OrderOutStatusEnum;
import cn.shoptnt.model.trade.order.enums.OrderOutTypeEnum;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 创建订单出库状态设置消费者
 * @author duanmingyu
 * @version 1.0
 * @since 7.2.1
 * @date 2020-07-09
 */
@Component
@Order(1)
public class OrderOutStatusConsumer implements OrderStatusChangeEvent {

    @Autowired
    private OrderOperateClient orderOperateClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        OrderDO orderDO = orderMessage.getOrderDO();
        //如果订单状态为新订单
        if (OrderStatusEnum.NEW.value().equals(orderDO.getOrderStatus())) {
            for (String type : OrderOutTypeEnum.getAll()) {
                OrderOutStatus orderOutStatus = new OrderOutStatus();
                orderOutStatus.setOrderSn(orderDO.getSn());
                orderOutStatus.setOutType(type);
                orderOutStatus.setOutStatus(OrderOutStatusEnum.WAIT.name());
                this.orderOperateClient.add(orderOutStatus);
            }
        }
    }
}
