/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.sss;

import cn.shoptnt.message.event.GoodsChangeEvent;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import cn.shoptnt.client.goods.GoodsWordsClient;
import cn.shoptnt.client.statistics.GoodsDataClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商品数据收集消费者
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018/6/20 下午2:19
 * @Description:
 *
 */
@Component
public class DataGoodsConsumer implements GoodsChangeEvent {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private GoodsDataClient goodsDataClient;

	@Autowired
	private GoodsWordsClient goodsWordsClient;

	@Override
    public void goodsChange(GoodsChangeMsg goodsChangeMsg){

		try {
			if (goodsChangeMsg.getGoodsIds() == null || goodsChangeMsg.getGoodsIds().length == 0) {
                return;
            }
			// 添加商品
			if (goodsChangeMsg.getOperationType().equals(GoodsChangeMsg.ADD_OPERATION)) {
                this.goodsDataClient.addGoods(goodsChangeMsg.getGoodsIds());
            }
			// 修改商品
			if (goodsChangeMsg.getOperationType().equals(GoodsChangeMsg.UPDATE_OPERATION)) {
                this.goodsDataClient.updateGoods(goodsChangeMsg.getGoodsIds());
            }
			// 删除商品 暂时不删除统计表重的商品
//			if (goodsChangeMsg.getOperationType().equals(GoodsChangeMsg.DEL_OPERATION)) {
//                this.goodsDataClient.deleteGoods(goodsChangeMsg.getGoodsIds());
//            }
			this.goodsWordsClient.batchUpdateGoodsNum();
		} catch (Exception e) {
			logger.error("商品消息监听异常：",e);
			e.printStackTrace();
		}
	}

}
