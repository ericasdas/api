package cn.shoptnt.message.standlone.goods;

import cn.shoptnt.message.dispatcher.goods.GoodsWordsChangeDispatcher;
import cn.shoptnt.model.base.message.GoodsWordChangeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 提示词变化 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class GoodsWordsChangeListener {

    @Autowired
    private GoodsWordsChangeDispatcher goodsWordsChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void goodsChange(GoodsWordChangeMessage wordChangeMessage) {
        goodsWordsChangeDispatcher.dispatch(wordChangeMessage);
    }
}
