package cn.shoptnt.message.standlone.member;

import cn.shoptnt.message.dispatcher.member.MemberHistoryDispatcher;
import cn.shoptnt.model.member.dto.HistoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 会员足迹 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class MemberHistoryListener {

    @Autowired
    private MemberHistoryDispatcher memberHistoryDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void createIndexPage(HistoryDTO historyDTO) {
        memberHistoryDispatcher.dispatch(historyDTO);
    }
}
