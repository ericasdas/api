package cn.shoptnt.message.standlone.system;

import cn.shoptnt.message.dispatcher.system.SmsSendMessageDispatcher;
import cn.shoptnt.model.base.vo.SmsSendVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 发送短信 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class SmsSendMessageListener {

    @Autowired
    private SmsSendMessageDispatcher smsSendMessageDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void sendMessage(SmsSendVO smsSendVO) {
        smsSendMessageDispatcher.dispatch(smsSendVO);
    }
}
