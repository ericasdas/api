package cn.shoptnt.message.delay;

import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.client.trade.OrderOperateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.trigger.Interface.TimeTriggerExecuter;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.enums.PayStatusEnum;
import cn.shoptnt.model.trade.order.vo.CancelVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 新订单超时未付款自动取消执行器
 *
 * @author zhangsong
 * @version v7.3.0
 * @since 1.0.0
 * @date 2021/02/25
 */
@Component("orderAutoCancelTriggerExecuter")
public class OrderAutoCancelTriggerExecuter implements TimeTriggerExecuter {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private OrderOperateClient orderOperateClient;

    @Override
    public void execute(Object object) {
        OrderStatusChangeMsg orderStatusChangeMsg = (OrderStatusChangeMsg) object;

        String orderSn = orderStatusChangeMsg.getOrderDO().getSn();
        OrderDO order = orderClient.getOrder(orderSn);

        //如果订单状态为已确认并且支付状态为未支付，则取消订单
        if (OrderStatusEnum.CONFIRM.value().equals(order.getOrderStatus()) && PayStatusEnum.PAY_NO.value().equals(order.getPayStatus())) {
            CancelVO cancel = new CancelVO();
            cancel.setOrderSn(orderSn);
            cancel.setReason("超时未付款");
            cancel.setOperator("系统自动处理");
            this.orderOperateClient.cancel(cancel, OrderPermission.client);
        }
    }

}
