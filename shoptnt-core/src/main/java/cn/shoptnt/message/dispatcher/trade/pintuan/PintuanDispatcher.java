package cn.shoptnt.message.dispatcher.trade.pintuan;


import cn.shoptnt.message.event.PintuanSuccessEvent;
import cn.shoptnt.model.base.message.PinTuanSuccessMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 拼团 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class PintuanDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PintuanSuccessEvent> events;

    public void dispatch(PinTuanSuccessMessage pinTuanSuccessMessage) {
        if (events != null) {
            for (PintuanSuccessEvent event : events) {
                try {
                    event.success(pinTuanSuccessMessage.getPinTuanOrderId());
                } catch (Exception e) {
                    logger.error("拼团成功消息", e);
                }
            }
        }
    }
}
