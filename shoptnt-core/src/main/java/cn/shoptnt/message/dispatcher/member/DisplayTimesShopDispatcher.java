package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.client.statistics.DisplayTimesClient;
import cn.shoptnt.model.base.message.ShopViewMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 店铺流量统计 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class DisplayTimesShopDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DisplayTimesClient displayTimesClient;

    public void dispatch(ShopViewMessage shopViewMessage) {
        try {
            displayTimesClient.countShop(shopViewMessage.getShopPageViews());
        } catch (Exception e) {
            logger.error("流量整理：店铺 异常", e);
        }
    }
}
