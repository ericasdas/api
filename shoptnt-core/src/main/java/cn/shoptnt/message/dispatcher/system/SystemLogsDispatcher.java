package cn.shoptnt.message.dispatcher.system;

import cn.shoptnt.message.event.SystemLogsEvent;
import cn.shoptnt.model.system.dos.SystemLogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统日志 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class SystemLogsDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<SystemLogsEvent> events;

    public void dispatch(SystemLogs log) {
        if (events != null) {
            for (SystemLogsEvent event : events) {
                try {
                    event.add(log);
                } catch (Exception e) {
                    logger.error("处理系统日志出错", e);
                }
            }
        }
    }
}
