/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.base.message.ShopStatusChangeMsg;

/**
 * 店铺状态变更
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/9/9 下午11:04
 */
public interface ShopStatusChangeEvent {

    /**
     * 状态变更
     *
     * @param shopStatusChangeMsg 店铺信息
     */
    void changeStatus(ShopStatusChangeMsg shopStatusChangeMsg);
}
