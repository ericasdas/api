/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.base.message.GoodsChangeMsg;

import java.io.IOException;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: TODO商品优先级变化
 * @date 2019/6/13 17:15
 * @since v7.0
 */
public interface GoodsPriorityChangeEvent {


    /**
     * 商品优先级变化
     * @param goodsChangeMsg
     */
    void priorityChange(GoodsChangeMsg goodsChangeMsg) throws IOException;
}
