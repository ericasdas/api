/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.service.security.BaseSignScanTask;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * sku签名数据扫描
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/19 16:23
 **/
@Service("skuSignScanTask")
public class SkuSignScanTask extends BaseSignScanTask<GoodsSkuDO> {

    @Autowired
    private GoodsClient goodsClient;

    @Override
    protected ScanResult scanModule(String rounds) throws IllegalAccessException {
        QueryWrapper<GoodsSkuDO> queryWrapper = createWrapper(rounds);
        ScanModuleDTO<GoodsSkuDO> scanModuleDTO = new ScanModuleDTO<>(rounds, queryWrapper, pageSize);
        return goodsClient.scanSkuModule(scanModuleDTO);
    }
    @Override
    public void reSign() {
        goodsClient.reSignSku();
    }

}
