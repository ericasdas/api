/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.client.member.MemberWalletClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.client.trade.TradeClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.security.FalsifyRecordMapper;
import cn.shoptnt.model.errorcode.SystemErrorCode;
import cn.shoptnt.model.security.FalsifyRecordParams;
import cn.shoptnt.service.security.FalsifyRecordManager;
import cn.shoptnt.model.security.FalsifyRecord;
import cn.shoptnt.model.security.FalsifyStatusEnum;
import cn.shoptnt.model.security.SecurityModuleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author shen
 * @Date 2021/11/21 14:23
 */
@Service
public class FalsifyRecordManagerImpl implements FalsifyRecordManager {
    @Autowired
    private FalsifyRecordMapper falsifyRecordMapper;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private MemberWalletClient memberWalletClient;

    @Autowired
    private TradeClient tradeClient;

    @Override
    public WebPage list(FalsifyRecordParams params) {

        QueryWrapper<FalsifyRecord> wrapper = new QueryWrapper<>();
        wrapper.eq(!StringUtil.isEmpty(params.getModule()), "module", params.getModule());
        wrapper.eq(!StringUtil.isEmpty(params.getState()), "state", params.getState());
        IPage<FalsifyRecord> iPage = falsifyRecordMapper.selectPage(new Page<>(params.getPageNo(), params.getPageSize()), wrapper);

        return PageConvert.convert(iPage);
    }

    @Override
    public void repair(Long recordId) {

        FalsifyRecord falsifyRecord = falsifyRecordMapper.selectById(recordId);
        if (falsifyRecord == null) {
            throw new ServiceException(SystemErrorCode.E953.code(), "获取篡改记录信息异常");
        }
        Long dataId = falsifyRecord.getDataId();
        //判断被篡改的数据模块
        switch (SecurityModuleEnum.valueOf(falsifyRecord.getModule())) {
            //商品模块 查询更新数据
            case goods:
                goodsClient.repair(dataId);
                break;
            case sku:
                goodsClient.repairSku(dataId);
                break;
            //订单模块  查询更新数据
            case order:
                orderClient.repair(dataId);
                break;
            //会员模块  查询更新数据
            case member:
                memberClient.repair(dataId);
                break;
            //预存款模块  查询更新数据
            case deposit:
                memberWalletClient.repair(dataId);
                break;
            //交易模块  查询更新数据
            case trade:
                tradeClient.repair(dataId);
                break;
            default:
        }
        //更新篡改记录状态
        UpdateWrapper<FalsifyRecord> wrapper = new UpdateWrapper<>();
        wrapper.set("state", FalsifyStatusEnum.repaired.name()).eq("record_id", recordId);
        falsifyRecordMapper.update(new FalsifyRecord(), wrapper);
    }

    @Override
    public void add(FalsifyRecord falsifyRecord) {
        falsifyRecordMapper.insert(falsifyRecord);
    }
}
