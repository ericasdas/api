/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import cn.shoptnt.model.trade.order.vo.TradeVO;

/**
 * 交易管理
 * @author Snow create in 2018/4/9
 * @version v2.0
 * @since v7.0.0
 */
public interface TradeManager {

    /**
     * 交易创建
     * @param clientType  客户的类型
     * @param way 检查获取方式
     * @return 交易VO
     */
    TradeVO createTrade(String clientType, CheckedWay way);

    /**
     * 交易数据签名扫描
     * @return
     */
    ScanResult scanModule(ScanModuleDTO scanModuleDTO);

    /**
     * 交易数据重新签名
     */
    void reSign();

    /**
     * 修复交易数据
     * @param tradeId
     */
    void repair(Long tradeId);
}
