/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.complain;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.trade.complain.dto.ComplainDTO;
import cn.shoptnt.model.trade.complain.dto.ComplainQueryParam;
import cn.shoptnt.model.trade.complain.vo.OrderComplainVO;
import cn.shoptnt.model.trade.order.vo.OrderFlowNode;
import cn.shoptnt.model.trade.complain.dos.OrderComplain;

import java.util.List;

/**
 * 交易投诉表业务层
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
public interface OrderComplainManager	{

	/**
	 * 查询交易投诉表列表
	 * @param param 条件参数
	 * @return WebPage 交易投诉表分页数据
	 */
	WebPage list(ComplainQueryParam param);
	/**
	 * 添加交易投诉表
	 * @param complain 交易投诉表
	 * @return OrderComplain 交易投诉表
	 */
	OrderComplain add(ComplainDTO complain);

	/**
	* 修改交易投诉表
	* @param orderComplain 交易投诉表
	* @param id 交易投诉表主键
	* @return OrderComplain 交易投诉表
	*/
	OrderComplain edit(OrderComplain orderComplain, Long id);
	
	/**
	 * 删除交易投诉表
	 * @param id 交易投诉表主键
	 */
	void delete(Long id);
	
	/**
	 * 获取交易投诉表
	 * @param id 交易投诉表主键
	 * @return OrderComplain  交易投诉表
	 */
	OrderComplain getModel(Long id);

	/**
	 * 撤销某个交易投诉
	 * @param id 投诉id
	 * @return 交易投诉表实体
	 */
    OrderComplain cancel(Long id);

	/**
	 * 审核并交由商家申诉
	 * @param id 投诉id
	 * @return 交易投诉表实体
	 */
	OrderComplain auth(Long id);

	/**
	 * 管理员仲裁结束流程
	 * @param id 投诉id
	 * @param arbitrationResult 仲裁结果
	 * @return 交易投诉表实体
	 */
	OrderComplain complete(Long id, String arbitrationResult);

	/**
	 * 商家申诉
	 * @param id 投诉id
	 * @param appealContent 申诉内容
	 * @param images 图片
	 * @return 交易投诉表实体
	 */
	OrderComplain appeal(Long id, String appealContent, String[] images);

	/**
	 * 提交仲裁
	 * @param id 投诉id
	 * @return 交易投诉表实体
	 */
	OrderComplain arbitrate(Long id);

	/**
	 * 获取交易投诉及对话信息
	 * @param id 投诉id
	 * @return 交易投诉表实体
	 */
    OrderComplainVO getModelAndCommunication(Long id);

	/**
	 * 查询交易投诉的流程图
	 * @param id 投诉id
	 * @return 交易投诉流程图
	 */
	List<OrderFlowNode> getComplainFlow(Long id);
}
