/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.member.impl;

import cn.shoptnt.framework.message.broadcast.BroadcastMessageSender;
import cn.shoptnt.model.member.dos.LogOffMember;
import cn.shoptnt.service.member.LogOffMemberManager;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.SystemLogsClient;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.database.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.security.model.TokenConstant;
import cn.shoptnt.mapper.member.MemberMapper;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.model.security.*;
import cn.shoptnt.model.shop.dos.ClerkDO;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.model.system.vo.AccountSetting;
import cn.shoptnt.service.member.MemberCollectionGoodsManager;
import cn.shoptnt.service.member.MemberCollectionShopManager;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dto.MemberQueryParam;
import cn.shoptnt.model.member.dto.MemberStatisticsDTO;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import cn.shoptnt.model.member.vo.MemberPointVO;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.model.trade.order.enums.CommentStatusEnum;
import cn.shoptnt.framework.auth.Token;
import cn.shoptnt.framework.context.request.ThreadContextHolder;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.TokenManager;
import cn.shoptnt.framework.security.message.UserDisableMsg;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.security.model.Role;
import cn.shoptnt.framework.util.*;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.service.shop.ClerkManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * 会员业务类
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 11:33:56
 */
@Service
public class MemberManagerImpl implements MemberManager {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private DirectMessageSender messageSender;
    @Autowired
    private MemberCollectionShopManager memberCollectionShopManager;
    @Autowired
    private MemberCollectionGoodsManager memberCollectionGoodsManager;
    @Autowired
    private OrderClient orderClient;
    @Autowired
    private BroadcastMessageSender broadcastMessageSender;
    @Autowired
    private ClerkManager clerkManager;
    @Autowired
    private Cache cache;
    @Autowired
    private SettingClient settingClient;
    @Autowired
    private SmsClient smsClient;

    @Autowired
    private SystemLogsClient systemLogsClient;

    @Autowired
    private LogOffMemberManager logOffMemberManager;


    /**
     * 查询会员列表
     *
     * @param memberQueryParam 查询条件
     * @return
     */
    @Override
    public WebPage list(MemberQueryParam memberQueryParam) {
        //新建查询条件包装器
        QueryWrapper<Member> wrapper = new QueryWrapper<>();

        //如果会员状态不为空
        if (memberQueryParam.getDisabled() != null) {
            //如果会员状态不等于正常或者删除状态，则默认以正常状态进行查询 0：正常，-1：删除
            if (memberQueryParam.getDisabled() != -1 && memberQueryParam.getDisabled() != 0) {
                wrapper.eq("disabled", 0);
            } else {
                //以会员状态为条件进行查询
                wrapper.eq("disabled", memberQueryParam.getDisabled());
            }
        } else {
            //以会员状态为正常状态进行查询
            wrapper.eq("disabled", 0);
        }
        //如果查询关键字不为空，则以会员用户名或者手机号或者会员昵称为条件进行模糊查询
        wrapper.and(StringUtil.notEmpty(memberQueryParam.getKeyword()), ew -> {
            ew.like("uname", memberQueryParam.getKeyword())
                    .or().eq("mobile", DbSecretUtil.encrypt(memberQueryParam.getKeyword(), TokenConstant.SECRET))
                    .or().like("nickname", memberQueryParam.getKeyword());
        });

        //如果会员手机号码不为空，则以会员手机号码为条件查询
        wrapper.eq(StringUtil.notEmpty(memberQueryParam.getMobile()), "mobile", DbSecretUtil.encrypt(memberQueryParam.getMobile(), TokenConstant.SECRET));
        //如果用户名不为空，则以用户名为条件模糊查询
        wrapper.like(StringUtil.notEmpty(memberQueryParam.getUname()), "uname", memberQueryParam.getUname());
        //如果会员邮箱不为空，则以会员邮箱为条件查询
        wrapper.eq(StringUtil.notEmpty(memberQueryParam.getEmail()), "email", memberQueryParam.getEmail());
        //如果会员性别不为空并且值等于1或2，则以会员性别为条件查询 1：男，0：女
        wrapper.eq(memberQueryParam.getSex() != null && (memberQueryParam.getSex() == 1 || memberQueryParam.getSex() == 0), "sex", memberQueryParam.getSex());
        //如果会员注册时间-时间范围起始时间不为空，则查询注册时间大于这个时间的会员信息
        wrapper.gt(StringUtil.notEmpty(memberQueryParam.getStartTime()), "create_time", memberQueryParam.getStartTime());
        //如果会员注册时间-时间范围结束时间不为空，则查询注册时间小于这个时间的会员信息
        wrapper.lt(StringUtil.notEmpty(memberQueryParam.getEndTime()), "create_time", memberQueryParam.getEndTime());

        //如果地区信息不为空，则以地区信息作为条件查询
        if (memberQueryParam.getRegion() != null) {
            wrapper.eq("province_id", memberQueryParam.getRegion().getProvinceId())
                    .eq("city_id", memberQueryParam.getRegion().getCityId())
                    .eq("county_id", memberQueryParam.getRegion().getCountyId());
            //如果地区中的乡镇id不等于0，则以乡镇ID作为条件查询
            wrapper.eq(!memberQueryParam.getRegion().getTownId().equals(0), "town_id", memberQueryParam.getRegion().getTownId());
        }
        //以会员注册时间倒序排序
        wrapper.orderByDesc("create_time");
        //获取会员分页列表数据
        IPage<Member> iPage = memberMapper.selectPage(new Page<>(memberQueryParam.getPageNo(), memberQueryParam.getPageSize()), wrapper);
        return PageConvert.convert(iPage);
    }

    /**
     * 修改会员登录次数
     *
     * @param memberId
     * @param now
     */
    @Override
    public void updateLoginNum(Long memberId, Long now) {
        //新建修改条件包装器
        UpdateWrapper<Member> wrapper = new UpdateWrapper<>();
        //修改会员最后登录时间以及登录次数+1，并以会员ID为修改条件
        wrapper.set("last_login", now).setSql("login_count = login_count+1").eq("member_id", memberId);
        //修改会员信息
        memberMapper.update(null, wrapper);
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Member edit(Member member, Long id) {
        //校验邮箱是否已经存在
        if (!StringUtil.isEmpty(member.getEmail())) {
            Member mb = this.getMemberByEmail(member.getEmail());
            if (mb != null && !mb.getMemberId().equals(id)) {
                throw new ServiceException(MemberErrorCode.E117.code(), "邮箱已经被占用");
            }
        }
        //校验用户名是否已经存在
        if (!StringUtil.isEmpty(member.getUname())) {
            Member mb = this.getMemberByName(member.getUname());
            if (mb != null && !mb.getMemberId().equals(id)) {
                throw new ServiceException(MemberErrorCode.E108.code(), "当前用户名已经被使用");
            }
        }

        //校验手机号码是否重复
        if (!StringUtil.isEmpty(member.getMobile())) {
            Member mb = this.getMemberByMobile(member.getMobile());
            if (mb != null && !mb.getMemberId().equals(id)) {
                throw new ServiceException(MemberErrorCode.E118.code(), "当前手机号已经被使用");
            }
        }
        //设置会员ID
        member.setMemberId(id);
        //修改会员信息
        memberMapper.updateById(member);
        return member;
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void disable(Long id) {
        //新建会员对象
        Member member = new Member();
        //设置会员状态为已删除
        member.setDisabled(-1);
        //设置会员ID
        member.setMemberId(id);
        //通过会员ID修改会员信息
        memberMapper.updateById(member);

        //发送redis订阅消息,通知各个节点此用户已经禁用
        UserDisableMsg userDisableMsg = new UserDisableMsg(id, Role.BUYER, UserDisableMsg.ADD);
        broadcastMessageSender.send(userDisableMsg);
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void recoverySendMsg(Long id) {
        //发送redis订阅消息,通知各个节点此用户已经解禁
        UserDisableMsg userDisableMsg = new UserDisableMsg(id, Role.BUYER, UserDisableMsg.DELETE);
        broadcastMessageSender.send(userDisableMsg);
    }

    @Override
    public void delete(Class<Member> clazz, Long id) {
        memberMapper.deleteById(id);
    }

    @Override
    public Member getModel(Long id) {
        return memberMapper.selectById(id);
    }

    @Override
    public MemberPointVO getMemberPoint() {
        //获取当前登录的会员信息
        Buyer buyer = UserContext.getBuyer();
        //通过当前登录的会员信息ID获取会员完整信息
        Member member = this.getModel(buyer.getUid());
        //非空校验
        if (member != null) {
            //新建会员积分对象VO
            MemberPointVO memberPointVO = new MemberPointVO();
            //如果会员消费积分不为空，则填充消费积分；如果为空，默认填充0积分
            if (member.getConsumPoint() != null) {
                memberPointVO.setConsumPoint(member.getConsumPoint());
            } else {
                memberPointVO.setConsumPoint(0L);
            }

            //如果会员等级积分不为空，则填充等级积分；如果为空，默认填充0积分
            if (member.getGradePoint() != null) {
                memberPointVO.setGradePoint(member.getGradePoint());
            } else {
                memberPointVO.setGradePoint(0L);
            }
            return memberPointVO;
        }
        throw new ResourceNotFoundException("此会员不存在！");

    }

    @Override
    public Member getMemberByName(String uname) {

        return memberMapper.getMemberByUname(uname);
    }

    @Override
    public Member getMemberByMobile(String mobile) {
        //如果是手机号格式，则用手机号加密查询
        if (Validator.isMobile(mobile)) {
            mobile = DbSecretUtil.encrypt(mobile, TokenConstant.SECRET);
        }

        return memberMapper.getMemberByMobile(mobile);
    }

    @Override
    public String[] generateMemberUname(String uname) {
        //如果用户输入的用户大于15位 则截取 拼接随机数5位，总长度不能大于二十
        if (uname.length() > 15) {
            uname = uname.substring(0, 15);

        }
        String[] strs = new String[2];
        int i = 0;
        while (true) {
            if (i > 1) {
                break;
            }
            String unameRandom = "" + (int) (Math.random() * (99999 - 10000 + 1));
            //根据拼接好的用户判断是否存在
            Member member = this.getMemberByName(uname + unameRandom);
            if (member == null) {
                strs[i] = uname + unameRandom;
                i++;
            }
        }
        return strs;
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Member register(Member member) {
        //手机号码校验
        Member m = this.getMemberByMobile(member.getMobile());
        if (m != null) {
            throw new ServiceException(MemberErrorCode.E107.code(), "该手机号已经被占用");
        }
        //用户名校验
        m = this.getMemberByName(member.getUname());
        if (m != null) {
            // 这里需要注意一下m_手机号，此用户名的手机号已更换，所以这里只验证用户名，不友好
            // 这里验证用户名是不是以m_手机组成，是的话，更换一个用户名，存储账号
            if (("m_" + member.getMobile()).equals(member.getUname())) {
                member.setUname("m_" + this.getStringRandom(11));
            } else {
                throw new ServiceException(MemberErrorCode.E107.code(), "当前会员已经注册");
            }
        }
        //邮箱校验
        if (!StringUtil.isEmpty(member.getEmail())) {
            m = this.getMemberByEmail(member.getEmail());
            if (m != null) {
                throw new ServiceException(MemberErrorCode.E117.code(), "邮箱已经被占用");
            }
        }
        //获取前端传入的密码
        String password = member.getPassword();
        //将密码+用户名进行MD5加密，然后放入会员对象中
        member.setPassword(StringUtil.md5(password + member.getUname().toLowerCase()));
        //设置会员注册时间为当前时间
        member.setCreateTime(DateUtil.getDateline());
        //设置会员等级积分为0
        member.setGradePoint(0L);
        //设置会员消费积分为0
        member.setConsumPoint(0L);
        //设置会员登录次数为0
        member.setLoginCount(0);
        //设置会员是否开启店铺 0：否，1：是
        member.setHaveShop(0);
        //设置会员是否被禁用 0：否，1：是
        member.setDisabled(0);
        //设置会员是否完善了个人信息 0：否，1：是
        member.setInfoFull(0);
        //会员信息入库
        memberMapper.insert(member);

        //组织数据结构发送会员注册消息
        MemberRegisterMsg memberRegisterMsg = new MemberRegisterMsg();
        memberRegisterMsg.setMember(member);
        //从请求头中获取uuid
        memberRegisterMsg.setUuid(ThreadContextHolder.getHttpRequest().getHeader("uuid"));
        this.messageSender.send(memberRegisterMsg);
        return member;
    }


    @Override
    public MemberVO login(String username, String password, Integer memberOrSeller) {
        //校验用户名密码
        Member member = this.validation(username, password);
        return this.loginHandle(member, memberOrSeller);
    }


    @Override
    public MemberVO mobileLogin(String phone, String smsCode, Integer memberOrSeller) {
        String accountSettingJson = settingClient.get(SettingGroup.ACCOUNT);
        AccountSetting accountSetting = JsonUtil.jsonToObject(accountSettingJson, AccountSetting.class);
        checkMobileStatus(phone, accountSetting);
        boolean isPass = smsClient.valid(SceneType.LOGIN.name(), phone, smsCode);
        if (!isPass) {
            int times = recordPsswordErrorCount(phone, accountSetting);
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码错误" + times + "次,超过" + accountSetting.getRetryTimes() + "次将被锁定");
        }
        //根据手机号获取会员信息
        Member member = this.getMemberByMobile(phone);
        if (member != null) {
            //检测会员是否可用
            member.checkDisable();
        } else {
            throw new ServiceException(MemberErrorCode.E107.code(), "手机号错误！");
        }
        //登录后处理
        return this.loginHandle(member, memberOrSeller);
    }

    /**
     * 检测手机号状态
     *
     * @param mobile
     * @param accountSetting
     */
    private void checkMobileStatus(String mobile, AccountSetting accountSetting) {
        Object lockFlag = cache.get(CachePrefix.LOCK_MOBILE.getPrefix() + mobile);
        if (lockFlag != null) {
            long endTime = (long) lockFlag;
            int minute = (int) CurrencyUtil.div(CurrencyUtil.sub(endTime, DateUtil.getDateline()), 60);
            minute = minute < 0 ? 1 : minute + 1;
            throw new ServiceException("500", "验证码错误超过" + accountSetting.getRetryTimes() + "次，您的手机号已经被锁定，请" + minute + "分钟后再试");
        }
    }

    /**
     * 记录密码错误次数,错误次数达到配置标准后设置账户锁定时间
     *
     * @param mobile
     * @param accountSetting
     */
    private int recordPsswordErrorCount(String mobile, AccountSetting accountSetting) {
        String errorPwdKey = CachePrefix.SMS_CODE_ERROR_NUM.getPrefix() + mobile;
        Object errorNum = cache.get(errorPwdKey);
        int times = 1;
        if (errorNum == null) {
            //首次错误，设置错误计数器
            int exp = CurrencyUtil.mul(accountSetting.getRetryCycle(), 3600).intValue();
            cache.put(errorPwdKey, times, exp);
        } else {
            times = (int) errorNum + 1;
            if (times >= accountSetting.getRetryTimes()) {
                //锁定账号
                lockMobile(mobile, CurrencyUtil.mul(accountSetting.getLockDuration(), 60).intValue());
                //删除密码错误次数记录
                cache.remove(errorPwdKey);
                throw new ServiceException("500", "验证码错误超过" + accountSetting.getRetryTimes() + "次，您的手机号已经被锁定，请" + accountSetting.getLockDuration() + "分钟后再试");
            } else {
                //增加密码错误次数
                cache.put(errorPwdKey, times);
            }
        }
        return times;
    }

    /**
     * 锁定手机号
     *
     * @param mobile
     * @param exp
     */
    private void lockMobile(String mobile, int exp) {
        long endTime = CurrencyUtil.add(DateUtil.getDateline(), exp).longValue();
        cache.put(CachePrefix.LOCK_MOBILE.getPrefix() + mobile, endTime, exp);
    }

    /**
     * 登录会员后的处理
     *
     * @param member         会员信息
     * @param memberOrSeller 会员还是卖家，1 会员  2 卖家
     */
    @Override
    public MemberVO loginHandle(Member member, Integer memberOrSeller) {
        //从请求header中获取用户的uuid
        String uuid = ThreadContextHolder.getHttpRequest().getHeader("uuid");
        //初始化会员信息
        MemberVO memberVO = this.convertMember(member, uuid);
        //发送登录消息
        this.sendMessage(member, memberOrSeller);

        return memberVO;
    }

    @Override
    public MemberVO connectLoginHandle(Member member, String uuid) {
        //初始化会员信息
        MemberVO memberVO = this.convertMember(member, uuid);
        //发送登录消息
        this.sendMessage(member, 1);
        return memberVO;
    }

    @Override
    public Member validation(String username, String password) {
        //用户名登录处理
        Member member = this.getMemberByName(username);
        //判断是否为uniapp注册账号，如果是且密码为空则提示去uniapp修改密码后登陆，add chushuai by 2020/10/09
        if (member != null && StringUtil.isEmpty(member.getPassword()) && username.startsWith("m_")) {
            throw new ServiceException(MemberErrorCode.E107.code(), "此账号为微信/支付宝等移动端三方授权账号，请在移动端登录并修改密码后在电脑端登录");
        }
        String accountSettingJson = settingClient.get(SettingGroup.ACCOUNT);
        AccountSetting accountSetting = JsonUtil.jsonToObject(accountSettingJson, AccountSetting.class);
        if (member != null) {
            //验证会员的账号密码
            if (!StringUtil.equals(member.getUname(), username)) {
                throw new ServiceException(MemberErrorCode.E107.code(), "账号密码错误！");
            }
            return checkPassword(password, member, accountSetting);
        }
        //手机号码登录处理
        member = this.getMemberByMobile(username);
        if (member != null) {
            //判断是否为uniapp注册账号，如果是且密码为空则提示去uniapp修改密码后登陆，add chushuai by 2020/10/09
            if (StringUtil.isEmpty(member.getPassword()) && member.getUname().startsWith("m_")) {
                throw new ServiceException(MemberErrorCode.E107.code(), "此账号为微信三方授权账号，请修改密码后登录！");
            }
            return checkPassword(password, member, accountSetting);
        }
        //邮箱登录处理
        member = this.getMemberByEmail(username);
        if (member != null) {
            return checkPassword(password, member, accountSetting);
        }
        throw new ServiceException(MemberErrorCode.E107.code(), "账号密码错误！");
    }

    /**
     * 验证密码
     *
     * @param password       用户输入的密码
     * @param member         会员DO
     * @param accountSetting 账号安全设置
     * @return
     */
    private Member checkPassword(String password, Member member, AccountSetting accountSetting) {
        String pwdmd5;
        checkAccountStatus(member, accountSetting);
        pwdmd5 = StringUtil.md5(password + member.getUname().toLowerCase());
        if (member.getPassword().equals(pwdmd5)) {
            return member;
        } else {
            int times = recordPsswordErrorCount(member.getMemberId(), accountSetting);
            throw new ServiceException(MemberErrorCode.E107.code(), "账号密码错误" + times + "次,超过" + accountSetting.getRetryTimes() + "次将被锁定");
        }
    }

    /**
     * 检测账户状态
     *
     * @param member
     * @param accountSetting
     */
    private void checkAccountStatus(Member member, AccountSetting accountSetting) {
        Integer status = member.getStatus();
        if (status != null && status == 0) {
            throw new ServiceException("500", "账户已休眠,请联系管理员");
        }
        Object lockFlag = cache.get(CachePrefix.LOCK_MEMBER.getPrefix() + member.getMemberId());
        if (lockFlag != null) {
            long endTime = (long) lockFlag;
            int minute = (int) CurrencyUtil.div(CurrencyUtil.sub(endTime, DateUtil.getDateline()), 60);
            minute = minute < 0 ? 1 : minute + 1;
            throw new ServiceException("500", "密码错误超过" + accountSetting.getRetryTimes() + "次，您的账号已经被锁定，请" + minute + "分钟后再试");
        }
    }

    /**
     * 记录密码错误次数,错误次数达到配置标准后设置账户锁定时间
     *
     * @param memberId
     * @param accountSetting
     */
    private int recordPsswordErrorCount(Long memberId, AccountSetting accountSetting) {
        String errorPwdKey = CachePrefix.PASSWORD_ERROR_NUM_MEMBER.getPrefix() + memberId;
        Object errorNum = cache.get(errorPwdKey);
        int times = 1;
        if (errorNum == null) {
            //首次错误，设置错误计数器
            int exp = CurrencyUtil.mul(accountSetting.getRetryCycle(), 3600).intValue();
            cache.put(errorPwdKey, times, exp);
        } else {
            times = (int) errorNum + 1;
            if (times >= accountSetting.getRetryTimes()) {
                //锁定账号
                lockMember(memberId, CurrencyUtil.mul(accountSetting.getLockDuration(), 60).intValue());
                //删除密码错误次数记录
                cache.remove(errorPwdKey);
                throw new ServiceException("500", "密码错误超过" + accountSetting.getRetryTimes() + "次，您的账号已经被锁定，请" + accountSetting.getLockDuration() + "分钟后再试");
            } else {
                //增加密码错误次数
                cache.put(errorPwdKey, times);
            }
        }
        return times;
    }


    /**
     * 锁定账号
     *
     * @param memberId
     * @param exp      锁定时长/s
     */
    private void lockMember(Long memberId, int exp) {
        long endTime = CurrencyUtil.add(DateUtil.getDateline(), exp).longValue();
        cache.put(CachePrefix.LOCK_MEMBER.getPrefix() + memberId, endTime, exp);

        Member member = memberMapper.selectById(memberId);
        //记录日志
        SystemLogs systemLogs = new SystemLogs();
        systemLogs.setOperateDetail("用户" + member.getUname() + "[" + member.getNickname() + "]登录密码错误过多，被锁定");
        systemLogs.setOperateIp(StringUtil.getIpAddress());
        systemLogs.setParams("lock");
        systemLogs.setMethod("lock");
        systemLogs.setOperateTime(DateUtil.getDateline());
        systemLogs.setOperatorId(memberId);
        systemLogs.setOperatorName("用户" + member.getUname() + "[" + member.getNickname() + "]");
        systemLogs.setSellerId(0L);
        systemLogs.setLevel(LogLevel.important.name());
        systemLogs.setClient("admin");
        systemLogsClient.add(systemLogs);

    }

    @Override
    public Member getMemberByAccount(String account) {
        //通过手机号进行查询账户信息
        Member member = this.getMemberByMobile(account);
        if (member != null) {
            return member;
        }
        //通过用户名进行查询账户信息
        member = this.getMemberByName(account);
        if (member != null) {
            return member;
        }
        //通过邮箱进行查询账户信息
        member = this.getMemberByEmail(account);
        if (member != null) {
            return member;
        }
        throw new ResourceNotFoundException("此会员不存在");
    }

    @Override
    public void logout() {

    }

    @Override
    public Member getMemberByEmail(String email) {

        return memberMapper.getMemberByEmail(email);
    }


    @Override
    public MemberStatisticsDTO getMemberStatistics() {
        MemberStatisticsDTO memberStatisticsDTO = new MemberStatisticsDTO();
        //会员收藏店铺数
        memberStatisticsDTO.setShopCollectCount(memberCollectionShopManager.getMemberCollectCount());
        //会员收藏商品数
        memberStatisticsDTO.setGoodsCollectCount(memberCollectionGoodsManager.getMemberCollectCount());
        //会员订单数
        memberStatisticsDTO.setOrderCount(orderClient.getOrderNumByMemberID(UserContext.getBuyer().getUid()));
        //待评论数
        memberStatisticsDTO.setPendingCommentCount(orderClient.getOrderCommentNumByMemberID(UserContext.getBuyer().getUid(), CommentStatusEnum.UNFINISHED.name()));
        return memberStatisticsDTO;
    }

    @Override
    public List<BackendMemberVO> newMember(Integer length) {

        List<Member> list = new QueryChainWrapper<>(memberMapper)
                .orderByDesc("create_time")
                .last("limit 0," + length)
                .list();
        //构建返回结果
        List<BackendMemberVO> resList = list.stream().map(m -> {
            BackendMemberVO res = new BackendMemberVO();
            BeanUtil.copyProperties(m, res);
            return res;
        }).collect(Collectors.toList());

        return resList;
    }

    @Override
    public List<Member> getMemberByIds(Long[] memberIds) {
        //新建查询条件包装器
        QueryWrapper<Member> wrapper = new QueryWrapper<>();
        //以会员ID为查询条件
        wrapper.in("member_id", memberIds);
        return memberMapper.selectList(wrapper);
    }

    @Override
    public void loginNumToZero() {
        //新建修改条件包装器
        UpdateWrapper<Member> wrapper = new UpdateWrapper<>();
        //设置登录次数为0
        wrapper.set("login_count", 0);
        //所有会员登录次数修改为0
        memberMapper.update(null, wrapper);
    }

    @Override
    public void memberLoginout(Long memberId) {

    }

    @Override
    public List<String> queryAllMemberIds() {
        //查询所有会员id
        List<Member> list = this.memberMapper.selectList(new QueryWrapper<Member>());
        return list.stream().map(m -> m.getMemberId().toString()).collect(Collectors.toList());
    }

    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private ShopTntConfig shoptntConfig;

    /**
     * 生成member的token
     *
     * @param member
     * @param uuid
     * @return
     */
    private MemberVO convertMember(Member member, String uuid) {
        //校验当前账号是否可用
        member.checkDisable();
        //新建买家用户角色对象
        Buyer buyer = new Buyer();
        //设置用户ID
        buyer.setUid(member.getMemberId());
        //设置用户名称
        buyer.setUsername(member.getUname());
        //设置uuid
        buyer.setUuid(uuid);
        //创建Token
        Token token = tokenManager.create(buyer);
        //获取访问Token
        String accessToken = token.getAccessToken();
        //获取刷新Token
        String refreshToken = token.getRefreshToken();
        //组织返回数据
        MemberVO memberVO = new MemberVO(member, accessToken, refreshToken);
        return memberVO;
    }


    /**
     * 发送登录消息
     *
     * @param member
     * @param memberOrSeller
     */
    private void sendMessage(Member member, Integer memberOrSeller) {
        MemberLoginMsg loginMsg = new MemberLoginMsg();
        loginMsg.setLastLoginTime(member.getLastLogin());
        loginMsg.setMemberId(member.getMemberId());
        loginMsg.setMemberOrSeller(memberOrSeller);
        this.messageSender.send(loginMsg);
    }

    /**
     * 获取会员信息
     *
     * @param wrapper 查询条件构造器
     * @return
     */
    private Member getMember(QueryWrapper<Member> wrapper) {
        return memberMapper.selectOne(wrapper);
    }

    /**
     * 生成随机用户名，数字和字母组成
     *
     * @param length
     * @return
     */
    public String getStringRandom(int length) {

        String val = "";
        Random random = new Random();

        //参数length，表示生成几位随机数
        for (int i = 0; i < length; i++) {

            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            //输出字母还是数字
            if ("char".equalsIgnoreCase(charOrNum)) {
                //输出是大写字母还是小写字母
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (random.nextInt(26) + temp);
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    /**
     * 根据会员ID获取会员信息
     * 如果会员是某店铺的店员，则将所属店铺信息放入会员信息中
     *
     * @param memberId 会员ID
     * @return
     */
    @Override
    public Member getMember(Long memberId) {
        //根据会员ID从数据库中获取会员信息
        Member member = this.getModel(memberId);

        //如果会员没有开通店铺，那么需要查询一下是否为某店铺的店员
        if (member.getHaveShop().intValue() == 0) {
            //根据会员ID获取店员信息
            ClerkDO clerkDO = this.clerkManager.getClerkByMemberId(memberId);
            if (clerkDO != null) {
                member.setHaveShop(1);
                member.setShopId(clerkDO.getShopId());
            }
        }
        return member;
    }

    @Override
    public void sleepMember(Long timeStamp) {
        UpdateWrapper<Member> updateWrapper = new UpdateWrapper<>();
        updateWrapper.le("last_login", timeStamp);
        updateWrapper.eq("status", 1);
        updateWrapper.set("status", 0);
        memberMapper.update(null, updateWrapper);
    }

    @Override
    public ScanResult scanModule(ScanModuleDTO scanModuleDTO) {
        QueryWrapper queryWrapper = scanModuleDTO.getQueryWrapper();
        Long pageSize = scanModuleDTO.getPageSize();
        String rounds = scanModuleDTO.getRounds();
        queryWrapper.eq("disabled", 0);
        IPage<Member> memberPage = memberMapper.selectPage(new Page(1, pageSize), queryWrapper);
        List<Member> memberList = memberPage.getRecords();
        ScanResult scanResult = new ScanResult();

        boolean signResult = true;

        for (Member member : memberList) {


            UpdateChainWrapper upw = new UpdateChainWrapper<>(memberMapper)
                    //设置会员状态
                    .set("scan_rounds", rounds);
            upw.set("sign_result", 1);

            upw.eq("member_id", member.getMemberId());
            upw.update();

        }

        scanResult.setSuccess(signResult);
        scanResult.setComplete(memberList.size() == 0);
        return scanResult;
    }

    @Override
    public void reSign() {
        List<Member> memberList = memberMapper.selectList(null);
        for (Member member : memberList) {
            memberMapper.updateById(member);
        }
    }

    @Override
    public void repair(Long memberId) {
        Member member = memberMapper.selectById(memberId);
        member.setDisabled(0);
        memberMapper.updateById(member);
        //发送解禁消息
        this.recoverySendMsg(member.getMemberId());
    }

    @Override
    public Boolean getLockStatus(Long memberId) {
        Object lockFlag = cache.get(CachePrefix.LOCK_MEMBER.getPrefix() + memberId);
        if (lockFlag != null) {
            return true;
        }
        return false;
    }

    @Override
    public void unLock(Long memberId) {
        cache.remove(CachePrefix.LOCK_MEMBER.getPrefix() + memberId);
    }

    @Override
    public void logOff(String smsCode) {
        Buyer buyer = UserContext.getBuyer();
        Member member = getMember(buyer.getUid());
        boolean isPass = smsClient.valid(SceneType.LOG_OFF.name(), member.getMobile(), smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码不正确");
        }
        //为保证之前此会员的数据 订单 售后 无法溯源 将注销的会员保存在另一张表
        LogOffMember logOffMember = new LogOffMember();
        BeanUtil.copyProperties(member, logOffMember);
        logOffMember.setMemberId(member.getMemberId());
        logOffMemberManager.save(logOffMember);
        //删除此会员
        memberMapper.deleteById(member);
    }
}
