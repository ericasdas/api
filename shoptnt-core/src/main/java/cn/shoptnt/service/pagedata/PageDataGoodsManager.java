/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.pagedata;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.shoptnt.model.pagedata.PageDataGoods;

/**
 * 楼层商品中间
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2021年12月10日17:30:15
 */
public interface PageDataGoodsManager extends IService<PageDataGoods> {

}
