/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.distribution;

/**
 * WithdrawCountManager
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-15 上午8:43
 */
public interface WithdrawCountManager {

    /**
     * 整理解冻金额
     */
    void withdrawCount();
}
