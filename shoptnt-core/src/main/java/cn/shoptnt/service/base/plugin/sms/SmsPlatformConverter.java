/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.plugin.sms;

import cn.shoptnt.model.system.vo.SmsPlatformVO;

/**
 * 短信网关vo转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.3.0
 * 2020/4/2
 */

public class SmsPlatformConverter {

    /**
     * 通过sms platofrm插件转换成vo
     * @param smsPlatform
     * @return
     */
    public static SmsPlatformVO toSmsPlatformVO(SmsPlatform smsPlatform) {
        SmsPlatformVO smsPlatformVO = new SmsPlatformVO();
        smsPlatformVO.setName(smsPlatform.getPluginName());
        smsPlatformVO.setOpen(smsPlatform.getIsOpen());
        smsPlatformVO.setBean(smsPlatform.getPluginId());
        smsPlatformVO.setConfigItems( smsPlatform.definitionConfigItem());
        return smsPlatformVO;
    }
}
