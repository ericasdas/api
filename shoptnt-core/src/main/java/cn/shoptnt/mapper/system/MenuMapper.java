/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.system.dos.Menu;
import cn.shoptnt.model.system.vo.MenusVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 菜单管理店铺的Mapper
 * @author zhanghao
 * @version v1.0
 * @since v7.2.2
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据id查询菜单
     * @param id 菜单管理主键
     * @return Map
     */
    List<Map> queryForId(@Param("id")Long id);

    /**
     * 获取菜单集合
     * @return MenusVO
     */
    List<MenusVO> queryForVo();
}
