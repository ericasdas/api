package cn.shoptnt.mapper.system;

import cn.shoptnt.framework.trigger.Interface.TimerTaskData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 延迟任务mapper
 * @author kingapex
 * @version 1.0
 * @data 2022/11/4 16:17
 **/

public interface TimerTaskMapper extends BaseMapper<TimerTaskData> {
}
