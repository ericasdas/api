/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.payment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.payment.dos.PaymentBillDO;

/**
 * 支付帐单的Mapper
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/29
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface PaymentBillMapper extends BaseMapper<PaymentBillDO> {
}
