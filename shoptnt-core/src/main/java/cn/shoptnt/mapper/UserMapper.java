/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/7/1
 */
public interface UserMapper extends BaseMapper<User> {
    List<User> customerSqlSegment(@Param("ew") Wrapper ew);
}
