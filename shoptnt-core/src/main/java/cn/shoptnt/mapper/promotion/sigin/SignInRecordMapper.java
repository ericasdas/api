package cn.shoptnt.mapper.promotion.sigin;

import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecordMapper
 * @description 签到记录mapper
 * @program: api
 * 2024/3/12 12:07
 */
public interface SignInRecordMapper extends BaseMapper<SignInRecord> {
}
