/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.client.trade.AfterSaleClient;
import cn.shoptnt.job.EveryHourExecute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时任务查询退款状态并且修改退款状态
 *
 * @author zh
 * @version v1.0
 * @since v7.1.5
 * 2020-02-03
 */
@Component
public class RefundStatusJobConsumer implements EveryHourExecute {

    @Autowired
    private AfterSaleClient afterSaleClient;

    @Override
    public void everyHour() {
        afterSaleClient.refundCompletion();
    }
}
