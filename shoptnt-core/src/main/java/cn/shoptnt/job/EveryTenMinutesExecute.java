/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job;

/**
 * @version v1.0
 * @Description: 每十分钟执行定时任务
 * @Author: gy
 * @Date: 2020/6/10 0010 10:02
 */
public interface EveryTenMinutesExecute {

    /**
     * 每十分钟执行定时任务
     */
    void everyTenMinutes();
}
