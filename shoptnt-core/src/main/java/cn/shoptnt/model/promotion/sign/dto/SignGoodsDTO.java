package cn.shoptnt.model.promotion.sign.dto;

import cn.shoptnt.model.goods.dos.GoodsDO;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignGoodsDTO
 * @description 签到推荐商品
 * @program: api
 * 2024/3/13 14:45
 */
public class SignGoodsDTO implements Serializable {
    private static final long serialVersionUID = -7192755289103448791L;

    @Schema(description =  "商品id")
    private Long goodsId;
    @Schema(description =  "商品名称")
    private String goodsName;
    @Schema(description =  "商品图片")
    private Double goodsPrice;
    @Schema(description = "商品图片")
    private String goodsImg;

    public SignGoodsDTO() {

    }

    public SignGoodsDTO(GoodsDO goodsDO) {
        this.goodsId = goodsDO.getGoodsId();
        this.goodsName = goodsDO.getGoodsName();
        this.goodsPrice = goodsDO.getPrice();
        this.goodsImg = goodsDO.getThumbnail();
    }


    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    @Override
    public String toString() {
        return "SignGoodsDTO{" +
                "goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsPrice=" + goodsPrice +
                '}';
    }
}
