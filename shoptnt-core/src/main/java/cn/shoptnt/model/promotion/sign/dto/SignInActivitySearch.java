package cn.shoptnt.model.promotion.sign.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecordSearchVO
 * @description 签到活动查询对象
 * @program: api
 * 2024/3/12 14:24
 */
public class SignInActivitySearch implements Serializable {
    private static final long serialVersionUID = 1777456563089971570L;

    @Schema(name = "start_time", description =  "开始时间")
    private Long startTime;

    @Schema(name = "end_time", description =  "结束时间")
    private Long endTime;

    @Schema(name = "title", description =  "活动标题")
    private String title;
    @Schema(name = "status", description =  "活动状态")
    private String status;


    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SignInActiveSearch{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", title='" + title + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
