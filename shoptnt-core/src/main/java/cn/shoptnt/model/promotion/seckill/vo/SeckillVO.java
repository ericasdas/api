/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.seckill.vo;

import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Snow
 * @version 1.0
 * @since 6.4.1
 * 2017年12月14日 16:58:55
 */
@Schema(description = "限时抢购活动vo")
public class SeckillVO extends SeckillDO {

	@Schema(name = "range_list",description =  "活动时刻表")
	@Size(min=1, max=23)
	private List<Integer> rangeList;

	@Schema(name = "0:未报名,1:已报名,2:已截止" )
	private Integer isApply;

	@Schema(name = "seckill_status_text",description = "状态值")
	private String seckillStatusText;


	public SeckillVO(){

	}

	public List<Integer> getRangeList() {
		return rangeList;
	}

	public void setRangeList(List<Integer> rangeList) {
		this.rangeList = rangeList;
	}

	public Integer getIsApply() {
		return isApply;
	}

	public void setIsApply(Integer isApply) {
		this.isApply = isApply;
	}

	public String getSeckillStatusText() {
		return seckillStatusText;
	}

	public void setSeckillStatusText(String seckillStatusText) {
		this.seckillStatusText = seckillStatusText;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o){
			return true;
		}

		if (o == null || getClass() != o.getClass()){
			return false;
		}

		SeckillVO seckillVO = (SeckillVO) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.append(rangeList, seckillVO.rangeList)
				.append(isApply, seckillVO.isApply)
				.append(seckillStatusText, seckillVO.seckillStatusText)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.appendSuper(super.hashCode())
				.append(rangeList)
				.append(isApply)
				.append(seckillStatusText)
				.toHashCode();
	}

	@Override
	public String toString() {
		return "SeckillVO{" +
				"rangeList=" + rangeList +
				", isApply=" + isApply +
				", seckillStatusText='" + seckillStatusText + '\'' +
				'}';
	}
}
