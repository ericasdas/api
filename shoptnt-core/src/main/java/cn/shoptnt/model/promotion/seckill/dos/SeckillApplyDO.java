/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.seckill.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 限时抢购申请实体
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 17:30:09
 */
@TableName("es_seckill_apply")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SeckillApplyDO implements Serializable {

    private static final long serialVersionUID = 2980175459354215L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long applyId;

    /**
     * 活动id
     */
    @Column(name = "seckill_id")
    @Schema(name = "seckill_id", description =  "活动id", required = true)
    private Long seckillId;

    /**
     * 时刻
     */
    @Column(name = "time_line")
    @Schema(name = "time_line", description =  "时刻")
    private Integer timeLine;

    /**
     * 活动开始日期
     */
    @Column(name = "start_day")
    @Schema(name = "start_day", description =  "活动开始日期")
    private Long startDay;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    @Schema(name = "goods_id", description = "商品ID")
    private Long goodsId;

    /**
     * skuID
     */
    @Column(name = "sku_id")
    @Schema(name = "sku_id", description = "skuID")
    private Long skuId;

    /**
     * 规格组合
     */
    @Column(name = "specs")
    @Schema(name = "specs", description = "规格组合")
    @JsonRawValue
    private String specs;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    @Schema(name = "goods_name", description = "商品名称")
    private String goodsName;

    /**
     * 商家ID
     */
    @Column(name = "seller_id")
    @Schema(name = "seller_id", description = "商家id")
    private Long sellerId;

    /**
     * 商家名称
     */
    @Column(name = "shop_name")
    @Schema(name = "shop_name", description = "商家名称")
    private String shopName;

    /**
     * 价格
     */
    @Column(name = "price")
    @Schema(name = "price", description = "价格")
    private Double price;

    /**
     * 售空数量
     */
    @Column(name = "sold_quantity")
    @Schema(name = "sold_quantity", description = "售空数量")
    private Integer soldQuantity;

    /**
     * 申请状态
     */
    @Column(name = "status")
    @Schema(name = "status", description = "申请状态,APPLY:申请中,PASS:已通过,FAIL:已驳回")
    private String status;

    /**
     * 驳回原因
     */
    @Column(name = "fail_reason")
    @Schema(name = "fail_reason", description = "驳回原因")
    private String failReason;

    @Column(name = "sales_num")
    @Schema(name = "sales_num", description = "已售数量")
    private Integer salesNum;

    @Column(name = "original_price")
    @Schema(name = "original_price", description = "商品原始价格")
    private Double originalPrice;


    @PrimaryKeyField
    public Long getApplyId() {
        return applyId;
    }

    public void setApplyId(Long applyId) {
        this.applyId = applyId;
    }

    public Long getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(Long seckillId) {
        this.seckillId = seckillId;
    }

    public Integer getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(Integer timeLine) {
        this.timeLine = timeLine;
    }

    public Long getStartDay() {
        return startDay;
    }

    public void setStartDay(Long startDay) {
        this.startDay = startDay;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFailReason() {
        return failReason;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getSalesNum() {
        if (salesNum == null) {
            salesNum = 0;
        }
        return salesNum;
    }

    public void setSalesNum(Integer salesNum) {
        this.salesNum = salesNum;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SeckillApplyDO applyDO = (SeckillApplyDO) o;

        return new EqualsBuilder()
                .append(applyId, applyDO.applyId)
                .append(seckillId, applyDO.seckillId)
                .append(timeLine, applyDO.timeLine)
                .append(startDay, applyDO.startDay)
                .append(goodsId, applyDO.goodsId)
                .append(goodsName, applyDO.goodsName)
                .append(skuId, applyDO.skuId)
                .append(sellerId, applyDO.sellerId)
                .append(shopName, applyDO.shopName)
                .append(price, applyDO.price)
                .append(soldQuantity, applyDO.soldQuantity)
                .append(status, applyDO.status)
                .append(failReason, applyDO.failReason)
                .append(salesNum, applyDO.salesNum)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(applyId)
                .append(seckillId)
                .append(timeLine)
                .append(startDay)
                .append(goodsId)
                .append(goodsName)
                .append(skuId)
                .append(sellerId)
                .append(shopName)
                .append(price)
                .append(soldQuantity)
                .append(status)
                .append(failReason)
                .append(salesNum)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "SeckillApplyDO{" +
                "applyId=" + applyId +
                ", seckillId=" + seckillId +
                ", timeLine=" + timeLine +
                ", startDay=" + startDay +
                ", goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", skuId=" + skuId +
                ", sellerId=" + sellerId +
                ", shopName='" + shopName + '\'' +
                ", price=" + price +
                ", soldQuantity=" + soldQuantity +
                ", status='" + status + '\'' +
                ", failReason='" + failReason + '\'' +
                ", salesNum=" + salesNum +
                '}';
    }
}
