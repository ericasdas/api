package cn.shoptnt.model.promotion.sign.dos;

import cn.shoptnt.framework.database.annotation.Column;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignInReward
 * @description 签到奖品
 * @program: api
 * 2024/3/12 11:20
 */
@TableName("es_sign_in_reward")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SignInReward implements Serializable {
    private static final long serialVersionUID = -877608164499809506L;

    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    @Schema(description =  "活动id", required = true, hidden = true)
    @Column(name = "activity_id")
    private Long activityId;

    @Schema(description =  "积分")
    @Column(name = "points")
    private int points;

    @Schema(description =  "优惠券id")
    @Column(name = "coupon_id")
    private Long couponId;

    @Schema(description =  "描述", required = true)
    @Column(name = "description")
    private String description;
    @Schema(description =  "图标", required = true)
    @Column(name = "icon")
    private String icon;
    @Schema(description =  "是否开启赠送积分", required = true)
    @Column(name = "point_switch")
    private String pointSwitch;

    @Schema(description =  "是否开启赠送优惠券", required = true)
    @Column(name = "coupon_switch")
    private String couponSwitch;
    @Schema(description =  "签到天数", required = true)
    @Column(name = "sign_day")
    private int signDay;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPointSwitch() {
        return pointSwitch;
    }

    public void setPointSwitch(String pointSwitch) {
        this.pointSwitch = pointSwitch;
    }

    public String getCouponSwitch() {
        return couponSwitch;
    }

    public void setCouponSwitch(String couponSwitch) {
        this.couponSwitch = couponSwitch;
    }

    public int getSignDay() {
        return signDay;
    }

    public void setSignDay(int signDay) {
        this.signDay = signDay;
    }


    @Override
    public String toString() {
        return "SignInReward{" +
                "id=" + id +
                ", activityId=" + activityId +
                ", points=" + points +
                ", couponId=" + couponId +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", pointSwitch='" + pointSwitch + '\'' +
                ", couponSwitch='" + couponSwitch + '\'' +
                ", signDay=" + signDay +
                '}';
    }
}
