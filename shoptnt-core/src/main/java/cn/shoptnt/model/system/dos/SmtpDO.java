/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;


/**
 * 邮件实体
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-25 16:16:53
 */
@TableName("es_smtp")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SmtpDO implements Serializable {

	private static final long serialVersionUID = 9787156257241506L;

	/**主键ID*/
	@TableId(type= IdType.ASSIGN_ID)
	@Schema(hidden=true)
	private Long id;
	/**主机*/
	@Schema(name="host",description = "主机")
	private String host;
	/**用户名*/
	@NotEmpty(message="用户名不能为空")
	@Schema(name="username",description = "用户名",required=true)
	private String username;
	/**密码*/
	@Schema(name="password",description = "密码")
	private String password;
	/**最后发信时间*/
	@Schema(name="last_send_time",description = "最后发信时间",hidden=true)
	private Long lastSendTime;
	/**已发数*/
	@Min(message="必须为数字", value = 0)
	@Schema(name="send_count",description = "已发数",hidden=true)
	private Integer sendCount;
	/**最大发信数*/
	@Schema(name="max_count",description = "最大发信数")
	private Integer maxCount;
	/**发信邮箱*/
	@NotEmpty(message="邮箱不能为空")
	@Email(message="邮箱格式不正确")
	@Schema(name="mail_from",description = "发信邮箱")
	private String mailFrom;
	/**端口*/
	@Min(message="必须为数字", value = 0)
	@Schema(name="port",description = "端口")
	private Integer port;
	/**ssl是否开启*/
	@Min(message="必须为数字", value = 0)
	@Schema(name="open_ssl",description = "ssl是否开启")
	private Integer openSsl;




	@Override
	public String toString() {
		return "SmtpDO [id=" + id + ", host=" + host + ", username=" + username + ", password=" + password
				+ ", lastSendTime=" + lastSendTime + ", sendCount=" + sendCount + ", maxCount=" + maxCount
				+ ", mailFrom=" + mailFrom + ", port=" + port + ", openSsl=" + openSsl + "]";
	}
	@PrimaryKeyField
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Long getLastSendTime() {
		return lastSendTime;
	}
	public void setLastSendTime(Long lastSendTime) {
		this.lastSendTime = lastSendTime;
	}

	public Integer getSendCount() {
		return sendCount;
	}
	public void setSendCount(Integer sendCount) {
		this.sendCount = sendCount;
	}

	public Integer getMaxCount() {
		return maxCount;
	}
	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getOpenSsl() {
		return openSsl;
	}
	public void setOpenSsl(Integer openSsl) {
		this.openSsl = openSsl;
	}



}
