/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 快递鸟表单
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2019-05-21 上午11:17
 */
public class FormItem {

    @Schema(name = "name", description = "表单名称")
    private String name;


    @Schema(name = "code", description = "快递鸟 参数")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
