/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dos;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.model.system.dto.FormItem;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * 物流公司实体
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-29 15:10:38
 */
@TableName("es_logistics_company")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LogisticsCompanyDO implements Serializable {

    private static final long serialVersionUID = 2885097420270994L;


    @TableId(type= IdType.ASSIGN_ID)
    @Schema(name = "id", description = "物流公司id")
    private Long id;

    @NotEmpty(message = "物流公司名称必填")
    @Schema(name = "name", description = "物流公司名称", required = true)
    private String name;

    @NotEmpty(message = "物流公司code必填")
    @Schema(name = "code", description = "物流公司code", required = true)
    private String code;

    @NotNull(message = "是否支持电子面单必填 1：支持 0：不支持")
    @Schema(name = "is_waybill", description = "是否支持电子面单1：支持 0：不支持", required = true)
    private Integer isWaybill;

    @Schema(name = "kdcode", description = "快递鸟物流公司code", required = true)
    private String kdcode;

    @Schema(name = "form_items", description = "物流公司电子面单表单", hidden = true)
    private String formItems;

    /**是否删除 DELETED：已删除，NORMAL：正常*/
    @Schema(name="delete_status",description = "是否删除 DELETED：已删除，NORMAL：正常")
    private String deleteStatus;

    /**禁用状态 OPEN：开启，CLOSE：禁用*/
    @Schema(name="disabled",description = "禁用状态 OPEN：开启，CLOSE：禁用")
    private String disabled;

    public LogisticsCompanyDO() {
        super();
    }

    public LogisticsCompanyDO(String name, String code, String kdcode, Integer isWaybill) {
        super();
        this.name = name;
        this.code = code;
        this.kdcode = kdcode;
        this.isWaybill = isWaybill;
    }

    public void setForm(List<FormItem> form) {
        if (form != null||form.size()==0) {
            this.setFormItems(JsonUtil.objectToJson(form));
        }
    }

    public List<FormItem> getForm() {
        if (!StringUtil.isEmpty(this.formItems)) {
            return JsonUtil.jsonToList(formItems, FormItem.class);
        }
        return null;
    }

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKdcode() {
        return kdcode;
    }

    public Integer getIsWaybill() {
        return isWaybill;
    }

    public void setKdcode(String kdcode) {
        this.kdcode = kdcode;
    }

    public String getFormItems() {
        return formItems;
    }

    public void setFormItems(String formItems) {
        this.formItems = formItems;
    }

    public void setIsWaybill(Integer isWaybill) {
        this.isWaybill = isWaybill;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        return "LogisticsCompanyDO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", isWaybill=" + isWaybill +
                ", kdcode='" + kdcode + '\'' +
                ", formItems='" + formItems + '\'' +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", disabled='" + disabled + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LogisticsCompanyDO that = (LogisticsCompanyDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(code, that.code) &&
                Objects.equals(isWaybill, that.isWaybill) &&
                Objects.equals(kdcode, that.kdcode) &&
                Objects.equals(formItems, that.formItems) &&
                Objects.equals(deleteStatus, that.deleteStatus) &&
                Objects.equals(disabled, that.disabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, isWaybill, kdcode, formItems, deleteStatus, disabled);
    }
}
