/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 结算单项表实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-04-26 15:39:57
 */
@TableName("es_bill_item")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillItem implements Serializable {

    private static final long serialVersionUID = 8456961440202335L;

    /**
     * 主键id
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号")
    private String orderSn;
    /**
     * 订单价格
     */
    @Schema(name = "price", description = "金额，退款金额或者收款金额")
    private Double price;
    /**
     * 优惠价格
     */
    @Schema(name = "discount_price", description = "优惠价格")
    private Double discountPrice;
    /**
     * 单项类型 收款/退款
     */
    @Schema(name = "item_type", description = "单项类型 收款/退款")
    private String itemType;
    /**
     * 加入时间
     */
    @Schema(name = "add_time", description = "加入时间")
    private Long addTime;
    /**
     * 所属账单id
     */
    @Schema(name = "bill_id", description = "所属账单id")
    private Long billId;
    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
    /**
     * 店铺id
     */
    @Schema(name = "seller_id", description = "店铺id")
    private Long sellerId;
    /**
     * 下单时间
     */
    @Schema(name = "order_time", description = "下单时间")
    private Long orderTime;
    /**
     * 退款单号
     */
    @Schema(name = "refund_sn", description = "退款单号")
    private String refundSn;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description = "会员id")
    private Long memberId;
    /**
     * 会员名称
     */
    @Schema(name = "member_name", description = "会员名称")
    private String memberName;
    /**
     * 收货人
     */
    @Schema(name = "ship_name", description = "收货人")
    private String shipName;
    /**
     * 支付方式
     */
    @Schema(name = "payment_type", description = "支付方式")
    private String paymentType;
    /**
     * 退货时间
     */
    @Schema(name = "refund_time", description = "退货时间")
    private Long refundTime;

    /**
     * 站点优惠券金额
     */
    @Schema(name = "site_coupon_price", description = "站点优惠券金额")
    private Double siteCouponPrice;

    /**
     * 站点优惠券佣金比例
     */
    @Schema(name = "coupon_commission", description = "站点优惠券佣金比例")
    private Double couponCommission;


    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public String getRefundSn() {
        return refundSn;
    }

    public void setRefundSn(String refundSn) {
        this.refundSn = refundSn;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Long refundTime) {
        this.refundTime = refundTime;
    }

    public Double getSiteCouponPrice() {
        return siteCouponPrice;
    }

    public void setSiteCouponPrice(Double siteCouponPrice) {
        this.siteCouponPrice = siteCouponPrice;
    }

    public Double getCouponCommission() {
        return couponCommission;
    }

    public void setCouponCommission(Double couponCommission) {
        this.couponCommission = couponCommission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillItem that = (BillItem) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (orderSn != null ? !orderSn.equals(that.orderSn) : that.orderSn != null) {
            return false;
        }
        if (price != null ? !price.equals(that.price) : that.price != null) {
            return false;
        }
        if (discountPrice != null ? !discountPrice.equals(that.discountPrice) : that.discountPrice != null) {
            return false;
        }
        if (itemType != null ? !itemType.equals(that.itemType) : that.itemType != null) {
            return false;
        }
        if (addTime != null ? !addTime.equals(that.addTime) : that.addTime != null) {
            return false;
        }
        if (billId != null ? !billId.equals(that.billId) : that.billId != null) {
            return false;
        }
        if (status != null ? !status.equals(that.status) : that.status != null) {
            return false;
        }
        if (sellerId != null ? !sellerId.equals(that.sellerId) : that.sellerId != null) {
            return false;
        }
        if (orderTime != null ? !orderTime.equals(that.orderTime) : that.orderTime != null) {
            return false;
        }
        if (refundSn != null ? !refundSn.equals(that.refundSn) : that.refundSn != null) {
            return false;
        }
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) {
            return false;
        }
        if (memberName != null ? !memberName.equals(that.memberName) : that.memberName != null) {
            return false;
        }
        if (shipName != null ? !shipName.equals(that.shipName) : that.shipName != null) {
            return false;
        }
        if (paymentType != null ? !paymentType.equals(that.paymentType) : that.paymentType != null) {
            return false;
        }
        return refundTime != null ? refundTime.equals(that.refundTime) : that.refundTime == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (orderSn != null ? orderSn.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (discountPrice != null ? discountPrice.hashCode() : 0);
        result = 31 * result + (itemType != null ? itemType.hashCode() : 0);
        result = 31 * result + (addTime != null ? addTime.hashCode() : 0);
        result = 31 * result + (billId != null ? billId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (sellerId != null ? sellerId.hashCode() : 0);
        result = 31 * result + (orderTime != null ? orderTime.hashCode() : 0);
        result = 31 * result + (refundSn != null ? refundSn.hashCode() : 0);
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        result = 31 * result + (memberName != null ? memberName.hashCode() : 0);
        result = 31 * result + (shipName != null ? shipName.hashCode() : 0);
        result = 31 * result + (paymentType != null ? paymentType.hashCode() : 0);
        result = 31 * result + (refundTime != null ? refundTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BillItem{" +
                "id=" + id +
                ", orderSn='" + orderSn + '\'' +
                ", orderPrice=" + price +
                ", discountPrice=" + discountPrice +
                ", itemType='" + itemType + '\'' +
                ", addTime=" + addTime +
                ", billId=" + billId +
                ", status=" + status +
                ", sellerId=" + sellerId +
                ", orderTime=" + orderTime +
                ", refundSn='" + refundSn + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", shipName='" + shipName + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", refundTime=" + refundTime +
                '}';
    }


}
