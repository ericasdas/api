/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.member.dos.MemberAsk;

import java.io.Serializable;
import java.util.List;

/**
 * 会员商品咨询消息
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
public class MemberAskMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -6950240368106665641L;

    private List<MemberAsk> memberAsks;

    private Long sendTime;

    public List<MemberAsk> getMemberAsks() {
        return memberAsks;
    }

    public void setMemberAsks(List<MemberAsk> memberAsks) {
        this.memberAsks = memberAsks;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_GOODS_ASK;
    }
}
