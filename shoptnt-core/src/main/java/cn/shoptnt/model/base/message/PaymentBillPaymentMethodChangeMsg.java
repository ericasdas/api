/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;
import java.util.Map;


/**
 * 支付账单支付方式变化消息
 *
 * @author zh
 * @version v2.0
 * @since v7.2.1
 * 2020年3月11日 上午9:52:13
 */
public class PaymentBillPaymentMethodChangeMsg implements Serializable, DirectMessage {


    private static final long serialVersionUID = 6863126682040261428L;
    /**
     * 账单信息
     * map中的key为 支付账单支付方式变化类型
     * map中的value为 paymentBill
     *
     * @see cn.shoptnt.model.payment.enums.PaymentBillChangeTypeEnum
     */
    private Map<String, Object> bills;

    public Map<String, Object> getBills() {
        return bills;
    }

    public void setBills(Map<String, Object> bills) {
        this.bills = bills;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.PAYMENT_BILL_PAYMENT_METHOD_CHANGE;
    }
}
