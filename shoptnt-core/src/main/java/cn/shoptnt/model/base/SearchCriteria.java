/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base;

import cn.shoptnt.model.statistics.exception.StatisticsException;
import cn.shoptnt.model.statistics.enums.QueryDateType;
import cn.shoptnt.framework.util.StringUtil;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 搜索参数
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/4/28 下午5:09
 */
@Schema
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = -1570682583055253820L;

    @Schema(name = "cycle_type", description = "周期 YEAR:年 MONTH:月", required = true)
    private String cycleType;

    @Schema(name = "year",description = "年份", example = "2016")
    private Integer year;

    @Schema(name = "month",description = "月份", example = "11")
    private Integer month;

    @Schema(name = "seller_id", description = "店铺id 管理员可用0查询全平台", example = "0")
    private Long sellerId;

    @Schema(name = "category_id",description = "分类id 0全部", example = "0")
    private Long categoryId;


    /**
     * @param cycleType 日期类型
     * @param year      年
     * @param month     月
     */
    public static void checkDataParams(String cycleType, Integer year, Integer month) throws StatisticsException {
        if (cycleType == null || year == null) {
            throw new StatisticsException("日期类型及年份不可为空");
        }
        if (cycleType.equals(QueryDateType.MONTH.value()) && month == null) {
            throw new StatisticsException("按月查询时，月份不可为空");
        }
    }

    /**
     * 校验参数
     *
     * @param searchCriteria 参数对象
     * @param checkDate      校验日期
     * @param checkCategory  校验分类
     * @param checkSeller    校验商家
     * @throws StatisticsException 抛出自定义统计异常
     */
    public static void checkDataParams(SearchCriteria searchCriteria, boolean checkDate, boolean checkCategory, boolean checkSeller) throws StatisticsException {

        if (checkDate) {
            if (searchCriteria.getCycleType() == null || searchCriteria.getYear() == null) {
                throw new StatisticsException("日期类型及年份不可为空");
            }
            if (searchCriteria.getCycleType().equals(QueryDateType.MONTH.value()) && searchCriteria.getMonth() == null) {
                throw new StatisticsException("按月查询时，月份不可为空");
            }
        }
        if (checkCategory) {
            if (searchCriteria.getCategoryId() == null) {
                throw new StatisticsException("商品分类不可为空");
            }
        }
        if (checkSeller) {
            if (searchCriteria.getSellerId() == null) {
                throw new StatisticsException("必须选择一个店铺");
            }
        }
    }

    public SearchCriteria() {
    }

    public static Integer[] defaultPrice() {
        Integer[] prices = new Integer[5];
        prices[0] = 0;
        prices[1] = 100;
        prices[2] = 1000;
        prices[3] = 10000;
        prices[4] = 100000;
        return prices;
    }


    public SearchCriteria(String circle, Integer year, Integer month) {

        if (StringUtil.isEmpty(circle)) {
            this.setCycleType(QueryDateType.YEAR.name());
        }

        if (year == null) {
            LocalDate localDate = LocalDate.now();
            this.setYear(localDate.getYear());
        }

        if (month == null) {
            LocalDate localDate = LocalDate.now();
            this.setMonth(localDate.getMonthValue());
        }

        this.setSellerId(0L);
        this.setCategoryId(0L);
    }


    public SearchCriteria(SearchCriteria searchCriteria) {
        if (StringUtil.isEmpty(searchCriteria.getCycleType())) {
            searchCriteria.setCycleType(QueryDateType.YEAR.name());
        }
        if (searchCriteria.getYear() == null) {
            LocalDate localDate = LocalDate.now();
            searchCriteria.setYear(localDate.getYear());
        }
        if (searchCriteria.getMonth() == null) {
            LocalDate localDate = LocalDate.now();
            searchCriteria.setMonth(localDate.getMonthValue());
        }
        if (searchCriteria.getSellerId() == null) {
            searchCriteria.setSellerId(0L);
        }
        if (searchCriteria.getCategoryId() == null) {
            searchCriteria.setCategoryId(0L);
        }

        this.setCategoryId(searchCriteria.getCategoryId());
        this.setSellerId(searchCriteria.getSellerId());
        this.setYear(searchCriteria.getYear());
        this.setMonth(searchCriteria.getMonth());
        this.setCycleType(searchCriteria.getCycleType());

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCycleType() {
        return cycleType;
    }

    public void setCycleType(String cycleType) {
        this.cycleType = cycleType;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
