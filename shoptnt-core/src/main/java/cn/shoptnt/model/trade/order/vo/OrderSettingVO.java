/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 订单设置VO
 * @author Snow create in 2018/7/13
 * @version v2.0
 * @since v7.0.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderSettingVO implements Serializable {

    @Schema(name = "cancel_order_day", description = "自动取消订单天数",required = true)
    @NotNull(message="自动取消订单天数不能为空")
    private Integer cancelOrderDay;


    @Schema(name = "cancel_order_hour", description = "自动取消订单小时数",required = true)
    @NotNull(message="自动取消订单小时数不能为空")
    private Integer cancelOrderHour;

    @Schema(name = "cancel_order_minutes", description = "自动取消订单分钟数",required = true)
    @NotNull(message="自动取消订单分钟数不能为空")
    private Integer cancelOrderMinutes;

    @Schema(name = "rog_order_day", description = "自动确认收货天数",required = true)
    @NotNull(message="自动确认收货天数不能为空")
    private Integer rogOrderDay;

    @Schema(name = "comment_order_day", description = "评价超时天数",required = true)
    @NotNull(message="评价超时天数不能为空")
    private Integer commentOrderDay;

    @Schema(name = "service_expired_day", description = "售后失效天数",required = true)
    @NotNull(message="售后失效天数不能为空")
    private Integer serviceExpiredDay;

    @Schema(name = "complete_order_day", description = "订单完成天数",required = true)
    @NotNull(message="订单完成天数不能为空")
    private Integer completeOrderDay;

    @Schema(name = "complete_order_pay", description = "自动支付天数,仅对货到付款的订单有效",required = true)
    @NotNull(message="自动支付天数不能为空")
    private Integer completeOrderPay;

    @Schema(name = "complain_expired_day", description = "交易投诉失效天数",required = true)
    @NotNull(message="交易投诉失效天数不能为空")
    private Integer complainExpiredDay;

    public Integer getCancelOrderDay() {
        return cancelOrderDay;
    }

    public void setCancelOrderDay(Integer cancelOrderDay) {
        this.cancelOrderDay = cancelOrderDay;
    }

    public Integer getCancelOrderHour() {
        return cancelOrderHour;
    }

    public void setCancelOrderHour(Integer cancelOrderHour) {
        this.cancelOrderHour = cancelOrderHour;
    }

    public Integer getCancelOrderMinutes() {
        return cancelOrderMinutes;
    }

    public void setCancelOrderMinutes(Integer cancelOrderMinutes) {
        this.cancelOrderMinutes = cancelOrderMinutes;
    }

    public Integer getRogOrderDay() {
        return rogOrderDay;
    }

    public void setRogOrderDay(Integer rogOrderDay) {
        this.rogOrderDay = rogOrderDay;
    }

    public Integer getCommentOrderDay() {
        return commentOrderDay;
    }

    public void setCommentOrderDay(Integer commentOrderDay) {
        this.commentOrderDay = commentOrderDay;
    }

    public Integer getServiceExpiredDay() {
        return serviceExpiredDay;
    }

    public void setServiceExpiredDay(Integer serviceExpiredDay) {
        this.serviceExpiredDay = serviceExpiredDay;
    }

    public Integer getCompleteOrderDay() {
        return completeOrderDay;
    }

    public void setCompleteOrderDay(Integer completeOrderDay) {
        this.completeOrderDay = completeOrderDay;
    }

    public Integer getCompleteOrderPay() {
        return completeOrderPay;
    }

    public void setCompleteOrderPay(Integer completeOrderPay) {
        this.completeOrderPay = completeOrderPay;
    }

    public Integer getComplainExpiredDay() {
        return complainExpiredDay;
    }

    public void setComplainExpiredDay(Integer complainExpiredDay) {
        this.complainExpiredDay = complainExpiredDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        OrderSettingVO that = (OrderSettingVO) o;

        return new EqualsBuilder()
                .append(cancelOrderDay, that.cancelOrderDay)
                .append(cancelOrderHour, that.cancelOrderHour)
                .append(cancelOrderMinutes, that.cancelOrderMinutes)
                .append(rogOrderDay, that.rogOrderDay)
                .append(commentOrderDay, that.commentOrderDay)
                .append(serviceExpiredDay, that.serviceExpiredDay)
                .append(completeOrderDay, that.completeOrderDay)
                .append(completeOrderPay, that.completeOrderPay)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(cancelOrderDay)
                .append(cancelOrderHour)
                .append(cancelOrderMinutes)
                .append(rogOrderDay)
                .append(commentOrderDay)
                .append(serviceExpiredDay)
                .append(completeOrderDay)
                .append(completeOrderPay)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "OrderSettingVO{" +
                "cancelOrderDay=" + cancelOrderDay +
                ", cancelOrderHour=" + cancelOrderHour +
                ", cancelOrderMinutes=" + cancelOrderMinutes +
                ", rogOrderDay=" + rogOrderDay +
                ", commentOrderDay=" + commentOrderDay +
                ", serviceExpiredDay=" + serviceExpiredDay +
                ", completeOrderDay=" + completeOrderDay +
                ", completeOrderPay=" + completeOrderPay +
                ", complainExpiredDay=" + complainExpiredDay +
                '}';
    }
}
