/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dto;

import cn.shoptnt.model.member.dos.ReceiptHistory;
import cn.shoptnt.model.promotion.coupon.vo.GoodsCouponPrice;
import cn.shoptnt.model.trade.cart.dos.CartDO;
import cn.shoptnt.model.trade.order.enums.OrderTypeEnum;
import cn.shoptnt.model.trade.order.vo.ConsigneeVO;
import cn.shoptnt.model.trade.order.vo.OrderParam;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 订单DTO
 *
 * @author 妙贤
 * @version 1.0
 * @since v7.0.0 2017年3月22日下午9:28:30
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderDTO extends CartDO implements Serializable {

    private static final long serialVersionUID = 8206833000476657708L;

    @Schema(description = "交易编号")
    private String tradeSn;

    @Schema(description = "订单编号")
    private String sn;

    @Schema(description = "收货信息")
    private ConsigneeVO consignee;

    @Schema(description = "配送方式")
    private Integer shippingId;

    @Schema(description = "支付方式")
    private String paymentType;

    @Schema(description = "发货时间")
    private Long shipTime;

    @Schema(description = "发货时间类型")
    private String receiveTime;

    @Schema(description = "会员id")
    private Long memberId;

    @Schema(description = "会员姓名")
    private String memberName;

    @Schema(description = "订单备注")
    private String remark;

    @Schema(description = "创建时间")
    private Long createTime;


    @Schema(description = "配送方式名称")
    private String shippingType;

    @Schema(description = "订单状态")
    private String orderStatus;

    @Schema(description = "付款状态")
    private String payStatus;

    @Schema(description = "配送状态")
    private String shipStatus;

    @Schema(description = "收货人姓名")
    private String shipName;

    @Schema(description = "订单价格")
    private Double orderPrice;

    @Schema(description = "配送费")
    private Double shippingPrice;

    @Schema(description = "评论状态")
    private String commentStatus;

    @Schema(description = "是否已经删除")
    private Integer disabled;

    @Schema(description = "支付方式id")
    private Integer paymentMethodId;

    @Schema(description = "支付插件id")
    private String paymentPluginId;

    @Schema(description = "支付方式名称")
    private String paymentMethodName;

    @Schema(description = "付款账号")
    private String paymentAccount;

    @Schema(description = "商品数量")
    private Integer goodsNum;

    @Schema(description = "发货仓库id")
    private Integer warehouseId;

    @Schema(description = "取消原因")
    private String cancelReason;

    @Schema(description = "收货地址省Id")
    private Long shipProvinceId;

    @Schema(description = "收货地址市Id")
    private Long shipCityId;

    @Schema(description = "收货地址区Id")
    private Long shipRegionId;

    @Schema(description = "收货地址街道Id")
    private Long shipTownId;

    @Schema(description = "收货省")
    private String shipProvince;

    @Schema(description = "收货地址市")
    private String shipCity;

    @Schema(description = "收货地址区")
    private String shipRegion;

    @Schema(description = "收货地址街道")
    private String shipTown;

    @Schema(description = "签收时间")
    private Long signingTime;

    @Schema(description = "签收人姓名")
    private String theSign;

    @Schema(description = "管理员备注")
    private String adminRemark;

    @Schema(description = "收货地址id")
    private Long addressId;

    @Schema(description = "应付金额")
    private Double needPayMoney;

    @Schema(description = "发货单号")
    private String shipNo;

    @Schema(description = "物流公司Id")
    private Integer logiId;

    @Schema(description = "物流公司名称")
    private String logiName;

    @Schema(description = "是否需要发票")
    private Integer needReceipt;

    @Schema(description = "抬头")
    private String receiptTitle;

    @Schema(description = "内容")
    private String receiptContent;

    @Schema(description = "售后状态")
    private String serviceStatus;

    @Schema(description = "订单来源")
    private String clientType;
    @Schema(description = "发票信息")
    private ReceiptHistory receiptHistory;

    /**
     * @see OrderTypeEnum
     * 因增加拼团业务新增订单类型字段 妙贤 2019/1/28 on v7.1.0
     */
    @Schema(description = "订单类型")
    private String orderType;


    /**
     * 订单的扩展数据
     * 为了增加订单的扩展性，个性化的业务可以将个性化数据（如拼团所差人数）存在此字段 妙贤 2019/1/28 on v7.1.0
     */
    @Schema(description = "扩展数据",hidden = true)
    private String orderData;


    /**
     * 使用优惠券的商品
     */
    @Schema(description = "使用优惠券的商品",hidden = true)
    private List<GoodsCouponPrice> goodsCouponPrices;

    @Schema(description = "是否为站点优惠券",hidden = true)
    private  Boolean isSiteCoupon;

    @Schema(description = "订单sku信息",hidden = true)
    private List<OrderSkuVO> orderSkuList;


    public void setOrderData(String orderData) {
        this.orderData = orderData;
    }

    /**
     * 无参构造器
     */
    public OrderDTO() {

    }


    /**
     * 用一个购物车购造订单
     *
     * @param cart
     */
    public OrderDTO(CartDO cart) {

        super(cart.getSellerId(), cart.getSellerName());

        // 初始化产品及优惠数据
        this.setWeight(cart.getWeight());
        this.setPrice(cart.getPrice());
        this.setSkuList(cart.getSkuList());
        this.setGiftJson(cart.getGiftJson());
        this.setGiftPoint(cart.getGiftPoint());
        this.orderType= OrderTypeEnum.NORMAL.name();

    }

    public OrderDTO(OrderParam orderParam){
        super(orderParam.getSellerId(), orderParam.getSellerName());

        // 初始化产品及优惠数据
        this.setWeight(orderParam.getWeight());
        this.setPrice(orderParam.getPrice());
        this.setGiftJson(orderParam.getGiftJson());
        this.setGiftPoint(orderParam.getGiftPoint());
        //购买的会员信息
        this.setMemberId(orderParam.getMemberId());
        this.setMemberName(orderParam.getMemberName());
        //交易号
        this.setTradeSn(orderParam.getTradeSn());

        this.setConsignee(orderParam.getConsignee());
        //设置配送方式
        this.setShippingId(orderParam.getShippingId());
        this.setShippingType(orderParam.getShippingType());

        //支付类型
        this.setPaymentType(orderParam.getPaymentType());
        //发票
        this.setNeedReceipt(orderParam.getNeedReceipt());
        this.setReceiptHistory(orderParam.getReceiptHistory());
        //收货时间
        this.setReceiveTime(orderParam.getReceiveTime());
        //订单备注
        this.setRemark(orderParam.getRemark());

        //订单来源
        this.setClientType(orderParam.getClientType());

        //支付方式
        this.setPaymentType(orderParam.getPaymentType());
        //备注
        this.setRemark(orderParam.getRemark());

        this.setOrderType(orderParam.getOrderType());

        this.setGoodsCouponPrices(orderParam.getCouponGoodsList());
         //是否为站点优惠券
        this.setSiteCoupon(orderParam.getIsSiteCoupon());
    }

    public Boolean getSiteCoupon() {
        return isSiteCoupon;
    }

    public void setSiteCoupon(Boolean siteCoupon) {
        isSiteCoupon = siteCoupon;
    }

    public List<OrderSkuVO> getOrderSkuList() {
        return orderSkuList;
    }

    public void setOrderSkuList(List<OrderSkuVO> orderSkuList) {
        this.orderSkuList = orderSkuList;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "tradeSn='" + tradeSn + '\'' +
                ", sn='" + sn + '\'' +
                ", consignee=" + consignee +
                ", shippingId=" + shippingId +
                ", paymentType='" + paymentType + '\'' +
                ", shipTime=" + shipTime +
                ", receiveTime='" + receiveTime + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", shippingType='" + shippingType + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", payStatus='" + payStatus + '\'' +
                ", shipStatus='" + shipStatus + '\'' +
                ", shipName='" + shipName + '\'' +
                ", orderPrice=" + orderPrice +
                ", shippingPrice=" + shippingPrice +
                ", commentStatus='" + commentStatus + '\'' +
                ", disabled=" + disabled +
                ", paymentMethodId=" + paymentMethodId +
                ", paymentPluginId='" + paymentPluginId + '\'' +
                ", paymentMethodName='" + paymentMethodName + '\'' +
                ", paymentAccount='" + paymentAccount + '\'' +
                ", goodsNum=" + goodsNum +
                ", warehouseId=" + warehouseId +
                ", cancelReason='" + cancelReason + '\'' +
                ", shipProvinceId=" + shipProvinceId +
                ", shipCityId=" + shipCityId +
                ", shipRegionId=" + shipRegionId +
                ", shipTownId=" + shipTownId +
                ", shipProvince='" + shipProvince + '\'' +
                ", shipCity='" + shipCity + '\'' +
                ", shipRegion='" + shipRegion + '\'' +
                ", shipTown='" + shipTown + '\'' +
                ", signingTime=" + signingTime +
                ", theSign='" + theSign + '\'' +
                ", adminRemark='" + adminRemark + '\'' +
                ", addressId=" + addressId +
                ", needPayMoney=" + needPayMoney +
                ", shipNo='" + shipNo + '\'' +
                ", logiId=" + logiId +
                ", logiName='" + logiName + '\'' +
                ", needReceipt=" + needReceipt +
                ", receiptTitle='" + receiptTitle + '\'' +
                ", receiptContent='" + receiptContent + '\'' +
                ", serviceStatus='" + serviceStatus + '\'' +
                ", clientType='" + clientType + '\'' +
                ", receiptHistory=" + receiptHistory +
                ", orderType='" + orderType + '\'' +
                ", orderData='" + orderData + '\'' +
                ", goodsCouponPrices=" + goodsCouponPrices +
                ", orderSkuList=" + orderSkuList +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderDTO that = (OrderDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(tradeSn, that.tradeSn)
                .append(sn, that.sn)
                .append(consignee, that.consignee)
                .append(shippingId, that.shippingId)
                .append(paymentType, that.paymentType)
                .append(shipTime, that.shipTime)
                .append(receiveTime, that.receiveTime)
                .append(memberId, that.memberId)
                .append(memberName, that.memberName)
                .append(remark, that.remark)
                .append(createTime, that.createTime)
                .append(shippingType, that.shippingType)
                .append(orderStatus, that.orderStatus)
                .append(payStatus, that.payStatus)
                .append(shipStatus, that.shipStatus)
                .append(shipName, that.shipName)
                .append(orderPrice, that.orderPrice)
                .append(shippingPrice, that.shippingPrice)
                .append(commentStatus, that.commentStatus)
                .append(disabled, that.disabled)
                .append(paymentMethodId, that.paymentMethodId)
                .append(paymentPluginId, that.paymentPluginId)
                .append(paymentMethodName, that.paymentMethodName)
                .append(paymentAccount, that.paymentAccount)
                .append(goodsNum, that.goodsNum)
                .append(warehouseId, that.warehouseId)
                .append(cancelReason, that.cancelReason)
                .append(shipProvinceId, that.shipProvinceId)
                .append(shipCityId, that.shipCityId)
                .append(shipRegionId, that.shipRegionId)
                .append(shipTownId, that.shipTownId)
                .append(shipProvince, that.shipProvince)
                .append(shipCity, that.shipCity)
                .append(shipRegion, that.shipRegion)
                .append(shipTown, that.shipTown)
                .append(signingTime, that.signingTime)
                .append(theSign, that.theSign)
                .append(adminRemark, that.adminRemark)
                .append(addressId, that.addressId)
                .append(needPayMoney, that.needPayMoney)
                .append(shipNo, that.shipNo)
                .append(logiId, that.logiId)
                .append(logiName, that.logiName)
                .append(needReceipt, that.needReceipt)
                .append(receiptTitle, that.receiptTitle)
                .append(receiptContent, that.receiptContent)
                .append(serviceStatus, that.serviceStatus)
                .append(clientType, that.clientType)
                .append(receiptHistory, that.receiptHistory)
                .append(orderType, that.orderType)
                .append(orderData, that.orderData)
                .append(goodsCouponPrices, that.goodsCouponPrices)
                .append(orderSkuList, that.orderSkuList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(tradeSn)
                .append(sn)
                .append(consignee)
                .append(shippingId)
                .append(paymentType)
                .append(shipTime)
                .append(receiveTime)
                .append(memberId)
                .append(memberName)
                .append(remark)
                .append(createTime)
                .append(shippingType)
                .append(orderStatus)
                .append(payStatus)
                .append(shipStatus)
                .append(shipName)
                .append(orderPrice)
                .append(shippingPrice)
                .append(commentStatus)
                .append(disabled)
                .append(paymentMethodId)
                .append(paymentPluginId)
                .append(paymentMethodName)
                .append(paymentAccount)
                .append(goodsNum)
                .append(warehouseId)
                .append(cancelReason)
                .append(shipProvinceId)
                .append(shipCityId)
                .append(shipRegionId)
                .append(shipTownId)
                .append(shipProvince)
                .append(shipCity)
                .append(shipRegion)
                .append(shipTown)
                .append(signingTime)
                .append(theSign)
                .append(adminRemark)
                .append(addressId)
                .append(needPayMoney)
                .append(shipNo)
                .append(logiId)
                .append(logiName)
                .append(needReceipt)
                .append(receiptTitle)
                .append(receiptContent)
                .append(serviceStatus)
                .append(clientType)
                .append(receiptHistory)
                .append(orderType)
                .append(orderData)
                .append(goodsCouponPrices)
                .append(orderSkuList)
                .toHashCode();
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public ConsigneeVO getConsignee() {
        return consignee;
    }

    public void setConsignee(ConsigneeVO consignee) {
        this.consignee = consignee;
    }

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getShipStatus() {
        return shipStatus;
    }

    public void setShipStatus(String shipStatus) {
        this.shipStatus = shipStatus;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Double getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(Double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentPluginId() {
        return paymentPluginId;
    }

    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Long getShipProvinceId() {
        return shipProvinceId;
    }

    public void setShipProvinceId(Long shipProvinceId) {
        this.shipProvinceId = shipProvinceId;
    }

    public Long getShipCityId() {
        return shipCityId;
    }

    public void setShipCityId(Long shipCityId) {
        this.shipCityId = shipCityId;
    }

    public Long getShipRegionId() {
        return shipRegionId;
    }

    public void setShipRegionId(Long shipRegionId) {
        this.shipRegionId = shipRegionId;
    }

    public Long getShipTownId() {
        return shipTownId;
    }

    public void setShipTownId(Long shipTownId) {
        this.shipTownId = shipTownId;
    }

    public String getShipProvince() {
        return shipProvince;
    }

    public void setShipProvince(String shipProvince) {
        this.shipProvince = shipProvince;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipRegion() {
        return shipRegion;
    }

    public void setShipRegion(String shipRegion) {
        this.shipRegion = shipRegion;
    }

    public String getShipTown() {
        return shipTown;
    }

    public void setShipTown(String shipTown) {
        this.shipTown = shipTown;
    }

    public Long getSigningTime() {
        return signingTime;
    }

    public void setSigningTime(Long signingTime) {
        this.signingTime = signingTime;
    }

    public String getTheSign() {
        return theSign;
    }

    public void setTheSign(String theSign) {
        this.theSign = theSign;
    }

    public String getAdminRemark() {
        return adminRemark;
    }

    public void setAdminRemark(String adminRemark) {
        this.adminRemark = adminRemark;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Double getNeedPayMoney() {
        return needPayMoney;
    }

    public void setNeedPayMoney(Double needPayMoney) {
        this.needPayMoney = needPayMoney;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public Integer getLogiId() {
        return logiId;
    }

    public void setLogiId(Integer logiId) {
        this.logiId = logiId;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public Integer getNeedReceipt() {
        return needReceipt;
    }

    public void setNeedReceipt(Integer needReceipt) {
        this.needReceipt = needReceipt;
    }

    public String getReceiptTitle() {
        return receiptTitle;
    }

    public void setReceiptTitle(String receiptTitle) {
        this.receiptTitle = receiptTitle;
    }

    public String getReceiptContent() {
        return receiptContent;
    }

    public void setReceiptContent(String receiptContent) {
        this.receiptContent = receiptContent;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public ReceiptHistory getReceiptHistory() {
        return receiptHistory;
    }

    public void setReceiptHistory(ReceiptHistory receiptHistory) {
        this.receiptHistory = receiptHistory;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderData() {
        return orderData;
    }

    public List<GoodsCouponPrice> getGoodsCouponPrices() {
        return goodsCouponPrices;
    }

    public void setGoodsCouponPrices(List<GoodsCouponPrice> goodsCouponPrices) {
        this.goodsCouponPrices = goodsCouponPrices;
    }

}
