/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.vo;

import cn.shoptnt.model.member.vo.MemberDepositeVO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 收银台参数VO
 * @author Snow create in 2018/7/11
 * @version v2.0
 * @since v7.0.0
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CashierVO {


    @Schema(name="ship_name",description="收货人姓名")
    private String shipName;

    @Schema(name="ship_addr",description="收货地址")
    private String shipAddr;

    @Schema(name="ship_mobile",description="收货人手机")
    private String shipMobile;

    @Schema(name="ship_tel",description="收货人电话")
    private String shipTel;

    @Schema(name="ship_province",description="配送地区-省份")
    private String shipProvince;

    @Schema(name="ship_city",description="配送地区-城市")
    private String shipCity;

    @Schema(name="ship_county",description="配送地区-区(县)")
    private String shipCounty;

    @Schema(name="ship_town",description="配送街道")
    private String shipTown;

    @Schema(name="need_pay_price",description="应付金额")
    private Double needPayPrice;

    @Schema(name="pay_type_text",description="支付方式")
    private String payTypeText;


    @Schema(name="deposite",description="预存款相关")
    private MemberDepositeVO deposite;

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getShipTel() {
        return shipTel;
    }

    public void setShipTel(String shipTel) {
        this.shipTel = shipTel;
    }

    public String getShipProvince() {
        return shipProvince;
    }

    public void setShipProvince(String shipProvince) {
        this.shipProvince = shipProvince;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipCounty() {
        return shipCounty;
    }

    public void setShipCounty(String shipCounty) {
        this.shipCounty = shipCounty;
    }

    public String getShipTown() {
        return shipTown;
    }

    public void setShipTown(String shipTown) {
        this.shipTown = shipTown;
    }

    public Double getNeedPayPrice() {
        return needPayPrice;
    }

    public void setNeedPayPrice(Double needPayPrice) {
        this.needPayPrice = needPayPrice;
    }

    public String getPayTypeText() {
        return payTypeText;
    }

    public void setPayTypeText(String payTypeText) {
        this.payTypeText = payTypeText;
    }


    public MemberDepositeVO getDeposite() {
        return deposite;
    }

    public void setDeposite(MemberDepositeVO deposite) {
        this.deposite = deposite;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CashierVO that = (CashierVO) o;

        return new EqualsBuilder()
                .append(shipName, that.shipName)
                .append(shipAddr, that.shipAddr)
                .append(shipMobile, that.shipMobile)
                .append(shipTel, that.shipTel)
                .append(shipProvince, that.shipProvince)
                .append(shipCity, that.shipCity)
                .append(shipCounty, that.shipCounty)
                .append(shipTown, that.shipTown)
                .append(needPayPrice, that.needPayPrice)
                .append(payTypeText, that.payTypeText)
                .append(deposite, that.deposite)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(shipName)
                .append(shipAddr)
                .append(shipMobile)
                .append(shipTel)
                .append(shipProvince)
                .append(shipCity)
                .append(shipCounty)
                .append(shipTown)
                .append(needPayPrice)
                .append(payTypeText)
                .append(deposite)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "CashierVO{" +
                "shipName='" + shipName + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                ", shipTel='" + shipTel + '\'' +
                ", shipProvince='" + shipProvince + '\'' +
                ", shipCity='" + shipCity + '\'' +
                ", shipCounty='" + shipCounty + '\'' +
                ", shipTown='" + shipTown + '\'' +
                ", needPayPrice=" + needPayPrice +
                ", payTypeText='" + payTypeText + '\'' +
                ", deposite=" + deposite +
                '}';
    }

}
