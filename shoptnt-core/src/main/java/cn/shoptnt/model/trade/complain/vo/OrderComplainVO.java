/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.complain.vo;

import cn.shoptnt.model.trade.complain.dos.OrderComplain;
import cn.shoptnt.model.trade.complain.dos.OrderComplainCommunication;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * 交易投诉表实体
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderComplainVO extends OrderComplain {

    /**
     * 对话信息
     */
    @Schema(name = "communication_list", description =  "对话信息")
    private List<OrderComplainCommunication> communicationList;


    public List<OrderComplainCommunication> getCommunicationList() {
        return communicationList;
    }

    public void setCommunicationList(List<OrderComplainCommunication> communicationList) {
        this.communicationList = communicationList;
    }
}
