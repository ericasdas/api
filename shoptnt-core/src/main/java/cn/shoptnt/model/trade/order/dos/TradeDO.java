/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import cn.shoptnt.model.trade.cart.vo.PriceDetailVO;
import cn.shoptnt.model.trade.order.enums.TradeStatusEnum;
import cn.shoptnt.model.trade.order.vo.ConsigneeVO;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 交易表实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-09 09:38:06
 */
@TableName("es_trade")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class TradeDO implements Serializable {

    private static final long serialVersionUID = 8834971381961212L;

    /**trade_id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long tradeId;
    /**交易编号*/
    @Schema(name="trade_sn",description="交易编号")
    private String tradeSn;
    /**买家id*/
    @Schema(name="member_id",description="买家id")
    private Long memberId;
    /**买家用户名*/
    @Schema(name="member_name",description="买家用户名")
    private String memberName;
    /**支付方式id*/
    @Schema(name="payment_method_id",description="支付方式id")
    private Long paymentMethodId;
    /**支付插件id*/
    @Schema(name="payment_plugin_id",description="支付插件id")
    private String paymentPluginId;
    /**支付方式名称*/
    @Schema(name="payment_method_name",description="支付方式名称")
    private String paymentMethodName;
    /**支付方式类型*/
    @Schema(name="payment_type",description="支付方式类型")
    private String paymentType;

    /**总价格*/
    @Schema(name="total_price",description="总价格")
    private Double totalPrice;

    /**商品价格*/
    @Schema(name="goods_price",description="商品价格")
    private Double goodsPrice;


    /**运费*/
    @Schema(name="freight_price",description="运费")
    private Double freightPrice;

    /**优惠的金额*/
    @Schema(name="discount_price",description="优惠的金额")
    private Double discountPrice;

    /**收货人id*/
    @Schema(name="consignee_id",description="收货人id")
    private Long consigneeId;
    /**收货人姓名*/
    @Schema(name="consignee_name",description="收货人姓名")
    private String consigneeName;
    /**收货国家*/
    @Schema(name="consignee_country",description="收货国家")
    private String consigneeCountry;
    /**收货国家id*/
    @Schema(name="consignee_country_id",description="收货国家id")
    private Long consigneeCountryId;
    /**收货省*/
    @Schema(name="consignee_province",description="收货省")
    private String consigneeProvince;
    /**收货省id*/
    @Schema(name="consignee_province_id",description="收货省id")
    private Long consigneeProvinceId;
    /**收货市*/
    @Schema(name="consignee_city",description="收货市")
    private String consigneeCity;
    /**收货市id*/
    @Schema(name="consignee_city_id",description="收货市id")
    private Long consigneeCityId;
    /**收货区*/
    @Schema(name="consignee_county",description="收货区")
    private String consigneeCounty;
    /**收货区id*/
    @Schema(name="consignee_county_id",description="收货区id")
    private Long consigneeCountyId;
    /**收货镇*/
    @Schema(name="consignee_town",description="收货镇")
    private String consigneeTown;
    /**收货镇id*/
    @Schema(name="consignee_town_id",description="收货镇id")
    private Long consigneeTownId;
    /**收货详细地址*/
    @Schema(name="consignee_address",description="收货详细地址")
    private String consigneeAddress;
    /**收货人手机号*/
    @Schema(name="consignee_mobile",description="收货人手机号")
    @SecretField(SecretType.MOBILE)
    private String consigneeMobile;
    /**收货人电话*/
    @Schema(name="consignee_telephone",description="收货人电话")
    private String consigneeTelephone;
    /**交易创建时间*/
    @Schema(name="create_time",description="交易创建时间")
    private Long createTime;
    /**订单json(预留，7.0可能废弃)*/
    @Schema(name="order_json",description="订单json(预留，7.0可能废弃)")
    private String orderJson;

    /**订单状态*/
    @Schema(name="trade_status",description="订单状态")
    private String tradeStatus;

    /**
     * 预存款抵扣金额
     */
    @Column(name = "balance")
    @Schema(name="balance",description="预存款抵扣金额")
    private Double balance;



    @JsonIgnore
    protected String scanRounds;

    public Long getTradeId() {
        return tradeId;
    }
    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeSn() {
        return tradeSn;
    }
    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public Long getMemberId() {
        return memberId;
    }
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentPluginId() {
        return paymentPluginId;
    }
    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }
    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }
    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Double getFreightPrice() {
        return freightPrice;
    }
    public void setFreightPrice(Double freightPrice) {
        this.freightPrice = freightPrice;
    }

    public Double getDiscountPrice() {
        if(discountPrice ==null){
            discountPrice=0.0;
        }

        return discountPrice;
    }
    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Long getConsigneeId() {
        return consigneeId;
    }
    public void setConsigneeId(Long consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeName() {
        return consigneeName;
    }
    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeCounty() {
        return consigneeCounty;
    }
    public void setConsigneeCounty(String consigneeCounty) {
        this.consigneeCounty = consigneeCounty;
    }

    public Long getConsigneeCountyId() {
        return consigneeCountyId;
    }
    public void setConsigneeCountyId(Long consigneeCountyId) {
        this.consigneeCountyId = consigneeCountyId;
    }

    public String getConsigneeProvince() {
        return consigneeProvince;
    }
    public void setConsigneeProvince(String consigneeProvince) {
        this.consigneeProvince = consigneeProvince;
    }

    public Long getConsigneeProvinceId() {
        return consigneeProvinceId;
    }
    public void setConsigneeProvinceId(Long consigneeProvinceId) {
        this.consigneeProvinceId = consigneeProvinceId;
    }

    public String getConsigneeCity() {
        return consigneeCity;
    }
    public void setConsigneeCity(String consigneeCity) {
        this.consigneeCity = consigneeCity;
    }

    public Long getConsigneeCityId() {
        return consigneeCityId;
    }
    public void setConsigneeCityId(Long consigneeCityId) {
        this.consigneeCityId = consigneeCityId;
    }

    public String getConsigneeCountry() {
        return consigneeCountry;
    }

    public void setConsigneeCountry(String consigneeCountry) {
        this.consigneeCountry = consigneeCountry;
    }

    public Long getConsigneeCountryId() {
        return consigneeCountryId;
    }

    public void setConsigneeCountryId(Long consigneeCountryId) {
        this.consigneeCountryId = consigneeCountryId;
    }

    public String getOrderJson() {
        return orderJson;
    }

    public void setOrderJson(String orderJson) {
        this.orderJson = orderJson;
    }

    public String getConsigneeTown() {
        return consigneeTown;
    }
    public void setConsigneeTown(String consigneeTown) {
        this.consigneeTown = consigneeTown;
    }

    public Long getConsigneeTownId() {
        return consigneeTownId;
    }
    public void setConsigneeTownId(Long consigneeTownId) {
        this.consigneeTownId = consigneeTownId;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }
    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }
    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getConsigneeTelephone() {
        return consigneeTelephone;
    }
    public void setConsigneeTelephone(String consigneeTelephone) {
        this.consigneeTelephone = consigneeTelephone;
    }

    public Long getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }
    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }


    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }





    public String getScanRounds() {
        return scanRounds;
    }

    public void setScanRounds(String scanRounds) {
        this.scanRounds = scanRounds;
    }
    @Override
    public String toString() {
        return "TradeDO{" +
                "tradeId=" + tradeId +
                ", tradeSn='" + tradeSn + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", paymentMethodId='" + paymentMethodId + '\'' +
                ", paymentPluginId='" + paymentPluginId + '\'' +
                ", paymentMethodName='" + paymentMethodName + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", totalPrice=" + totalPrice +
                ", goodsPrice=" + goodsPrice +
                ", freightPrice=" + freightPrice +
                ", discountPrice=" + discountPrice +
                ", consigneeId=" + consigneeId +
                ", consigneeName='" + consigneeName + '\'' +
                ", consigneeCountry='" + consigneeCountry + '\'' +
                ", consigneeCountryId=" + consigneeCountryId +
                ", consigneeProvince='" + consigneeProvince + '\'' +
                ", consigneeProvinceId=" + consigneeProvinceId +
                ", consigneeCity='" + consigneeCity + '\'' +
                ", consigneeCityId=" + consigneeCityId +
                ", consigneeCounty='" + consigneeCounty + '\'' +
                ", consigneeCountyId=" + consigneeCountyId +
                ", consigneeTown='" + consigneeTown + '\'' +
                ", consigneeTownId=" + consigneeTownId +
                ", consigneeAddress='" + consigneeAddress + '\'' +
                ", consigneeMobile='" + consigneeMobile + '\'' +
                ", consigneeTelephone='" + consigneeTelephone + '\'' +
                ", createTime=" + createTime +
                ", orderJson='" + orderJson + '\'' +
                ", tradeStatus='" + tradeStatus + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TradeDO that = (TradeDO) o;

        return new EqualsBuilder()
                .append(tradeId, that.tradeId)
                .append(tradeSn, that.tradeSn)
                .append(memberId, that.memberId)
                .append(memberName, that.memberName)
                .append(paymentMethodId, that.paymentMethodId)
                .append(paymentPluginId, that.paymentPluginId)
                .append(paymentMethodName, that.paymentMethodName)
                .append(paymentType, that.paymentType)
                .append(totalPrice, that.totalPrice)
                .append(goodsPrice, that.goodsPrice)
                .append(freightPrice, that.freightPrice)
                .append(discountPrice, that.discountPrice)
                .append(consigneeId, that.consigneeId)
                .append(consigneeName, that.consigneeName)
                .append(consigneeCountry, that.consigneeCountry)
                .append(consigneeCountryId, that.consigneeCountryId)
                .append(consigneeProvince, that.consigneeProvince)
                .append(consigneeProvinceId, that.consigneeProvinceId)
                .append(consigneeCity, that.consigneeCity)
                .append(consigneeCityId, that.consigneeCityId)
                .append(consigneeCounty, that.consigneeCounty)
                .append(consigneeCountyId, that.consigneeCountyId)
                .append(consigneeTown, that.consigneeTown)
                .append(consigneeTownId, that.consigneeTownId)
                .append(consigneeAddress, that.consigneeAddress)
                .append(consigneeMobile, that.consigneeMobile)
                .append(consigneeTelephone, that.consigneeTelephone)
                .append(createTime, that.createTime)
                .append(orderJson, that.orderJson)
                .append(tradeStatus, that.tradeStatus)
                .append(balance, that.balance)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(tradeId)
                .append(tradeSn)
                .append(memberId)
                .append(memberName)
                .append(paymentMethodId)
                .append(paymentPluginId)
                .append(paymentMethodName)
                .append(paymentType)
                .append(totalPrice)
                .append(goodsPrice)
                .append(freightPrice)
                .append(discountPrice)
                .append(consigneeId)
                .append(consigneeName)
                .append(consigneeCountry)
                .append(consigneeCountryId)
                .append(consigneeProvince)
                .append(consigneeProvinceId)
                .append(consigneeCity)
                .append(consigneeCityId)
                .append(consigneeCounty)
                .append(consigneeCountyId)
                .append(consigneeTown)
                .append(consigneeTownId)
                .append(consigneeAddress)
                .append(consigneeMobile)
                .append(consigneeTelephone)
                .append(createTime)
                .append(orderJson)
                .append(tradeStatus)
                .append(balance)
                .toHashCode();
    }

    public TradeDO() {

    }

    /**
     * 参数构造器
     * @param tradeVO
     */
    public TradeDO(TradeVO tradeVO) {
        PriceDetailVO priceDetail = tradeVO.getPriceDetail();

        this.setTotalPrice(priceDetail.getTotalPrice());
        this.setGoodsPrice(priceDetail.getGoodsPrice());
        this.setFreightPrice(priceDetail.getFreightPrice());
        this.setDiscountPrice(priceDetail.getDiscountPrice());
        this.setBalance(0D);

        ConsigneeVO consignee =  tradeVO.getConsignee();

        this.setConsigneeName(consignee.getName());
        this.setConsigneeAddress(consignee.getAddress());
        this.setConsigneeId(consignee.getConsigneeId());
        this.setConsigneeMobile(consignee.getMobile());

        this.setConsigneeProvince(consignee.getProvince());
        this.setConsigneeCity(consignee.getCity());
        this.setConsigneeCounty(consignee.getCounty());

        this.setConsigneeProvinceId(consignee.getProvinceId());
        this.setConsigneeCityId(consignee.getCityId());
        this.setConsigneeCountyId(consignee.getCountyId());
        this.setConsigneeTown(consignee.getTown());

        this.setTradeSn(tradeVO.getTradeSn());
        this.setPaymentType(tradeVO.getPaymentType());
        this.setCreateTime(DateUtil.getDateline());

        //交易状态
        this.setTradeStatus(TradeStatusEnum.NEW.value());
        this.setMemberId(tradeVO.getMemberId());
        this.setMemberName(tradeVO.getMemberName());
    }

}
