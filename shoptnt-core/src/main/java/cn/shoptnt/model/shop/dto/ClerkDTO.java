/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.dto;

import cn.shoptnt.framework.validation.annotation.Mobile;
import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 店员DTO
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-04 18:48:39
 */
public class ClerkDTO {
    /**
     * 手机号码
     */
    @Schema(description =  "手机号码", required = true)
    private String mobile;
    /**
     * 权限id
     */
    @Schema(name = "role_id", description =  "角色id,如果是店主则传0", required = true)
    private Long roleId;


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
