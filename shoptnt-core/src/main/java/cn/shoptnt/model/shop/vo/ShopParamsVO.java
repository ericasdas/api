/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 
 * 店铺搜索条件VO
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午8:43:57
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShopParamsVO {
	/** 页码 */
	@Schema(name="page_no",description =  "页码")
	private Long pageNo;
	/** 分页数 */
	@Schema(name="page_size",description = "分页数")
	private Long pageSize;
	 /**店铺名称*/
    @Schema(name="shop_name",description = "店铺名称")
    private String shopName;	
    /**会员名称*/
    @Schema(name="member_name",description = "会员名称")
    private String memberName;
    /**开始时间*/
    @Schema(name="start_time",description = "店铺开始时间")
    private String startTime;
    /**结束时间*/
    @Schema(name="end_time",description = "店铺关闭时间")
    private String endTime;
    /**关键字*/
    @Schema(name="keyword",description = "关键字")
    private String keyword;
    /**店铺状态*/
    @Schema(name="shop_disable",description = "店铺状态")
    private String shopDisable;
	/**排序方式*/
	@Schema(name="order",description = "排序方式")
    private String order ;

	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getShopDisable() {
		return shopDisable;
	}
	public void setShopDisable(String shopDisable) {
		this.shopDisable = shopDisable;
	}
	public Long getPageNo() {
		return pageNo;
	}
	public void setPageNo(Long pageNo) {
		this.pageNo = pageNo;
	}
	public Long getPageSize() {
		return pageSize;
	}
	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "ShopParamsVO{" +
				"pageNo=" + pageNo +
				", pageSize=" + pageSize +
				", shopName='" + shopName + '\'' +
				", memberName='" + memberName + '\'' +
				", startTime='" + startTime + '\'' +
				", endTime='" + endTime + '\'' +
				", keyword='" + keyword + '\'' +
				", shopDisable='" + shopDisable + '\'' +
				", order='" + order + '\'' +
				'}';
	}
}
