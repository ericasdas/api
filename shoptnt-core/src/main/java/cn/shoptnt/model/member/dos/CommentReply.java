/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 评论回复实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 16:34:50
 */
@TableName(value = "es_comment_reply")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CommentReply implements Serializable {

    private static final long serialVersionUID = 8995158403058181L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long replyId;
    /**
     * 评论id
     */
    @Schema(name = "comment_id", description =  "评论id")
    private Long commentId;

    /**
     * 回复内容
     */
    @Schema(name = "content", description =  "回复内容")
    private String content;

    /**
     * 创建时间
     */
    @Schema(name = "create_time", description =  "创建时间")
    private Long createTime;
    /**
     * 商家或者买家
     */
    @Schema(name = "role", description =  "商家或者买家")
    private String role;
    /**
     * 父子路径0|10|
     */
    @Schema(name = "path", description =  "父子路径0|10|")
    private String path;

    @Schema(name = "reply_type", description =  "回复类型 :初评(INITIAL),追评(ADDITIONAL)")
    private String replyType;

    public Long getReplyId() {
        return replyId;
    }

    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getReplyType() {
        return replyType;
    }

    public void setReplyType(String replyType) {
        this.replyType = replyType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommentReply that = (CommentReply) o;

        return new EqualsBuilder()
                .append(replyId, that.replyId)
                .append(commentId, that.commentId)
                .append(content, that.content)
                .append(createTime, that.createTime)
                .append(role, that.role)
                .append(path, that.path)
                .append(replyType, that.replyType)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(replyId)
                .append(commentId)
                .append(content)
                .append(createTime)
                .append(role)
                .append(path)
                .append(replyType)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "CommentReply{" +
                "replyId=" + replyId +
                ", commentId=" + commentId +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", role='" + role + '\'' +
                ", path='" + path + '\'' +
                '}';
    }


}
