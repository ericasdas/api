/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;


import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 请求登陆model
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-09-24
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LoginUserDTO implements Serializable {

    private static final long serialVersionUID = -1232483319436590972L;

    @Schema(name = "uuid",description =  "此次登陆随机数")
    private String uuid;

    @Schema(name = "tokenOutTime",description =  "token过期时间")
    private Integer tokenOutTime;

    @Schema(name = "refreshTokenOutTime",description =  "refreshToken过期时间")
    private Integer refreshTokenOutTime;

    @Schema(name = "openid", description =  "openid", required = true)
    private String openid;

    @Schema(name = "openType", description =  "openid类型")
    private ConnectTypeEnum openType;

    @Schema(name = "unionid", description =  "unionid", required = true)
    private String unionid;

    @Schema(name = "unionType", description =  "unionid类型")
    private ConnectTypeEnum unionType;


    @Schema(name = "headimgurl", description =  "头像",hidden = true)
    private String headimgurl;

    @Schema(name = "nickName", description =  "用户昵称")
    private String nickName;

    @Schema(name = "sex", description =  "性别：1:男;0:女")
    private Integer sex;

    @Schema(name = "province", description =  "省份")
    private String province;

    @Schema(name = "city", description =  "城市")
    private String city;

    @Schema(name = "oldUuid", description =  "重定向之前的uuid（分销使用）")
    private String oldUuid;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getTokenOutTime() {
        return tokenOutTime;
    }

    public void setTokenOutTime(Integer tokenOutTime) {
        this.tokenOutTime = tokenOutTime;
    }

    public Integer getRefreshTokenOutTime() {
        return refreshTokenOutTime;
    }

    public void setRefreshTokenOutTime(Integer refreshTokenOutTime) {
        this.refreshTokenOutTime = refreshTokenOutTime;
    }


    public ConnectTypeEnum getOpenType() {
        return openType;
    }

    public void setOpenType(ConnectTypeEnum openType) {
        this.openType = openType;
    }

    public ConnectTypeEnum getUnionType() {
        return unionType;
    }

    public void setUnionType(ConnectTypeEnum unionType) {
        this.unionType = unionType;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOldUuid() {
        return oldUuid;
    }

    public void setOldUuid(String oldUuid) {
        this.oldUuid = oldUuid;
    }
}
