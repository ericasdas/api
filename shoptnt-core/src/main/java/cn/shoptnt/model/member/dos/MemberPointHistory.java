/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * 会员积分表实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-03 15:44:12
 */
@TableName(value = "es_member_point_history")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberPointHistory implements Serializable {

    private static final long serialVersionUID = 5222393178191730L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 会员ID
     */
    @Schema(name = "member_id", description =  "会员ID")
    private Long memberId;
    /**
     * 等级积分
     */
    @Min(message = "必须为数字", value =  0)
    @Schema(name = "gade_point", description =  "等级积分")
    private Integer gradePoint;
    /**
     * 操作时间
     */
    @Schema(name = "time", description =  "操作时间")
    private Long time;
    /**
     * 操作理由
     */
    @Schema(name = "reason", description =  "操作理由")
    private String reason;
    /**
     * 等级积分类型
     */
    @Min(message = "最小值为0", value = 0)
    @Max(message = "最大值为1", value = 1)
    @Schema(name = "grade_point_type", description =  "等级积分类型 1为增加，0为消费")
    private Integer gradePointType;
    /**
     * 操作者
     */
    @Schema(name = "operator", description = "操作者")
    private String operator;
    /**
     * 消费积分
     */
    @Min(message = "必须为数字", value = 0)
    @Schema(name = "consum_point", description = "消费积分")
    private Long consumPoint;
    /**
     * 消费积分类型
     */
    @Min(message = "最小值为0", value = 0)
    @Max(message = "最大值为1", value = 1)
    @Schema(name = "consum_point_type", description =  "消费积分类型，1为增加，0为消费")
    private Integer consumPointType;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Long getConsumPoint() {
        return consumPoint;
    }

    public void setConsumPoint(Long consumPoint) {
        this.consumPoint = consumPoint;
    }

    public Integer getConsumPointType() {
        return consumPointType;
    }

    public void setConsumPointType(Integer consumPointType) {
        this.consumPointType = consumPointType;
    }

    public Integer getGradePoint() {
        return gradePoint;
    }

    public void setGradePoint(Integer gradePoint) {
        this.gradePoint = gradePoint;
    }

    public Integer getGradePointType() {
        return gradePointType;
    }

    public void setGradePointType(Integer gradePointType) {
        this.gradePointType = gradePointType;
    }

    @Override
    public String toString() {
        return "MemberPointHistory{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", gradePoint=" + gradePoint +
                ", time=" + time +
                ", reason='" + reason + '\'' +
                ", gradePointType=" + gradePointType +
                ", operator='" + operator + '\'' +
                ", consumPoint=" + consumPoint +
                ", consumPointType=" + consumPointType +
                '}';
    }
}
