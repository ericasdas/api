/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 店铺评分DTO
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:38:00
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberShopScoreDTO implements Serializable {

    private static final long serialVersionUID = 9224429719464458L;

    /**发货速度评分*/
    @Column(name = "delivery_score")
    @Schema(name="delivery_score",description = "发货速度评分")
    private Double deliveryScore;
    /**描述相符度评分*/
    @Column(name = "description_score")
    @Schema(name="description_score",description = "描述相符度评分")
    private Double descriptionScore;
    /**服务评分*/
    @Column(name = "service_score")
    @Schema(name="service_score",description = "服务评分")
    private Double serviceScore;
    /**卖家*/
    @Column(name = "seller_id")
    @Schema(name="seller_id",description = "卖家")
    private Long sellerId;

    public Double getDeliveryScore() {
        return deliveryScore;
    }

    public void setDeliveryScore(Double deliveryScore) {
        this.deliveryScore = deliveryScore;
    }

    public Double getDescriptionScore() {
        return descriptionScore;
    }

    public void setDescriptionScore(Double descriptionScore) {
        this.descriptionScore = descriptionScore;
    }

    public Double getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(Double serviceScore) {
        this.serviceScore = serviceScore;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "MemberShopScoreDTO{" +
                "deliveryScore=" + deliveryScore +
                ", descriptionScore=" + descriptionScore +
                ", serviceScore=" + serviceScore +
                ", sellerId=" + sellerId +
                '}';
    }
}
