/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.payment.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * @description: 支付账单VO
 * @author: liuyulei
 * @create: 2019-12-27 10:23
 * @version:1.0
 * @since:7.1.4
 **/
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PaymentBillVO implements Serializable {
    private static final long serialVersionUID = -2694305846531958299L;
    /**
     * 交易单号
     */
    @Schema(name="sub_sn",description = "单号")
    private String subSn;
    /**
     * 支付账单编号（提交给第三方平台单号）
     */
    @Schema(name="bill_sn",description = "提交给第三方平台单号")
    private String billSn;
    /**
     * 交易类型
     */
    @Schema(name="service_type",description = "交易类型")
    private String serviceType;
    /**
     * 支付方式名称
     */
    @Schema(name="payment_name",description = "支付方式名称")
    private String paymentName;
    /**
     * 交易金额
     */
    @Schema(name="trade_price",description = "交易金额")
    private Double tradePrice;

    /**
     * 支付插件id
     */
    @Schema(name="payment_plugin_id",description = "支付插件id")
    private String paymentPluginId;

    /**
     * 支付方式
     */
    @Schema(name="pay_mode",description = "支付方式")
    private String payMode;

    /**
     * 客户端类型
     */
    @Schema(name="client_type",description = "客户端类型")
    private String clientType;


    /**
     * 支付配置
     */
    @Schema(name="pay_config",description = "客户端类型")
    private String payConfig;

    public String getSubSn() {
        return subSn;
    }

    public void setSubSn(String subSn) {
        this.subSn = subSn;
    }

    public String getBillSn() {
        return billSn;
    }

    public void setBillSn(String billSn) {
        this.billSn = billSn;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public Double getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(Double tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getPaymentPluginId() {
        return paymentPluginId;
    }

    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getPayConfig() {
        return payConfig;
    }

    public void setPayConfig(String payConfig) {
        this.payConfig = payConfig;
    }

    @Override
    public String toString() {
        return "PaymentBillVO{" +
                "subSn='" + subSn + '\'' +
                ", billSn='" + billSn + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", paymentName='" + paymentName + '\'' +
                ", tradePrice=" + tradePrice +
                ", paymentPluginId='" + paymentPluginId + '\'' +
                ", payMode='" + payMode + '\'' +
                ", clientType='" + clientType + '\'' +
                ", payConfig='" + payConfig + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentBillVO that = (PaymentBillVO) o;

        return new EqualsBuilder()
                .append(subSn, that.subSn)
                .append(billSn, that.billSn)
                .append(serviceType, that.serviceType)
                .append(paymentName, that.paymentName)
                .append(tradePrice, that.tradePrice)
                .append(paymentPluginId, that.paymentPluginId)
                .append(payMode, that.payMode)
                .append(clientType, that.clientType)
                .append(payConfig, that.payConfig)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(subSn)
                .append(billSn)
                .append(serviceType)
                .append(paymentName)
                .append(tradePrice)
                .append(paymentPluginId)
                .append(payMode)
                .append(clientType)
                .append(payConfig)
                .toHashCode();
    }

}
