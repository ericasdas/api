/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.payment.enums;

/**
 * 支付账单支付方式变化类型
 *
 * @author zh create in 2018/4/8
 * @version v2.0
 * @since v7.0.0
 */
public enum PaymentBillChangeTypeEnum {

    /**
     * 支付金额变化
     */
    PRICE("支付金额变化"),

    /**
     * 支付方式变化
     */
    PAYMENT_METHOD("支付方式变化"),
    /**
     * 支付类型变化 trade or order
     */
    SERVICE_TYPE("支付类型");

    private String description;


    PaymentBillChangeTypeEnum(String description) {
        this.description = description;
    }


    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }

}
