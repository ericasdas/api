/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.statistics.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;

/**
 * 流量分析，店铺流量DO
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/8 17:57
 */
@Schema(description = "店铺流量DO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PageViewDTO {

    @Schema(description = "周期类型", name = "cycle_type")
    @NotNull(message = "日期类型不可为空")
    private String cycleType;

    @Schema(description = "年份，默认当前年份", name = "year")
    private Integer year;

    @Schema(description = "月份，默认当前月份", name = "month")
    private Integer month;

    @Schema(description = "商家id", name = "seller_id")
    private Long sellerId;

    public String getCycleType() {
        return cycleType;
    }

    public void setCycleType(String cycleType) {
        this.cycleType = cycleType;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "PageViewDO{" +
                "cycleType='" + cycleType + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", sellerId=" + sellerId +
                '}';
    }
}
