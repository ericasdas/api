/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * 商品查询条件
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月21日 下午3:46:04
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsQueryParam {
    /**
     * 页码
     */
    @Schema(name = "page_no", description = "页码")
    private Long pageNo;
    /**
     * 分页数
     */
    @Schema(name = "page_size", description = "分页数")
    private Long pageSize;
    /**
     * 是否上架 0代表已下架，1代表已上架
     */
    @Schema(name = "market_enable", description = "是否上架 0代表已下架，1代表已上架")
    @Min(value = 0 , message = "审核状态不正确")
    @Max(value = 2 , message = "审核状态不正确")
    private Integer marketEnable;
    /**
     * 店铺分类
     */
    @Schema(name = "shop_cat_path", description = "店铺分类Path0|10|")
    private String shopCatPath;
    /**
     * 关键字
     */
    @Schema(name = "keyword", description = "关键字")
    private String keyword;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description = "商品名称")
    private String goodsName;
    /**
     * 商品编号
     */
    @Schema(name = "goods_sn", description = "商品编号")
    private String goodsSn;
    /**
     * 店铺名称
     */
    @Schema(name = "seller_name", description = "店铺名称")
    private String sellerName;
    /**
     * 卖家id
     */
    @Schema(name = "seller_id", description = "卖家id")
    private Long sellerId;
    /**
     * 商品分类路径
     */
    @Schema(name = "category_path", description = "商品分类路径，例如0|10|")
    private String categoryPath;
    /**
     * 商品审核状态
     */
    @Schema(name = "is_auth", description = "商品审核状态 0：待审核，1：审核通过，2：审核拒绝")
    @Min(value = 0 , message = "审核状态不正确")
    @Max(value = 2 , message = "审核状态不正确")
    private Integer isAuth;
    /**
     * 商品是否已放入回收站
     */
    @Schema(name = "disabled", description = "商品是否已放入回收站 0：是，1：否")
    @Min(value = 0 , message = "值不正确")
    @Max(value = 0 , message = "值不正确")
    private Integer disabled;
    /**
     * 商品类型
     */
    @Schema(name = "goods_type", description = "商品类型 NORMAL：正常商品，POINT：积分商品")
    private String  goodsType;
    /**
     * 商品品牌
     */
    @Schema(name = "brand_id", description = "商品品牌ID")
    private Integer brandId;
    /**
     * 商品价格--价格区间起始值
     */
    @Schema(name = "brand_id", description = "商品价格--价格区间起始值")
    private Double start_price;
    /**
     * 商品价格--价格区间结束值
     */
    @Schema(name = "brand_id", description = "商品品牌ID")
    private Double end_price;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }


    public String getShopCatPath() {
        return shopCatPath;
    }

    public void setShopCatPath(String shopCatPath) {
        this.shopCatPath = shopCatPath;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Double getStart_price() {
        return start_price;
    }

    public void setStart_price(Double start_price) {
        this.start_price = start_price;
    }

    public Double getEnd_price() {
        return end_price;
    }

    public void setEnd_price(Double end_price) {
        this.end_price = end_price;
    }

    @Override
    public String toString() {
        return "GoodsQueryParam{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", marketEnable=" + marketEnable +
                ", shopCatPath='" + shopCatPath + '\'' +
                ", keyword='" + keyword + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsSn='" + goodsSn + '\'' +
                ", sellerName='" + sellerName + '\'' +
                ", sellerId=" + sellerId +
                ", categoryPath='" + categoryPath + '\'' +
                ", isAuth=" + isAuth +
                ", disabled=" + disabled +
                ", goodsType='" + goodsType + '\'' +
                ", brandId=" + brandId +
                ", start_price=" + start_price +
                ", end_price=" + end_price +
                '}';
    }
}
