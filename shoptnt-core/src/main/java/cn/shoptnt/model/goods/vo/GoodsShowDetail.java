/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import java.io.Serializable;
import java.util.List;

import cn.shoptnt.model.goods.dos.GoodsGalleryDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 商品详情页使用
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月29日 上午9:54:05
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsShowDetail extends CacheGoods implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3531885212488390703L;
    @Schema(name = "分类名称")
    private String categoryName;
    @Schema(name = "是否下架或删除，1正常  0 下架或删除")
    private Integer goodsOff;
    @Schema(name = "商品参数")
    private List<GoodsParamsGroupVO> paramList;
    @Schema(name = "商品相册")
    private List<GoodsGalleryDO> galleryList;

    /**
     * seo标题
     */
    @Schema(name = "page_title", description =  "seo标题")
    private String pageTitle;
    /**
     * seo关键字
     */
    @Schema(name = "meta_keywords", description =  "seo关键字")
    private String metaKeywords;
    /**
     * seo描述
     */
    @Schema(name = "meta_description", description =  "seo描述")
    private String metaDescription;

    /** 谁承担运费0：买家承担，1：卖家承担 */
    @Schema(name = "goods_transfee_charge", description =  "谁承担运费0：买家承担，1：卖家承担")
    private Integer goodsTransfeeCharge;

    @Override
    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    @Override
    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getGoodsOff() {
        return goodsOff;
    }

    public void setGoodsOff(Integer goodsOff) {
        this.goodsOff = goodsOff;
    }

    public List<GoodsParamsGroupVO> getParamList() {
        return paramList;
    }

    public void setParamList(List<GoodsParamsGroupVO> paramList) {
        this.paramList = paramList;
    }

    public List<GoodsGalleryDO> getGalleryList() {
        return galleryList;
    }

    public void setGalleryList(List<GoodsGalleryDO> galleryList) {
        this.galleryList = galleryList;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }
}
