/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.dos.CategoryDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * 商品分类vo
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月16日 下午4:53:23
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CategoryVO extends CategoryDO {

    private static final long serialVersionUID = 3843585201476087204L;

    @Schema(description = "子分类列表")
    private List<CategoryVO> children;

    @Schema(description = "分类关联的品牌列表")
    private List<BrandVO> brandList;

    public CategoryVO() {

    }

    public CategoryVO(CategoryDO cat) {
        this.setCategoryId(cat.getCategoryId());
        this.setCategoryPath(cat.getCategoryPath());
        this.setName(cat.getName());
        this.setParentId(cat.getParentId());
        this.setImage(cat.getImage());
        this.setCategoryOrder(cat.getCategoryOrder());
        this.setAdvImage(cat.getAdvImage());
        this.setAdvImageLink(cat.getAdvImageLink());
        this.setIsShow(cat.getIsShow());

    }

    public List<CategoryVO> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryVO> children) {
        this.children = children;
    }


    public List<BrandVO> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<BrandVO> brandList) {
        this.brandList = brandList;
    }

    @Override
    public String toString() {
        return "CategoryVO{" +
                "children=" + children +
                ", brandList=" + brandList +
                '}';
    }
}
