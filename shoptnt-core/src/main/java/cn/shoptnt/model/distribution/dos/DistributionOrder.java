/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.distribution.dos;

import cn.shoptnt.model.distribution.dto.DistributionRefundDTO;

/**
 * DistributionOrder
 *
 * @author zhanghao
 * @version v1.0
 * @since v7.0
 * 2021-08-12
 */
public class DistributionOrder {

    /**
     * order 分销订单对象
     */
    private DistributionOrderDO order;

    /**
     * distributionRefundDTO 分销退款单对象
     */
    private DistributionRefundDTO distributionRefundDTO;

    public DistributionOrder(DistributionOrderDO order, DistributionRefundDTO distributionRefundDTO) {
        this.order = order;
        this.distributionRefundDTO = distributionRefundDTO;
    }

    public DistributionOrderDO getOrder() {
        return order;
    }

    public void setOrder(DistributionOrderDO order) {
        this.order = order;
    }

    public DistributionRefundDTO getDistributionRefundDTO() {
        return distributionRefundDTO;
    }

    public void setDistributionRefundDTO(DistributionRefundDTO distributionRefundDTO) {
        this.distributionRefundDTO = distributionRefundDTO;
    }
}
