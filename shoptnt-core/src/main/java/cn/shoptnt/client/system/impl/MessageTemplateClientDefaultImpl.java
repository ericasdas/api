/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.MessageTemplateClient;
import cn.shoptnt.model.system.enums.MessageCodeEnum;
import cn.shoptnt.model.system.dos.MessageTemplateDO;
import cn.shoptnt.model.system.enums.WechatMsgTemplateTypeEnum;
import cn.shoptnt.service.system.MessageTemplateManager;
import cn.shoptnt.service.system.WechatMsgTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/27 09:44
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class MessageTemplateClientDefaultImpl implements MessageTemplateClient {

    @Autowired
    private MessageTemplateManager messageTemplateManager;

    @Autowired
    private WechatMsgTemplateManager wechatMsgTemplateManager;

    @Override
    public MessageTemplateDO getModel(MessageCodeEnum messageCodeEnum) {
        return messageTemplateManager.getModel(messageCodeEnum);
    }

    @Override
    public void sendWechatMsg(Long memberId, WechatMsgTemplateTypeEnum messageType, List<Object> keywords) {

        wechatMsgTemplateManager.send(memberId, messageType, keywords);
    }
}
