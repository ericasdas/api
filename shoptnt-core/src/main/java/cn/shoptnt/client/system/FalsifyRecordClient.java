/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;

import cn.shoptnt.model.security.FalsifyRecord;

/**
 * @author zs
 * @version v1.0
 * @Description: 篡改记录
 * @date 2021-12-21
 * @since v5.2.3
 */
public interface FalsifyRecordClient {
    void add(FalsifyRecord falsifyRecord);
}
