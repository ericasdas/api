/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.distribution;

import cn.shoptnt.model.distribution.dos.DistributionOrder;
import cn.shoptnt.model.distribution.dos.DistributionOrderDO;

/**
 * DistributionClient
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午1:54
 */
public interface BillMemberClient {

    /**
     * 购买商品产生的结算
     *
     * @param order
     */
    void buyShop(DistributionOrderDO order);


    /**
     * 退货商品产生的结算
     *
     * @param distributionOrder
     */
    void returnShop(DistributionOrder distributionOrder);
}
