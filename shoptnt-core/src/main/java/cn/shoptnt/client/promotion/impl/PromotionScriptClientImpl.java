/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.promotion.impl;

import cn.shoptnt.client.promotion.PromotionScriptClient;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.halfprice.dos.HalfPriceDO;
import cn.shoptnt.model.promotion.minus.dos.MinusDO;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.seckill.enums.SeckillGoodsApplyStatusEnum;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;
import cn.shoptnt.service.promotion.coupon.CouponManager;
import cn.shoptnt.service.promotion.fulldiscount.FullDiscountManager;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyActiveManager;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyScriptManager;
import cn.shoptnt.service.promotion.halfprice.HalfPriceManager;
import cn.shoptnt.service.promotion.minus.MinusManager;
import cn.shoptnt.service.promotion.seckill.SeckillGoodsManager;
import cn.shoptnt.service.promotion.seckill.SeckillManager;
import cn.shoptnt.service.promotion.seckill.SeckillScriptManager;
import cn.shoptnt.service.trade.pintuan.PintuanManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 促销脚本客户端
 *
 * @author fk
 * @version v7.0
 * @date 19/3/28 上午11:10
 * @since v7.0
 */
@Service
public class PromotionScriptClientImpl implements PromotionScriptClient {

    @Autowired
    private GroupbuyScriptManager groupbuyScriptManager;
    @Autowired
    private SeckillScriptManager seckillScriptManager;
    @Autowired
    private FullDiscountManager fullDiscountManager;
    @Autowired
    private MinusManager minusManager;
    @Autowired
    private HalfPriceManager halfPriceManager;
    @Autowired
    private GroupbuyActiveManager groupbuyActiveManager;
    @Autowired
    private SeckillManager seckillManager;
    @Autowired
    private SeckillGoodsManager seckillGoodsManager;
    @Autowired
    private PintuanManager pintuanManager;
    @Autowired
    private CouponManager couponManager;

    @Override
    public void createGroupBuyCacheScript(Long promotionId, List<PromotionGoodsDO> goodsList) {
        groupbuyScriptManager.createCacheScript(promotionId, goodsList);
    }

    @Override
    public void deleteGroupBuyCacheScript(Long promotionId, List<PromotionGoodsDO> goodsList) {
        groupbuyScriptManager.deleteCacheScript(promotionId, goodsList);
    }

    @Override
    public void deleteCacheScript(Long promotionId, List<SeckillApplyDO> goodsList) {
        seckillScriptManager.deleteCacheScript(promotionId, goodsList);
    }

    @Override
    public List<FullDiscountDO> selectNoEndFullDiscount() {
        return fullDiscountManager.selectNoEndFullDiscount();
    }

    @Override
    public List<MinusDO> selectNoEndMinus() {
        return minusManager.selectNoEndMinus();
    }

    @Override
    public List<HalfPriceDO> selectNoEndHalfPrice() {
        return halfPriceManager.selectNoEndHalfPrice();
    }

    @Override
    public List<GroupbuyActiveDO> selectNoEndGroupbuy() {
        return groupbuyActiveManager.selectNoEndGroupbuy();
    }

    @Override
    public List<SeckillDO> selectNoEndSeckill() {
        return seckillManager.selectNoEndSeckill();
    }

    @Override
    public List<SeckillApplyDO> selectGoodsBySeckillId(Long seckillId) {
        return seckillGoodsManager.getGoodsList(seckillId, SeckillGoodsApplyStatusEnum.PASS.value());
    }

    @Override
    public void createCacheScript(Long seckillId, List<SeckillApplyDO> goodsList) {
        seckillScriptManager.createCacheScript(seckillId, goodsList);
    }

    @Override
    public List<Pintuan> selectNoEndPintuan() {
        return pintuanManager.selectNoEndPintuan();
    }

    @Override
    public List<CouponDO> selectNoEndCoupon() {
        return couponManager.selectNoEndCoupon();
    }
}
