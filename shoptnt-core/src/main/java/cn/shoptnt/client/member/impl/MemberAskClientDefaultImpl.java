/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberAskClient;
import cn.shoptnt.model.member.dos.AskMessageDO;
import cn.shoptnt.service.member.AskMessageManager;
import cn.shoptnt.service.member.MemberAskManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 评论对外接口实现
 * @date 2018/7/26 11:30
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class MemberAskClientDefaultImpl implements MemberAskClient {

    @Autowired
    private MemberAskManager memberAskManager;

    @Autowired
    private AskMessageManager askMessageManager;

    @Override
    public Integer getNoReplyCount(Long sellerId) {

        return memberAskManager.getNoReplyCount(sellerId);
    }

    @Override
    public void sendMessage(AskMessageDO askMessageDO) {
        this.askMessageManager.addAskMessage(askMessageDO);
    }
}
