/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.DepositeClient;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import cn.shoptnt.model.member.vo.MemberDepositeVO;
import cn.shoptnt.service.member.DepositeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @description: 预存款客户端实现
 * @author: liuyulei
 * @create: 2019-12-30 18:41
 * @version:1.0
 * @since:7.1.4
 **/
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class DepositeClientImpl implements DepositeClient {

    @Autowired
    private DepositeManager depositeManager;

    @Override
    public Boolean increase(Double money, Long memberId, String detail) {
        return this.depositeManager.increase(money,memberId,detail);
    }

    @Override
    public Boolean reduce(Double money, Long memberId, String detail) {
        return this.depositeManager.reduce(money,memberId,detail);
    }

    @Override
    public MemberWalletDO getModel(Long memberId) {
        return this.depositeManager.getModel(memberId);
    }

    @Override
    public MemberWalletDO add(MemberWalletDO walletDO) {
        return this.depositeManager.add(walletDO);
    }

    @Override
    public void checkPwd(Long memberId, String password) {
        this.depositeManager.checkPwd(memberId,password);
    }

    @Override
    public MemberDepositeVO getDepositeVO(Long memberId) {
        return this.depositeManager.getDepositeVO(memberId);
    }
}
