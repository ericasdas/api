/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.model.member.dos.ReceiptHistory;
import cn.shoptnt.model.member.vo.ReceiptHistoryVO;
import cn.shoptnt.service.member.ReceiptHistoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 会员历史默认发票实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午2:57
 * @since v7.0
 */

@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class MemberHistoryReceiptDefaultImpl implements MemberHistoryReceiptClient {

    @Autowired
    private ReceiptHistoryManager receiptHistoryManager;

    @Override
    public ReceiptHistoryVO getReceiptHistory(String orderSn) {
        return receiptHistoryManager.getReceiptHistory(orderSn);
    }

    @Override
    public ReceiptHistory add(ReceiptHistory receiptHistory) {
        return receiptHistoryManager.add(receiptHistory);
    }

    @Override
    public ReceiptHistory edit(ReceiptHistory receiptHistory) {
        return receiptHistoryManager.edit(receiptHistory, receiptHistory.getHistoryId());
    }

    @Override
    public void updatePriceByOrderSn(Double orderPrice, String orderSn) {
        receiptHistoryManager.updatePriceByOrderSn(orderPrice, orderSn);
    }
}
