/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.ShopCatClient;
import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.service.shop.ShopCatManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/25 16:47
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class ShopCatClientDefaultImpl implements ShopCatClient {
    @Autowired
    private ShopCatManager shopCatManager;

    @Override
    public List getChildren(String catPath) {
        return shopCatManager.getChildren(catPath);
    }


    @Override
    public ShopCatDO getModel(Long id) {
        return shopCatManager.getModel(id);
    }
}
