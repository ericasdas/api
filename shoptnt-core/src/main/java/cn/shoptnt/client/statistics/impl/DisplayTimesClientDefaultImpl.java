/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.statistics.impl;

import cn.shoptnt.client.statistics.DisplayTimesClient;
import cn.shoptnt.model.statistics.dos.GoodsPageView;
import cn.shoptnt.model.statistics.dos.ShopPageView;
import cn.shoptnt.service.statistics.DisplayTimesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 访问次数client
 *
 * @author fk
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2020/3/20 上午8:21
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class DisplayTimesClientDefaultImpl implements DisplayTimesClient {

    @Autowired
    private DisplayTimesManager displayTimesManager;

    @Override
    public void countNow() {
        displayTimesManager.countNow();
    }

    @Override
    public void countShop(List<ShopPageView> list) {
        displayTimesManager.countShop(list);
    }

    @Override
    public void countGoods(List<GoodsPageView> list) {

        displayTimesManager.countGoods(list);
    }
}
