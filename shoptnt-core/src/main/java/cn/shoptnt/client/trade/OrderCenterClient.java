/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;

import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.vo.OrderParam;
import cn.shoptnt.model.trade.order.vo.TradeParam;
import cn.shoptnt.model.trade.order.vo.TradeVO;

public interface OrderCenterClient {


    /**
     * 创建订单对象
     * @param orderParam
     * @return
     */
    OrderDTO createOrderDTO(OrderParam orderParam);


    /**
     * 创建交易对象
     * @param tradeParam
     * @return
     */
    TradeVO createTradeVO(TradeParam tradeParam);

    /**
     * 创建交易
     * @param tradeVO
     */
    void createTrade(TradeVO tradeVO);

}
