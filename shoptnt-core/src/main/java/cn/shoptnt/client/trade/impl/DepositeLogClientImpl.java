/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.DepositeLogClient;
import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.model.trade.deposite.DepositeLogDO;
import cn.shoptnt.service.trade.deposite.DepositeLogManager;
import cn.shoptnt.framework.database.WebPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @description: 预存款记录客户端实现
 * @author: liuyulei
 * @create: 2020-01-02 16:32
 * @version:1.0
 * @since:7.1.4
 **/
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class DepositeLogClientImpl implements DepositeLogClient {


    @Autowired
    private DepositeLogManager depositeLogManager;

    @Override
    public void add(DepositeLogDO logDO) {
        depositeLogManager.add(logDO);
    }

    @Override
    public WebPage list(DepositeParamDTO paramDTO) {
        return this.depositeLogManager.list(paramDTO);
    }
}
