/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.goods.impl;

import cn.shoptnt.client.goods.TagClient;
import cn.shoptnt.service.goods.TagsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 商品标签对外接口实现
 *
 * @author zh
 * @version v7.0
 * @date 19/3/21 下午3:29
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class TagClientDefaultImpl implements TagClient {

    @Autowired
    private TagsManager tagsManager;

    @Override
    public void addShopTags(Long sellerId) {
        tagsManager.addShopTags(sellerId);
    }
}
