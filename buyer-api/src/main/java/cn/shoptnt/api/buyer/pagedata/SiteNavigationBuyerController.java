/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.pagedata;

import cn.shoptnt.model.pagedata.SiteNavigation;
import cn.shoptnt.service.pagedata.SiteNavigationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;

/**
 * 导航栏控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-12 17:07:22
 */
@RestController
@RequestMapping("/buyer/pages/site-navigations")
@Tag(name = "导航栏相关API")
public class SiteNavigationBuyerController {

    @Autowired
    private SiteNavigationManager siteNavigationManager;


    @Operation(summary = "查询导航栏列表")
    @Parameters({
            @Parameter(name = "client_type",description = "客户端类型", required = true,   in = ParameterIn.QUERY),
    })
    @GetMapping
    public List<SiteNavigation> list(@Parameter(hidden = true) String clientType) {

        return this.siteNavigationManager.listByClientType(clientType);
    }
}