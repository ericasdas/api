/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.vo.MemberDepositeVO;
import cn.shoptnt.service.member.DepositeManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.Map;

/**
 * 会员钱包控制器
 * @author liuyulei
 * @version v1.0
 * @since v7.2.0
 * 2019-12-30 16:24:51
 */
@RestController
@RequestMapping("/buyer/members/wallet")
@Tag(name = "会员预存款相关API")
public class MemberWalletBuyerController {

	@Autowired
	private DepositeManager depositeManager;

	@Autowired
	private ValidatorClient validatorClient;

	@Autowired
	private PassportManager passportManager;

	@Autowired
	private ShopTntConfig shoptntConfig;

	@Autowired
	private SmsClient smsClient;

	@Autowired
	private Cache cache;

	@Autowired
	private MemberManager memberManager;


	@GetMapping
	@Operation(summary	= "查询会员预存款余额")
	public Double get()	{
		Buyer buyer = UserContext.getBuyer();
		return depositeManager.getDeposite(buyer.getUid());
	}

	@GetMapping(value = "/cashier")
	@Operation(summary	= "获取预存款相关，收银台使用")
	public MemberDepositeVO getDepositeVO()	{
		Buyer buyer = UserContext.getBuyer();
		return depositeManager.getDepositeVO(buyer.getUid());
	}
	@GetMapping(value = "/check")
	@Operation(summary	= "检测会员是否设置过支付密码,会员中心设置或者修改密码时使用")
	public Boolean checkPassword() {
		Buyer buyer = UserContext.getBuyer();
		return depositeManager.getDepositeVO(buyer.getUid()).getIsPwd();
	}

	@Operation(summary = "获取账户信息")
	@GetMapping("info")
	public String getMemberInfo() {
		Buyer buyer = UserContext.getBuyer();
		//对会员状态进行校验
		Member member = memberManager.getModel(buyer.getUid());

		member.checkDisable();

		if (StringUtil.isEmpty(member.getMobile())) {
			throw new ServiceException(MemberErrorCode.E153.code(), "未绑定手机号");
		}
		//对获得的会员信息进行处理
		//这里查出的手机号是加密的
		String mobile = member.getMobile();

		//对用户名的处理
		String name = member.getUname();

		//将数据组织好json格式返回
		String uuid = StringUtil.getUUId();

		//身份验证时对用户名和手机号进行加密
		Map map = new HashMap(16);
        map.put("uname", name.substring(0, 1) + "***" + name.substring(name.length() - 1, name.length()));
        mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
		map.put("mobile", mobile);
		map.put("uuid", uuid);
		cache.put(uuid, member, shoptntConfig.getSmscodeTimout());
		return JsonUtil.objectToJson(map);

	}

	@GetMapping(value = "/check/smscode")
	@Operation(summary = "验证修改密码验证码")
	@Parameters({
			@Parameter(name = "sms_code", description = "验证码", required = true,   in = ParameterIn.QUERY)
	})
	public String checkUpdatePwdCode(@Valid @Parameter(hidden = true) @NotEmpty(message = "验证码不能为空") String smsCode) {
		Buyer buyer = UserContext.getBuyer();
		Member member = memberManager.getModel(buyer.getUid());
		if (member == null || StringUtil.isEmpty(member.getMobile())) {
			throw new ServiceException(MemberErrorCode.E114.code(), "当前会员未绑定手机号");
		}
		//这里查出的手机号是加密的
		String mobile = member.getMobile();
		//验证手机验证码
		boolean isPass = smsClient.valid(SceneType.SET_PAY_PWD.name(), mobile, smsCode);
		if (!isPass) {
			throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码不正确");
		}
		return null;

	}

	@PostMapping(value = "/set-pwd")
	@Operation(summary	= "设置支付密码")
	@Parameters({
			@Parameter(name = "password", description = "支付密码", required = true,   in = ParameterIn.QUERY)
	})
	public void setPwd(@Pattern(regexp = "[a-fA-F0-9]{32}", message = "密码格式不正确") String password)	{
		Buyer buyer = UserContext.getBuyer();
		Member member = memberManager.getModel(buyer.getUid());
		//这里查出的手机号是加密的，所以要使用会话秘钥解密
		String mobile = member.getMobile();
		String str = StringUtil.toString(cache.get(CachePrefix.MOBILE_VALIDATE.getPrefix() + "_" + SceneType.SET_PAY_PWD.name() + "_" + mobile));
		if (StringUtil.isEmpty(str)) {
			throw new ServiceException(MemberErrorCode.E115.code(), "请先对当前用户进行身份校验");
		}
		this.depositeManager.setDepositePwd(buyer.getUid(),password);
	}

	@PostMapping(value = "/smscode")
	@Operation(summary = "发送验证码")
	@Parameters({
			@Parameter(name = "uuid", description = "uuid客户端的唯一标识", required = true,   in = ParameterIn.QUERY),
			@Parameter(name = "captcha", description = "图片验证码", required = true,   in = ParameterIn.QUERY),
			@Parameter(name = "scene", description = "业务类型", required = true,   in = ParameterIn.QUERY)
	})
	public String sendSmsCode() {
		//参数验证（验证图片验证码或滑动验证参数等）
		this.validatorClient.validate();

		Buyer buyer = UserContext.getBuyer();
		Member member = memberManager.getModel(buyer.getUid());
		//当前会员是否绑定手机号
		if (member == null || StringUtil.isEmpty(member.getMobile())) {
			throw new ServiceException(MemberErrorCode.E114.code(), "当前会员未绑定手机号");
		}
		//这里查出的手机号是加密的，所以要使用会话秘钥解密
		String mobile = member.getMobile();
		passportManager.sendSetPayPwdSmsCode(mobile);
		return shoptntConfig.getSmscodeTimout() / 60 + "";


	}


}
