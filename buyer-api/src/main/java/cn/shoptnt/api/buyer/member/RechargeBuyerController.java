/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.client.trade.RechargeClient;
import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Buyer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

/**
 * @description: 会员充值API
 * @author: liuyulei
 * @create: 2019-12-31 09:26
 * @version:1.0
 * @since:7.1.4
 **/
@Tag(name = "会员充值API")
@RestController
@RequestMapping("/buyer/members/recharge")
@Validated
public class RechargeBuyerController {

    @Autowired
    private RechargeClient rechargeClient;


    @GetMapping(value = "/list")
    @Operation(summary = "查询会员预存款充值记录")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页面显示大小", required = true, in = ParameterIn.QUERY)
    })
    public WebPage list(@Valid DepositeParamDTO paramDTO) {
        Buyer buyer = UserContext.getBuyer();
        paramDTO.setMemberId(buyer.getUid());
        return this.rechargeClient.list(paramDTO);
    }


}
