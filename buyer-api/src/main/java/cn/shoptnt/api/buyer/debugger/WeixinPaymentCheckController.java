/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.model.payment.enums.ClientType;
import cn.shoptnt.model.payment.enums.PayMode;
import cn.shoptnt.model.payment.vo.PayBill;
import cn.shoptnt.service.payment.PaymentManager;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-15
 */
@RestController
@RequestMapping("/debugger/payment")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class WeixinPaymentCheckController {

    @Autowired
    private PaymentManager paymentManager;


    @GetMapping(value = "/weixin/pc/qr/iframe")
    public String qrPayFrame(  ) {
        StringBuffer html = new StringBuffer();

        html.append("<iframe id=\"iframe-qrcode\" width=\"200px\" height=\"200px\" scrolling=\"no\" src=\"payment/weixin/pc/qr/image\"></iframe>");

        return html.toString();
    }

    @GetMapping(value = "/weixin/pc/qr/image")
    public String qrPay(  ) {

        PayBill payBill = createBill();
        payBill.setClientType(ClientType.PC);
        payBill.setPayMode(PayMode.qr.name());

        StringBuffer html = new StringBuffer();
        Map<String,Object> map = paymentManager.pay(payBill);
        html.append("<form action='"+ map.get("gateway_url") +"' method='POST' >");


//        if (form.getFormItems() != null) {
//            form.getFormItems().forEach(formItem -> {
//                html.append("<input type='hidden' style='width:1000px' name='" + formItem.getItemName() + "' value='" + formItem.getItemValue() + "' />");
//            });
//        }

        html.append("<input type='submit' value='pay'/>");

        html.append("</form>");
        return html.toString();
    }




    private PayBill createBill() {

        PayBill payBill = new PayBill();
        payBill.setTradeType(TradeTypeEnum.debugger);
        payBill.setOrderPrice(0.01);
        payBill.setPluginId("weixinPayPlugin");
        payBill.setSubSn("123456");

        return payBill;
    }

}
