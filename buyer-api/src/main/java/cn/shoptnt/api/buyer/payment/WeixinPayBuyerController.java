/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.payment;

import cn.shoptnt.service.payment.plugin.weixin.WeixinPayPlugin;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.io.ByteArrayOutputStream;

/**
 * @author fk
 * @version v2.0
 * @Description: 订单支付
 * @date 2018/4/1616:44
 * @since v7.0.0
 */
@Tag(name = "订单支付API")
@RestController
@RequestMapping("/buyer/payment")
@Validated
public class WeixinPayBuyerController {

    @Autowired
    private WeixinPayPlugin weixinPayPlugin;

    @Parameter(hidden = true)
    @Operation(summary = "显示一个微信二维码")
    @GetMapping(value = "/weixin/qr/{pr}")
    public byte[] qr(@PathVariable(name = "pr") String pr) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        //图片的宽度
        int width = 200;
        //高度
        int height = 200;
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix m = writer.encode("weixin://wxpay/bizpayurl?pr=" + pr, BarcodeFormat.QR_CODE, height, width);
        MatrixToImageWriter.writeToStream(m, "png", stream);

        return stream.toByteArray();
    }


    @Operation(summary = "获取微信扫描支付的状态")
    @Parameters({
            @Parameter(name = "weixin_trade_sn", description = "微信预付订单号", required = true,   in = ParameterIn.PATH),
    })
    @GetMapping("/weixin/status/{weixin_trade_sn}")
    public String payStatus(@PathVariable(name = "weixin_trade_sn") String weixinTradeSn) {

        String result = weixinPayPlugin.onQuery(weixinTradeSn, null);
        return result;
    }

}
