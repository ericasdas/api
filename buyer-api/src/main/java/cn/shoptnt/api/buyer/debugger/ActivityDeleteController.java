/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.mapper.promotion.PromotionGoodsMapper;
import cn.shoptnt.mapper.promotion.groupbuy.GroupbuyActiveMapper;
import cn.shoptnt.mapper.promotion.groupbuy.GroupbuyGoodsMapper;
import cn.shoptnt.mapper.promotion.seckill.SeckillApplyMapper;
import cn.shoptnt.mapper.promotion.seckill.SeckillMapper;
import cn.shoptnt.mapper.promotion.seckill.SeckillRangeMapper;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyGoodsDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillRangeDO;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;

/**
 * @author lzg
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2021/02/24 16:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/debugger")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class ActivityDeleteController {

    @Autowired
    private GroupbuyActiveMapper groupbuyActiveMapper;

    @Autowired
    private PromotionGoodsMapper promotionGoodsMapper;

    @Autowired
    private GroupbuyGoodsMapper groupbuyGoodsMapper;

    @Autowired
    private SeckillMapper seckillMapper;

    @Autowired
    private SeckillApplyMapper seckillApplyMapper;

    @Autowired
    private SeckillRangeMapper seckillRangeMapper;


    @DeleteMapping(value = "/group/{act_id}")
    @Parameters({
            @Parameter(name = "act_id", description = "团购活动ID", required = true,   in = ParameterIn.PATH)
    })
    public String DeleteGroup(@PathVariable("act_id") Long actId) {
        int i = groupbuyActiveMapper.delete(new QueryWrapper<GroupbuyActiveDO>().eq("act_id", actId));
        i += groupbuyGoodsMapper.delete(new QueryWrapper<GroupbuyGoodsDO>().eq("act_id", actId));
        i += promotionGoodsMapper.delete(new QueryWrapper<PromotionGoodsDO>().eq("activity_id", actId));
        return "已删除活动ID  ：" + actId +  " ,共计删除 ：" + i +" 条。" ;
    }

    @DeleteMapping(value = "/seckill/{act_id}")
    @Parameters({
            @Parameter(name = "act_id", description = "限时抢购活动ID", required = true,   in = ParameterIn.PATH)
    })
    public String DeleteSkill(@PathVariable("act_id") Long actId) {
        int i = seckillMapper.deleteById(actId);
        i += seckillApplyMapper.delete(new QueryWrapper<SeckillApplyDO>().eq("seckill_id", actId));
        i += seckillRangeMapper.delete(new QueryWrapper<SeckillRangeDO>().eq("seckill_id", actId));
        i += promotionGoodsMapper.delete(new QueryWrapper<PromotionGoodsDO>().eq("activity_id", actId));
        return "已删除活动ID  ：" + actId +  " ,共计删除 ：" + i +" 条。" ;
    }


}
