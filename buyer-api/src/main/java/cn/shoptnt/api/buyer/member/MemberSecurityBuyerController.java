/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.validation.annotation.DemoSiteDisable;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.EmailClient;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.member.MemberSecurityManager;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 会员安全控制器
 *
 * @author zh
 * @version v7.0
 * @date 18/4/23 下午3:30
 * @since v7.0
 */
@RestController
@RequestMapping("/buyer/members")
@Validated
@Tag(name = "会员安全API")
public class MemberSecurityBuyerController {

    @Autowired
    private MemberSecurityManager memberSecurityManager;
    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private Cache cache;
    @Autowired
    private ShopTntConfig shoptntConfig;
    @Autowired
    private EmailClient emailClient;

    @PostMapping(value = "/security/send")
    @Operation(summary = "发送手机验证验证码")
    public String sendValSmsCode() {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        Buyer buyer = UserContext.getBuyer();
        Member member = memberManager.getModel(buyer.getUid());
        //当前会员是否绑定手机号
        if (member == null || StringUtil.isEmpty(member.getMobile())) {
            throw new ServiceException(MemberErrorCode.E114.code(), "当前会员未绑定手机号");
        }
        String mobile = member.getMobile();
        //发送验证码
        memberSecurityManager.sendValidateSmsCode(mobile);
        //将验证码失效时间返回，用于前端提示
        return shoptntConfig.getSmscodeTimout() / 60 + "";
    }


    @PostMapping(value = "/security/bind/send/{mobile}")
    @Operation(summary = "发送绑定手机验证码")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH)
    })
    public String sendBindSmsCode(@PathVariable("mobile") String mobile) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        //发送绑定手机号码
        memberSecurityManager.sendBindSmsCode(mobile);
        return null;
    }

    @PutMapping("/security/bind/{mobile}")
    @Operation(summary = "手机号码绑定API")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sms_code", description = "手机验证码", required = true,   in = ParameterIn.QUERY),
    })
    public String bindMobile(@PathVariable String mobile, @Parameter(hidden = true) @NotEmpty(message = "短信验证码不能为空") String smsCode) {
        boolean isPass = smsClient.valid(SceneType.BIND_MOBILE.name(), mobile, smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码错误");
        }
        //手机绑定
        memberSecurityManager.bindMobile(mobile);
        return null;
    }

    @GetMapping(value = "/security/exchange-bind")
    @Operation(summary = "验证换绑验证验证码")
    @Parameters({
            @Parameter(name = "sms_code", description = "验证码", required = true,   in = ParameterIn.QUERY)
    })
    public String checkExchangeBindCode(@Valid @Parameter(hidden = true) @NotEmpty(message = "验证码不能为空") String smsCode) {
        return this.valSmsCode(smsCode);

    }

    /**
     * 验证手机验证码
     *
     * @param code 验证码
     * @return
     */
    private String valSmsCode(String code) {
        Buyer buyer = UserContext.getBuyer();
        Member member = memberManager.getModel(buyer.getUid());
        if (member == null || StringUtil.isEmpty(member.getMobile())) {
            throw new ServiceException(MemberErrorCode.E114.code(), "当前会员未绑定手机号");
        }
        //这里查出的手机号是加密的
        String mobile = member.getMobile();
        boolean isPass = smsClient.valid(SceneType.VALIDATE_MOBILE.name(), mobile, code);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码不正确");
        }
        return null;
    }


    @PutMapping("/security/exchange-bind/{mobile}")
    @Operation(summary = "手机号码换绑API")
    @Parameters({
            @Parameter(name = "mobile", description = "手机号", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "sms_code", description = "手机验证码", required = true,   in = ParameterIn.QUERY),
    })
    public String exchangeBindMobile(@PathVariable("mobile") String mobile, @Parameter(hidden = true) @NotEmpty(message = "短信验证码不能为空") String smsCode) {

        boolean isPass = smsClient.valid(SceneType.BIND_MOBILE.name(), mobile, smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码错误");
        }
        memberSecurityManager.changeBindMobile(mobile);
        return null;
    }

    @GetMapping(value = "/security/password")
    @Operation(summary = "验证修改密码验证码")
    @Parameters({
            @Parameter(name = "sms_code", description = "验证码", required = true,   in = ParameterIn.QUERY)
    })
    public String checkUpdatePwdCode(@Valid @Parameter(hidden = true) @NotEmpty(message = "验证码不能为空") String smsCode) {
        return this.valSmsCode(smsCode);

    }


    @PutMapping(value = "/security/password")
    @Operation(summary = "修改密码")
    @Parameters({
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY)
    })
    @DemoSiteDisable
    public String updatePassword(@NotEmpty(message = "密码不能为空") String password) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        Buyer buyer = UserContext.getBuyer();
        Member member = memberManager.getModel(buyer.getUid());
        String mobile = member.getMobile();
        String str = StringUtil.toString(cache.get(CachePrefix.MOBILE_VALIDATE.getPrefix() + "_" + SceneType.VALIDATE_MOBILE.name() + "_" + mobile));
        //当前用户已绑定手机号时才进行身份校验
        if (!StringUtil.isEmpty(member.getMobile())&&StringUtil.isEmpty(str)) {
            throw new ServiceException(MemberErrorCode.E115.code(), "请先对当前用户进行身份校验");
        }
        memberSecurityManager.updatePassword(buyer.getUid(), password);
        return null;
    }

    @PostMapping(value = "/security/bind/email/send")
    @Operation(summary = "发送绑定邮箱验证码")
    @Parameters({
            @Parameter(name = "email", description = "电子邮箱", required = true,   in = ParameterIn.QUERY)
    })
    public String sendBindEmailCode(@NotEmpty(message = "电子邮箱不能为空") String email) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        //发送绑定邮箱验证码
        memberSecurityManager.sendBindEmailCode(email);
        return null;
    }

    @PostMapping("/security/bind/email")
    @Operation(summary = "电子邮箱绑定")
    @Parameters({
            @Parameter(name = "email", description = "电子邮箱", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "email_code", description = "电子邮箱验证码", required = true,   in = ParameterIn.QUERY)
    })
    public String bindEmail(@Parameter(hidden = true) @NotEmpty(message = "电子邮箱不能为空") String email, @Parameter(hidden = true) @NotEmpty(message = "电子邮箱验证码不能为空") String emailCode) {
        boolean isPass = emailClient.valid(SceneType.BIND_EMAIL.name(), email, emailCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "邮箱验证码错误");
        }
        memberSecurityManager.bindEmail(email);
        return null;
    }
}
