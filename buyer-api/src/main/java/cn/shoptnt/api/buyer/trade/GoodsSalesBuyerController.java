/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.member.vo.SalesVO;
import cn.shoptnt.service.trade.order.MemberSalesManager;
import cn.shoptnt.framework.database.WebPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * @author fk
 * @version v2.0
 * @Description: 商品交易controller
 * @date 2018/8/15 15:51
 * @since v7.0.0
 */
@Tag(name ="商品交易模块")
@RestController
@RequestMapping("/buyer/trade/goods")
public class GoodsSalesBuyerController {

    @Autowired
    private MemberSalesManager memberSalesManager;

    @Operation(summary	= "查询某商品的销售记录")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name="goods_id",description="商品ID",required=true, in=ParameterIn.PATH)
    })
    @GetMapping("/{goods_id}/sales")
    public WebPage salesList(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("goods_id") Long goodsId)	{
        return	this.memberSalesManager.list(pageSize,pageNo, goodsId);
    }


}
