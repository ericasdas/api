/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.MemberComment;
import cn.shoptnt.model.member.dto.AdditionalCommentDTO;
import cn.shoptnt.model.member.dto.CommentQueryParam;
import cn.shoptnt.model.member.dto.CommentScoreDTO;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.model.member.enums.CommentTypeEnum;
import cn.shoptnt.model.member.vo.CommentVO;
import cn.shoptnt.model.member.vo.MemberCommentCount;
import cn.shoptnt.service.member.MemberCommentManager;
import cn.shoptnt.model.trade.order.enums.CommentStatusEnum;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import java.util.List;

/**
 * 评论控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:19:14
 */
@RestController
@RequestMapping("/buyer/members/comments")
@Tag(name = "评论相关API")
public class MemberCommentBuyerController {

    @Autowired
    private MemberCommentManager memberCommentManager;

    @Operation(summary = "查询我的评论列表")
    @GetMapping
    public WebPage list(@Valid CommentQueryParam param) {

        Buyer buyer = UserContext.getBuyer();
        param.setMemberId(buyer.getUid());

        return this.memberCommentManager.list(param);
    }


    @Operation(summary = "提交评论")
    @PostMapping
    public MemberComment addComments(@Valid @RequestBody CommentScoreDTO comment) {

        return memberCommentManager.add(comment, Permission.BUYER);
    }

    @Operation(summary = "查询某商品的评论")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品ID", required = true, in = ParameterIn.PATH),
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "have_image", description = "是否有图 1：有图，0：无图",  in = ParameterIn.QUERY),
            @Parameter(name = "have_additional", description = "是否含有追评 1：是，0：否",  in = ParameterIn.QUERY),
            @Parameter(name = "grade", description = "评价等级 good：好评，neutral：中评，bad：差评",   in = ParameterIn.QUERY),
            @Parameter(name = "key_words", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping("/goods/{goods_id}")
    public WebPage list(@PathVariable("goods_id") Long goodsId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize,
                        @Parameter(hidden = true) Integer haveImage, @Parameter(hidden = true) Integer haveAdditional, @Parameter(hidden = true) String grade, @Parameter(hidden = true) String keyWords) {
        //评论查询条件
        CommentQueryParam param = new CommentQueryParam();
        param.setCommentsType(CommentTypeEnum.INITIAL.value());
        param.setAuditStatus(AuditEnum.PASS_AUDIT.value());
        param.setGoodsId(goodsId);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setHaveImage(haveImage);
        param.setHaveAdditional(haveAdditional);
        param.setGrade(grade);
        param.setKeyword(keyWords);

        return this.memberCommentManager.list(param);
    }

    @Operation(summary = "查询某商品的评论数量")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品ID", required = true, in = ParameterIn.PATH)
    })
    @GetMapping("/goods/{goods_id}/count")
    public MemberCommentCount count(@PathVariable("goods_id") Long goodsId) {

        return this.memberCommentManager.count(goodsId);
    }

    @Operation(summary = "提交追评")
    @PostMapping("/additional")
    public List<AdditionalCommentDTO> additionalComments(@Valid @RequestBody List<AdditionalCommentDTO> comments) {

        return memberCommentManager.additionalComments(comments, Permission.BUYER);
    }

    @Operation(summary = "查询追评列表，可根据状态查询")
    @GetMapping("/list")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "comment_status", description = "评论状态，WAIT_CHASE(待追评评论),FINISHED(已经完成评论)",example = "WAIT_CHASE(待追评评论),FINISHED(已经完成评论)",
                   required = true,   in = ParameterIn.QUERY),

    })
    public WebPage commentList(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String commentStatus, CommentQueryParam param) {
        Buyer buyer = UserContext.getBuyer();
        if(buyer == null ){
            throw new NoPermissionException("没有权限!");
        }

        //评论状态不能为空，且不能是未评论状态
        if(StringUtil.isEmpty(commentStatus) || CommentStatusEnum.UNFINISHED.name().equals(commentStatus)){
            throw new ServiceException(MemberErrorCode.E200.code(),"评论状态异常");
        }

        param.setMemberId(buyer.getUid());
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setCommentStatus(commentStatus);
        return this.memberCommentManager.list(param);
    }

    @Operation(summary = "根据订单编号和SKUid查询初评信息")
    @GetMapping("/detail")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "sku_id", description = "SKUid",  in = ParameterIn.QUERY)
    })
    public List<CommentVO> commentDetail(@Parameter(hidden = true) String orderSn,@Parameter(hidden = true) Long skuId){
        return this.memberCommentManager.get(orderSn,skuId);
    }

    @Operation(summary = "评论id查询评论详情")
    @GetMapping("/{comment_id}")
    @Parameters({
            @Parameter(name = "comment_id", description = "初评id", required = true,  in = ParameterIn.PATH)
    })
    public CommentVO commentDetail(@PathVariable("comment_id") Long commentId){
        return this.memberCommentManager.get(commentId);
    }


}
