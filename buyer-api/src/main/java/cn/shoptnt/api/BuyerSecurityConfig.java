/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.framework.context.ApplicationContextHolder;
import cn.shoptnt.framework.security.AuthenticationService;
import cn.shoptnt.framework.security.SecurityUrls;
import cn.shoptnt.framework.security.TokenAuthenticationFilter;
import cn.shoptnt.framework.security.message.UserDisableReceiver;
import cn.shoptnt.framework.security.model.Role;
import cn.shoptnt.model.base.DomainHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.frameoptions.AllowFromStrategy;
import org.springframework.security.web.header.writers.frameoptions.StaticAllowFromStrategy;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 妙贤 on 2018/3/12.
 * 买家安全配置
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/12
 */
@Configuration
@EnableWebSecurity
@Order(3)
public class BuyerSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DomainHelper domainHelper;

    @Autowired
    private BuyerAuthenticationService buyerAuthenticationService;

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;


    private static List<String> urls = new ArrayList<String>();

    static {

        //公用的需要过滤掉的
        urls.addAll(SecurityUrls.getCommonUrls());

        urls.add("/shops/list");
        urls.add("/shops/{spring:[0-9]+}");
        urls.add("/shops/cats/{spring:[0-9]+}");
        urls.add("/shops/navigations/{spring:[0-9]+}");
        urls.add("/shops/sildes/{spring:[0-9]+}");
        urls.add("/members/asks/goods/{spring:[0-9]+}");
        urls.add("/members/asks/detail/{spring:[0-9]+}");
        urls.add("/members/history" );
        urls.add("/members/logout*");
        urls.add("/members/asks/relation/{spring:[0-9]+}/{spring:[0-9]+}");
        urls.add("/members/asks/reply/list/{spring:[0-9]+}");
        urls.add("/members/comments/goods/{spring:[0-9]+}");
        urls.add("/members/comments/goods/{spring:[0-9]+}/count");
        urls.add("/pintuan/orders/**");

        urls.add("/pintuan/goods");
        urls.add("/pintuan/goods/**");

        urls.add("/payment/order/pay/query/**");
        urls.add("/payment/return/**");
        urls.add("/payment/weixin/**");
        urls.add("/payment/callback/**");

        urls.add("/passport/connect/pc/WECHAT/**");
        urls.add("/passport/login-binder/pc/**");
        urls.add("/passport/**");
        urls.add("/account-binder/**");
        urls.add("/connect/**");
        urls.add("/goods/**");
        urls.add("/pages/**");
        urls.add("/actuator/**");
        urls.add("/debugger/**");
        urls.add("/jquery.min.js");
        urls.add("/promotions/**");
        urls.add("/view");
        urls.add("/trade/goods/**");
        urls.add("/trade/orders/use/virtual");
        urls.add("/distribution/su/**");
        urls.add("/im/ws");
        urls.add("/wechat/**");
    }

    /**
     * 定义 buyer 工程的权限
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        List<String> newUrls = new ArrayList<String>();
        for (String url : urls) {
            newUrls.add("/buyer" + url);
        }
        //过滤掉不需要买家权限的api
        String[] urlList = newUrls.toArray(new String[urls.size()]  );
        http.cors().and().csrf().disable()
                .antMatcher("/buyer/**")
                //禁用session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                //定义验权失败返回格式
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler).authenticationEntryPoint(authenticationEntryPoint).and()
                .authorizeRequests()
                .and()
                .addFilterBefore(new TokenAuthenticationFilter(buyerAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class);
        //过滤掉swagger的路径
        http.authorizeRequests().antMatchers(urlList).permitAll();

        //定义有买家权限才可以访问
        http.authorizeRequests().anyRequest().hasRole(Role.BUYER.name());
        http.headers().addHeaderWriter(xFrameOptionsHeaderWriter());
        //禁用缓存
        http.headers().cacheControl().and()
                .contentSecurityPolicy("script-src  'self' 'unsafe-inline' ;" +
                        " frame-ancestors " + domainHelper.getBuyerDomain());

    }

    public XFrameOptionsHeaderWriter xFrameOptionsHeaderWriter() throws URISyntaxException {

        String buyerDomain = domainHelper.getBuyerDomain();

        URI uri = new URI(buyerDomain);

        AllowFromStrategy allowFromStrategy = new StaticAllowFromStrategy(uri);

        XFrameOptionsHeaderWriter xFrameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(allowFromStrategy);

        return xFrameOptionsHeaderWriter;
    }



    @Autowired
    private List<AuthenticationService> authenticationServices;


    @Bean
    public UserDisableReceiver userDisableReceiver() {
        return new UserDisableReceiver(authenticationServices);
    }
}
