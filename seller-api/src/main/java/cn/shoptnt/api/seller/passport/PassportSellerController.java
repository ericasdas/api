/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.passport;

import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.Validator;
import io.jsonwebtoken.ExpiredJwtException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;


/**
 * 商家验证码处理
 *
 * @author zh
 * @version v7.0
 * @since v7.0
 * 2018年3月23日 上午10:12:12
 */
@RestController
@RequestMapping("/seller/check")
@Tag(name = "登录注册必要校验API")
@Validated
public class PassportSellerController {

    @Autowired
    private PassportManager passportManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private SmsClient smsClient;

    @GetMapping(value = "/smscode/{mobile}")
    @Operation(summary = "验证手机验证码")
    @Parameters({
            @Parameter(name = "scene", description = "业务类型", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "sms_code", description = "验证码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.PATH),
    })
    public String checkSmsCode(@NotEmpty(message = "业务场景不能为空") String scene, @PathVariable("mobile") String mobile, @Valid @Parameter(hidden = true) @NotEmpty(message = "验证码不能为空") String smsCode) {
        boolean isPass = smsClient.valid(scene, mobile, smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码不正确");
        }
        return null;

    }

    @GetMapping("/username/{username}")
    @Operation(summary = "用户名重复校验")
    @Parameter(name = "username", description = "用户名", required = true,   in = ParameterIn.PATH)
    public String checkUserName(@PathVariable("username") String username) {
        Member member = memberManager.getMemberByName(username);
        Map map = new HashMap(16);
        if (member != null) {
            map.put("exist", true);
            map.put("suggests", memberManager.generateMemberUname(username));
            return JsonUtil.objectToJson(map);
        }
        map.put("exist", false);
        return JsonUtil.objectToJson(map);
    }


    @GetMapping("/mobile/{mobile}")
    @Operation(summary = "手机号重复校验")
    @Parameter(name = "mobile", description = "手机号", required = true,   in = ParameterIn.PATH)
    public String checkMobile(@PathVariable("mobile") String mobile) {
        boolean isPass = Validator.isMobile(mobile);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "手机号码格式不正确");
        }
        Member member = memberManager.getMemberByMobile(mobile);
        Map map = new HashMap(16);
        if (member != null) {
            map.put("exist", true);
            return JsonUtil.objectToJson(map);
        }
        map.put("exist", false);
        return JsonUtil.objectToJson(map);
    }


    @Operation(summary = "刷新token")
    @PostMapping("/token")
    @Parameter(name = "refresh_token", description = "刷新token", required = true,   in = ParameterIn.QUERY)
    public String refreshToken(@Parameter(hidden = true) @NotEmpty(message = "刷新token不能为空") String refreshToken) {
        try {
            return passportManager.exchangeSellerToken(refreshToken);
        } catch (ExpiredJwtException e) {
            throw new ServiceException(MemberErrorCode.E109.code(), "当前token已经失效");
        }
    }
}
