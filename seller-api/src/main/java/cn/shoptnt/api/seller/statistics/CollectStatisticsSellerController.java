/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.CollectFrontStatisticsManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 商家统计 商品收藏前50
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年4月18日下午5:02:50
 */
@RestController
@RequestMapping("/seller/statistics/collect")
@Tag(name = "商家统计  商品收藏前50")
public class CollectStatisticsSellerController {

    @Autowired
    private CollectFrontStatisticsManager collectFrontStatisticsManager;

    @Operation(summary = "收藏统计")
    @GetMapping(value = "/chart")
    public SimpleChart getChart() {
        Long sellerId = UserContext.getSeller().getSellerId();
        return this.collectFrontStatisticsManager.getChart(sellerId);
    }

    @Operation(summary = "收藏列表数据")
    @GetMapping(value = "/page")
    @Parameters({
            @Parameter(name = "page_no", description = "当前页码，默认为1",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页内数据量，默认为10",  in = ParameterIn.QUERY)})
    public WebPage getPage(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        Long sellerId = UserContext.getSeller().getSellerId();
        return this.collectFrontStatisticsManager.getPage(pageNo, pageSize, sellerId);
    }

}
