/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.shop.vo.ShopRoleVO;
import cn.shoptnt.service.shop.ShopRoleManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * 角色控制器
 *
 * @author admin
 * @version v1.0.0
 * @since v7.0.0
 * 2018-04-17 16:48:27
 */
@RestController
@RequestMapping("/seller/shops/roles")
@Tag(name = "角色相关API")
public class ShopRoleSellerController {

    @Autowired
    private ShopRoleManager shopRoleManager;


    @Operation(summary = "查询角色列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter @NotEmpty(message = "页码不能为空") Long pageNo, @Parameter @NotEmpty(message = "每页数量不能为空") Long pageSize) {
        return this.shopRoleManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加角色")
    @Parameters({
            @Parameter(name = "shopRoleVO", description = "角色", required = true)
    })
    @PostMapping
    public ShopRoleVO add(@RequestBody @Parameter ShopRoleVO shopRoleVO) {
        return this.shopRoleManager.add(shopRoleVO);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改角色表")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "ShopRoleVO", description = "角色", required = true)
    })
    public ShopRoleVO edit(@RequestBody @Parameter ShopRoleVO shopRoleVO, @PathVariable Long id) {
        return this.shopRoleManager.edit(shopRoleVO, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除角色")
    @Parameters({
            @Parameter(name = "id", description = "要删除的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.shopRoleManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个角色表")
    @Parameters({
            @Parameter(name = "id", description = "要查询的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public ShopRoleVO get(@PathVariable Long id) {

        return shopRoleManager.getRole(id);
    }

    @GetMapping(value = "/{id}/checked")
    @Operation(summary = "根据角色id查询所拥有的菜单权限")
    @Parameters({
            @Parameter(name = "id", description = "要查询的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public List<String> getCheckedMenu(@PathVariable Long id) {
        return this.shopRoleManager.getRoleMenu(id);
    }

}
