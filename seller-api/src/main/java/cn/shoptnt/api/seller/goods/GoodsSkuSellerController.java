/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goods.GoodsSkuManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * 商品sku控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:48:40
 */
@RestController
@RequestMapping("/seller/goods")
@Tag(name = "商品sku相关API")
public class GoodsSkuSellerController {

	@Autowired
	private GoodsQueryManager goodsQueryManager;

	@Autowired
	private GoodsSkuManager goodsSkuManager;

	@Operation(summary = "商品sku信息信息获取api")
	@Parameter(name	= "goods_id", description = "商品id",	required = true, 	in = ParameterIn.PATH)
	@GetMapping("/{goods_id}/skus")
	public List<GoodsSkuVO> queryByGoodsId(@PathVariable(name = "goods_id") Long goodsId) {

		CacheGoods goods = goodsQueryManager.getFromCache(goodsId);

		return  goods.getSkuList();
	}

	@Operation(summary = "更新商品的sku信息")
	@Parameter(name	= "goods_id", description = "商品id",	required = true, 	in = ParameterIn.PATH)
	@PutMapping("/{goods_id}/skus")
	public List<GoodsSkuVO> updateGoodsSku(@PathVariable(name = "goods_id") Long goodsId,@RequestBody List<GoodsSkuVO> skuList){

		goodsSkuManager.editSkus(skuList,goodsId);

		return  skuList;
	}

	@GetMapping(value = "/skus/{sku_ids}/details")
	@Operation(summary = "查询多个商品的基本信息")
	@Parameters({
			@Parameter(name = "sku_ids", description = "要查询的SKU主键", required = true,  in = ParameterIn.PATH) })
	public List<GoodsSkuVO> getGoodsDetail(@PathVariable("sku_ids") Long[] skuIds) {

		return this.goodsSkuManager.query(skuIds);
	}


	@Operation(summary = "查询SKU列表")
	@GetMapping("/skus")
	public WebPage list(GoodsQueryParam param, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
		param.setPageNo(pageNo);
		param.setPageSize(pageSize);
		param.setSellerId(UserContext.getSeller().getSellerId());
		return this.goodsSkuManager.list(param);
	}

}
