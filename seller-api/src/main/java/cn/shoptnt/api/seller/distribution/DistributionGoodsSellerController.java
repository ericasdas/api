/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.distribution;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.base.vo.SuccessMessage;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.distribution.dos.DistributionGoods;
import cn.shoptnt.model.distribution.dos.DistributionSetting;
import cn.shoptnt.service.distribution.DistributionGoodsManager;
import cn.shoptnt.framework.util.JsonUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 分销商品
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/8/30 上午9:42
 */
@RestController
@RequestMapping("/seller/distribution")
@Tag(name = "分销商品API")
public class DistributionGoodsSellerController {


    @Autowired
    private DistributionGoodsManager distributionGoodsManager;


    @Autowired
    private SettingClient settingClient;

    @Operation(summary = "分销商品返利获取")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品id", required = true, in = ParameterIn.PATH),
    })
    @GetMapping(value = "/goods/{goods_id}")
    public DistributionGoods querySetting(@PathVariable("goods_id") Long goodsId) {
        return distributionGoodsManager.getModel(goodsId);
    }


    @Operation(summary = "分销商品返利设置")
    @PutMapping(value = "/goods")
    public DistributionGoods settingGoods(DistributionGoods distributionGoods) {
        return distributionGoodsManager.edit(distributionGoods);
    }


    @Operation(summary = "获取分销设置:1开启/0关闭")
    @GetMapping(value = "/setting")
    public SuccessMessage setting() {
        DistributionSetting ds = JsonUtil.jsonToObject(settingClient.get(SettingGroup.DISTRIBUTION), DistributionSetting.class);
        return new SuccessMessage(ds.getGoodsModel());
    }


}
