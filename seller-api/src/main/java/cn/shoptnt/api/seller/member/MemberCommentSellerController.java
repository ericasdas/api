/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.member;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.member.dos.CommentReply;
import cn.shoptnt.model.member.dto.CommentQueryParam;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.model.member.vo.CommentVO;
import cn.shoptnt.service.member.CommentReplyManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import cn.shoptnt.service.member.MemberCommentManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 评论控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:19:14
 */
@RestController
@RequestMapping("/seller/members/comments")
@Tag(name = "评论相关API")
@Validated
public class MemberCommentSellerController {

    @Autowired
    private MemberCommentManager memberCommentManager;
    @Autowired
    private CommentReplyManager commentReplyManager;


    @Operation(summary = "查询评论列表")
    @GetMapping
    public WebPage list(@Valid CommentQueryParam param) {
        //审核状态
        param.setAuditStatus(AuditEnum.PASS_AUDIT.value());
        param.setSellerId(UserContext.getSeller().getSellerId());

        return this.memberCommentManager.list(param);
    }

    @Operation(summary = "回复评论")
    @Parameters({
            @Parameter(name = "comment_id", description = "评论id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "reply", description = "商家回复内容", required = true,   in = ParameterIn.QUERY)})
    @PostMapping(value = "/{comment_id}/reply")
    public CommentReply replyComment(@PathVariable(name = "comment_id") Long commentId, @NotEmpty(message = "回复内容不能为空") String reply) {

        CommentReply commentReply = this.commentReplyManager.replyComment(commentId,reply,Permission.SELLER);

        return commentReply;
    }

    @Operation(summary = "查询商品评论详请")
    @Parameters({
            @Parameter(name = "comment_id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{comment_id}")
    public CommentVO get(@PathVariable("comment_id") Long commentId) {
        CommentVO commentVO = this.memberCommentManager.get(commentId);

        Seller seller = UserContext.getSeller();
        if (!seller.getSellerId().equals(commentVO.getSellerId())){
            throw new ServiceException(TradeErrorCode.E460.code(),"操作订单无权限");
        }
        return commentVO;
    }

}
