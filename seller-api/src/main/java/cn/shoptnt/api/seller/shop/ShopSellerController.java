/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;


import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.validation.annotation.SafeDomain;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.model.shop.dos.ClerkDO;
import cn.shoptnt.model.shop.dto.ShopReceiptDTO;
import cn.shoptnt.model.shop.dto.ShopSettingDTO;
import cn.shoptnt.model.shop.vo.ShopInfoVO;
import cn.shoptnt.model.shop.vo.ShopSettingVO;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.model.shop.vo.operator.SellerEditShop;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.shop.ClerkManager;
import cn.shoptnt.service.shop.ShopManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 店铺相关API
 *
 * @author zhangjiping
 * @version v7.0.0
 * @since v7.0.0
 * 2018年3月30日 上午10:54:57
 */
@Tag(name = "店铺相关API")
@RestController
@RequestMapping("/seller/shops")
@Validated
public class ShopSellerController {
    @Autowired
    private ShopManager shopManager;

    @Autowired
    private ClerkManager clerkManager;

    public static String noShop = "NO_SHOP";

    @Operation(summary = "修改店铺设置")
    @Parameter(name = "shop_name", description = "店铺名称", required = true, in = ParameterIn.QUERY)
    @PutMapping()
    @Log(client = LogClient.seller, detail = "修改店铺设置信息,店铺:${shopName}", level = LogLevel.important)
    public ShopSettingVO edit(@Valid ShopSettingDTO shopSetting, @RegionFormat @RequestParam("shop_region") Region shopRegion, String shopName) {
        Long sellerId = UserContext.getSeller().getSellerId();
        shopSetting.setShopId(sellerId);
        return this.shopManager.edit(shopSetting, shopRegion, shopName);

    }


    @Operation(summary = "修改店铺Logo")
    @PutMapping(value = "/logos")
    @Parameter(name = "logo", description = "图片路径", required = true, in = ParameterIn.QUERY)
    @Log(client = LogClient.seller, detail = "修改店铺Logo：${logo}", level = LogLevel.important)
    public SellerEditShop editShopLogo(@Parameter(hidden = true) @NotEmpty(message = "店铺logo必填") @SafeDomain String logo) {
        SellerEditShop sellerEdit = new SellerEditShop();
        Seller seller = UserContext.getSeller();
        sellerEdit.setSellerId(seller.getSellerId());
        sellerEdit.setOperator("修改店铺logo");
        shopManager.editShopOnekey("shop_logo", logo);
        return sellerEdit;
    }


    @Operation(summary = "修改货品预警数设置")
    @PutMapping(value = "/warning-counts")
    @Parameter(name = "warning_count", description = "预警货品数", required = true, in = ParameterIn.QUERY)
    public SellerEditShop editWarningCount(@Parameter(hidden = true) @NotNull(message = "预警货品数必填") String warningCount) {
        SellerEditShop sellerEdit = new SellerEditShop();
        Seller seller = UserContext.getSeller();
        sellerEdit.setSellerId(seller.getUid());
        sellerEdit.setOperator("修改货品预警数");
        this.shopManager.editShopOnekey("goods_warning_count", warningCount);
        return sellerEdit;
    }

    @Operation(summary = "获取店铺信息")
    @GetMapping
    public ShopInfoVO get() {
        Seller seller = UserContext.getSeller();
        ShopInfoVO shop = shopManager.getShopInfo(seller.getSellerId());
        return shop;
    }

    @Operation(summary = "获取店铺状态")
    @GetMapping(value = "/status")
    public String getShopStatus() {
        ClerkDO clerk = clerkManager.getClerkByMemberId(UserContext.getBuyer().getUid());
        if (clerk != null && clerk.getUserState().equals(0)) {
            ShopVO shopVO = shopManager.getShop(clerk.getShopId());
            return shopVO.getShopDisable();
        }
        return noShop;

    }

    @Operation(summary = "商家发票设置")
    @PutMapping(value = "/receipt")
    public void receiptSetting(@Valid ShopReceiptDTO shopReceiptDTO) {

        this.shopManager.receiptSetting(shopReceiptDTO);
    }

}
